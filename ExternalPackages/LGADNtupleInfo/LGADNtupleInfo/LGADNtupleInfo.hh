//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Feb 27 16:23:07 2018 by ROOT version 6.10/02
// from TTree pulse/Digitized waveforms
// found on file: /home/atlasj/work/FNALtestbeam/DRS4/rootfile/Run-110.root
//////////////////////////////////////////////////////////

#ifndef LGADNtupleInfo_h
#define LGADNtupleInfo_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class LGADNtupleInfo {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           event;
   UShort_t        tc[4];
   Short_t         channel[36][1024];
   Double_t        channelFilter[36][1024];
   Float_t         time[4][1024];
   Float_t         xmin[36];
   Float_t         amp[36];
   Float_t         base[36];
   Float_t         integral[36];
   Float_t         intfull[36];
   Float_t         gauspeak[36];
   Float_t         sigmoidTime[36];
   Float_t         fullFitTime[36];
   Float_t         linearTime0[36];
   Float_t         linearTime15[36];
   Float_t         linearTime30[36];
   Float_t         linearTime45[36];
   Float_t         linearTime60[36];
   Float_t         fallingTime[36];
   Float_t         risetime[36];
   Float_t         constantThresholdTime[36];
   Bool_t          isRinging[36];

   // List of branches
   TBranch        *b_event;   //!
   TBranch        *b_tc;   //!
   TBranch        *b_channel;   //!
   TBranch        *b_channelFilter;   //!
   TBranch        *b_time;   //!
   TBranch        *b_xmin;   //!
   TBranch        *b_amp;   //!
   TBranch        *b_base;   //!
   TBranch        *b_integral;   //!
   TBranch        *b_intfull;   //!
   TBranch        *b_gauspeak;   //!
   TBranch        *b_sigmoidTime;   //!
   TBranch        *b_fullFitTime;   //!
   TBranch        *b_linearTime0;   //!
   TBranch        *b_linearTime15;   //!
   TBranch        *b_linearTime30;   //!
   TBranch        *b_linearTime45;   //!
   TBranch        *b_linearTime60;   //!
   TBranch        *b_fallingTime;   //!
   TBranch        *b_risetime;   //!
   TBranch        *b_constantThresholdTime;   //!
   TBranch        *b_isRinging;   //!

   LGADNtupleInfo(TTree *tree=0);
   virtual ~LGADNtupleInfo();
  virtual void     Action(){}
  virtual void     Finalize(){}
  virtual void     Initialize(){}

   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef LGADNtupleInfo_cxx
LGADNtupleInfo::LGADNtupleInfo(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/home/atlasj/work/FNALtestbeam/DRS4/rootfile/Run-110.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/home/atlasj/work/FNALtestbeam/DRS4/rootfile/Run-110.root");
      }
      f->GetObject("pulse",tree);

   }
   Init(tree);
}

LGADNtupleInfo::~LGADNtupleInfo()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t LGADNtupleInfo::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t LGADNtupleInfo::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void LGADNtupleInfo::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("event", &event, &b_event);
   fChain->SetBranchAddress("tc", tc, &b_tc);
   fChain->SetBranchAddress("channel", channel, &b_channel);
   fChain->SetBranchAddress("channelFilter", channelFilter, &b_channelFilter);
   fChain->SetBranchAddress("time", time, &b_time);
   fChain->SetBranchAddress("xmin", xmin, &b_xmin);
   fChain->SetBranchAddress("amp", amp, &b_amp);
   fChain->SetBranchAddress("base", base, &b_base);
   fChain->SetBranchAddress("integral", integral, &b_integral);
   fChain->SetBranchAddress("intfull", intfull, &b_intfull);
   fChain->SetBranchAddress("gauspeak", gauspeak, &b_gauspeak);
   fChain->SetBranchAddress("sigmoidTime", sigmoidTime, &b_sigmoidTime);
   fChain->SetBranchAddress("fullFitTime", fullFitTime, &b_fullFitTime);
   fChain->SetBranchAddress("linearTime0", linearTime0, &b_linearTime0);
   fChain->SetBranchAddress("linearTime15", linearTime15, &b_linearTime15);
   fChain->SetBranchAddress("linearTime30", linearTime30, &b_linearTime30);
   fChain->SetBranchAddress("linearTime45", linearTime45, &b_linearTime45);
   fChain->SetBranchAddress("linearTime60", linearTime60, &b_linearTime60);
   fChain->SetBranchAddress("fallingTime", fallingTime, &b_fallingTime);
   fChain->SetBranchAddress("risetime", risetime, &b_risetime);
   fChain->SetBranchAddress("constantThresholdTime", constantThresholdTime, &b_constantThresholdTime);
   fChain->SetBranchAddress("isRinging", isRinging, &b_isRinging);
   Notify();
}

Bool_t LGADNtupleInfo::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void LGADNtupleInfo::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t LGADNtupleInfo::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef LGADNtupleInfo_cxx
