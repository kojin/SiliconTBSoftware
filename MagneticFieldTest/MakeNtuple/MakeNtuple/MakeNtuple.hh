#ifndef __MakeNtuple_HH__
#define __MakeNtuple_HH__
#include <iostream>

#include "ReadUSBpixData/PixelEvents.hh"
#include "TTree.h"
#include "TFile.h"

class MakeNtuple{
private:
  std::string treename;
  std::string filename;
  TTree *tree; 
  TFile *file;
public:
  MakeNtuple(std::string _filename, std::string _treename){
    treename=_treename;
    filename=_filename;
  }
  MakeNtuple(){}
  ~MakeNtuple(){}
  int CreateNtuple(int nlayer,std::vector<PixelEvent> *event);

};


#endif
