#ifndef __PixelEvents_HH_
#define __PixelEvents_HH_
#include <iostream>
#include <vector>
#include "Hit/PixelHit.hh"
#include "Cluster/Cluster.hh"


class AnPixelEvent{
private:
  int EventNumber;
  int BCID;
  std::vector<PixelHit> hits;
  std::vector<Cluster> clusters;
public:
  AnPixelEvent(){}
  ~AnPixelEvent(){}
  void SetEventNumber(int eventnumber){
    EventNumber=eventnumber;
    hits.clear();
    clusters.clear();
  }
  void SetBCID(int bcid){
    BCID=bcid;;
  }
  int GetEventNumber(){
    return EventNumber;
  }
  int GetBCID(){
    return BCID;;
  }
  std::vector<PixelHit> GetHits(){
    return hits;
  }
  std::vector<Cluster> GetClusters(){
    return clusters;
  }
  void AddHit(PixelHit hit){
    hits.push_back(hit);
  }
  void AddHit(int ich,int icol,int irow, int ntot, int ntot2, int lv1,double posZ,std::string type){
    PixelHit ahit(ich,icol,irow,ntot,ntot2,lv1,0,type);
    ahit.SetPosZ(posZ);
    hits.push_back(ahit);
  }
  void MakeCluster();

  void PrintPixelEvent(){
    std::cout << "--" << std::endl;
    std::cout << "EventNumber = " << EventNumber << std::endl;
    std::cout << "nhit = " << hits.size() << std::endl;
    for(unsigned int ihit=0 ; ihit < hits.size();ihit ++){
      hits[ihit].PrintPixelHit();
    }
    std::cout << "ncluster = " << clusters.size() << std::endl;
    for(unsigned int iclu=0 ; iclu < clusters.size();iclu ++){
      clusters[iclu].PrintCluster();
    }

  }
  
};
class PixelEvent {
private:
  std::vector<AnPixelEvent*> events;
public:
  PixelEvent(){
    events.clear();
  }
  ~PixelEvent(){};
  int Add(AnPixelEvent *anevent){
    int evn=anevent->GetEventNumber();
    if(events.size()!=0){
      int lasteven=events[events.size()-1]->GetEventNumber();
      if(lasteven<evn)events.push_back(anevent);
      else if(lasteven==evn){
	std::cout << "this event has been created by loop in another channel : " << evn  << std::endl;
	return 0;
      }else {
	std::cout << "this event is not new. adding " << evn << " while " << lasteven << " events proccessed."<< std::endl;
	return -1;
      }
    }else{
      events.push_back(anevent);
    }
    return 0;
  }
  void doClustering(){
    for(unsigned int ieve=0;ieve<events.size();ieve++){    
      events[ieve]->MakeCluster();
    }
  }
  std::vector<AnPixelEvent*> Get(){
    return events;
  }
  void PrintPixelEvents(){
    for(unsigned int ievent=0;ievent<events.size();ievent++){
      events[ievent]->PrintPixelEvent();
    }
  }
  AnPixelEvent *GetThisEvent(int eventnumber){
    int here=-1; bool debug=false;
    //    if(eventnumber==28998)debug=true;
    if(debug)std::cout << events.size() << std::endl;
    for(unsigned int ievent=0;ievent<events.size();ievent++){
      if(events[ievent]->GetEventNumber()<eventnumber){
	here=ievent;
      }
      if(debug)std::cout << events[ievent]->GetEventNumber() << std::endl;
      if(events[ievent]->GetEventNumber()==eventnumber){
	return events[ievent];
      }
    }
    std::cout << "no such event with eventnumber=" << eventnumber << std::endl;
    std::cout << "creating event" << std::endl;
    AnPixelEvent *newevent = new AnPixelEvent();
    newevent->SetEventNumber(eventnumber);
    std::vector<AnPixelEvent*>::iterator itr=events.begin();
    itr=events.insert(itr+here+1,newevent);
    if(0){
      for(int ii=here-3;ii<here+2;ii++){
	std::cout << events[ii]->GetEventNumber() << std::endl;
      }
    }
    return events[here+1];
  }


  
};
#endif
