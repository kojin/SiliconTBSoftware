//#include "Common/header.hh"
#include "UserInterFace/UserInterFace.hh"
#include "FileRead/FileRead.hh"
#include "LGADAnalysis.hh"

int main(){
  std::cerr << "--start-- " <<std::endl;
  // main
  UserInterFace ui;
  ui.SetCondition();
  FileRead fr;
  TChain *chain = new TChain(ui.getTreename().c_str());
  fr.SetChain(chain,ui,ui.getTreename());
  std::cerr << "INFO : " << ui.getInputList().size() 
            << " Files are loaded." << std::endl;
  chain->Print();
  LGADAnalysis la;
  la.SetUserInterFace(ui);
  la.SetChain(chain);
  la.Execute();
  std::cerr << "---end--- " <<std::endl;
  
  return 0;
}
