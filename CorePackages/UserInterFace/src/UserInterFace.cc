#include "UserInterFace.hh"
#include "Command.hh"
#include <iostream>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <cstdio>

UserInterFace::UserInterFace()
{
  m_commandList.clear();  m_inputList.clear();
  m_grPath=""; m_grVersion=-1; m_jestessys=0;m_jessys=0; m_tespusys=0; m_jespusys=0; m_clsys=0;  m_pusys=0; 
  m_eessys=0;m_effsys=0;
  m_treename="";m_treename2=""; m_output=""; m_skimoutput=""; m_outputtree="";
  m_sample=0; m_decaymode=-1; m_totalevent=-1; m_pileup=0;  m_muscale=1;
  m_trigger=0; m_from_evt=0;  m_to_evt=0;
  m_loosetau=0;  m_7to8tev=0; m_leptotau=0;

}
UserInterFace::UserInterFace(int argc, char** argv)
{
  if (argc >= 2) {
    std::cerr << "[warn] Arguments are not supported by this UI." 
	      << std::endl;
    std::cerr << "       ==> They are ignored." << std::endl;
  }
  UserInterFace();
}

UserInterFace::~UserInterFace(void)
{
}

const std::string & UserInterFace::getCmdString(void)
{
  for (;;) {
    std::cout << "cmd> ";
    getline(std::cin,m_buffer);
    if (m_buffer == "\n" || m_buffer == "") {
      continue;
    } else {
      break;
    }
  }
  return m_buffer;
}
void UserInterFace::SetCondition(){
  for(;;){
    const std::string & mesg =getCmdString() ;
    if (mesg[0]=='#'){
      continue;
    }
    if (mesg == "quit" || mesg == "q" ||  mesg == ".q"   ||
	mesg == "exit" || mesg == "end"  || mesg == "terminate") {
      exit(0);
    }
    std::cout << mesg <<std::endl;
    if (mesg == "help" || mesg == "man"){
      PrintHelp();
      continue;
    }
    if (mesg == "process" || mesg == "run" || mesg == "start" ||
	mesg == "exe"     || mesg == "exec" || mesg == "execute" ) {
      break;
    }
    Command cmds;
    if (mesg.size() > 0) {
      addCommand(mesg);
      std::vector<std::string> strs = cmds.getCmdInfo(mesg);
      doCmd(strs);
    }
  }
}
void UserInterFace::addCommand(const std::string &str){
  if (str.size() != 0) {
    m_commandList.push_back(str);
  }
}
void UserInterFace::doCmd(std::vector<std::string> & strs){
  // good run list path 
  if (strs[0] == "grPath") {
    m_grPath=strs[1];
    //    std::cout << "GoodRunListDir: " << strs[1] << std::endl;
  }

  // good run list version
  else if (strs[0] == "grVersion") {
    m_grVersion=atoi(strs[1].c_str());
    //    std::cout << "GoodRunListDir: " << strs[1] << std::endl;
  }

  // input
  else if (strs[0] == "input") {
    m_inputList.push_back(strs[1]);
    //    std::cout << "input: " << strs[1] << std::endl;
  }

  // sample num
  else if (strs[0] == "sample") {
    m_sample = atoi(strs[1].c_str());
    //    std::cout << "sample: " << strs[1] << std::endl;
  }
     
  // sample num
  else if (strs[0] == "decaymode") {
    m_decaymode = atoi(strs[1].c_str());
    //    std::cout << "sample: " << strs[1] << std::endl;
  }
     
  else if (strs[0] == "leptotau") {
    m_leptotau = atoi(strs[1].c_str());
    //    std::cout << "sample: " << strs[1] << std::endl;
  }
     
  // total event
  else if (strs[0] == "totalevent") {
    m_totalevent = atoi(strs[1].c_str());
    //    std::cout << "totalevent: " << strs[1] << std::endl;
  }

  // trigger num
  else if (strs[0] == "trigger") {
    m_trigger = atoi(strs[1].c_str());
    //    std::cout << "trigger: " << strs[1] << std::endl;
  }
     
  // treename
  else if (strs[0] == "treename") {
    m_treename=strs[1];
    //    std::cout << "treename: " << m_treename << std::endl;
  }
  else if (strs[0] == "treename2") {
    m_treename2=strs[1];
    //    std::cout << "treename2: " << m_treename << std::endl;
  }

  // jobtype
  else if (strs[0] == "jobtype") {
    m_jobtype=strs[1];
    //    std::cout << "treename: " << m_treename << std::endl;
  }
     
  // pileup
  else if (strs[0] == "pileup") {
    m_pileup = atoi(strs[1].c_str());
    //    std::cout << "treename: " << m_treename << std::endl;
  }

  // loosetau
  else if (strs[0] == "loosetau") {
    m_loosetau = atoi(strs[1].c_str());
    //    std::cout << "treename: " << m_treename << std::endl;
  }

  // 7to8tev
  else if (strs[0] == "7to8tev") {
    m_7to8tev = atoi(strs[1].c_str());
    //    std::cout << "treename: " << m_treename << std::endl;
  }

  // muscale
  else if (strs[0] == "muscale") {
    m_muscale = atof(strs[1].c_str());
    std::cout << "m_muscale: " << m_muscale << std::endl;
  }
     
  // jessys
  else if (strs[0] == "jessys") {
    m_jessys = atoi(strs[1].c_str());
    //    std::cout << "treename: " << m_treename << std::endl;
  }
  // jessys
  else if (strs[0] == "jestessys") {
    m_jestessys = atoi(strs[1].c_str());
    //    std::cout << "treename: " << m_treename << std::endl;
  }
  // tespusys
  else if (strs[0] == "tespusys") {
    m_tespusys = atoi(strs[1].c_str());
    //    std::cout << "treename: " << m_treename << std::endl;
  }

  // jespusys
  else if (strs[0] == "jespusys") {
    m_jespusys = atoi(strs[1].c_str());
    //    std::cout << "treename: " << m_treename << std::endl;
  }
  // clsys
  else if (strs[0] == "clsys") {
    m_clsys = atoi(strs[1].c_str());
    //    std::cout << "treename: " << m_treename << std::endl;
  }
  // pusys
  else if (strs[0] == "pusys") {
    m_pusys = atoi(strs[1].c_str());
    //    std::cout << "treename: " << m_treename << std::endl;
  }
  // electron enegy scale sys
  else if (strs[0] == "eessys") {
    m_eessys = atoi(strs[1].c_str());
    //    std::cout << "treename: " << m_treename << std::endl;
  }
  // effsys
  else if (strs[0] == "effsys") {
    m_effsys = atoi(strs[1].c_str());
    //    std::cout << "treename: " << m_treename << std::endl;
  }
     
  // output
  else if (strs[0] == "output") {
    m_output=strs[1];
    //    std::cout << "output: " << m_output << std::endl;
  }

  // output
  else if (strs[0] == "skimoutput") {
    m_skimoutput=strs[1];
    //    std::cout << "output: " << m_output << std::endl;
  }
     
  // outputtree
  else if (strs[0] == "outputtree") {
    m_outputtree = strs[1];
    //    std::cout << "outputtree: " << m_outputtree << std::endl;
  }
     
  // begin event number
  else if (strs[0] == "from") {
    m_from_evt = atoi(strs[1].c_str());
    //    std::cout << "from: " << m_from_evt << std::endl;
  }

  // end event number
  else if (strs[0] == "to") {
    m_to_evt = atoi(strs[1].c_str());
    //    std::cout << "to: " << m_to_evt << std::endl;
  }

  // no matching
  else  {
    std::cout << strs[0] << ": command not found "  << std::endl;
    std::cout <<  " " << std::endl;
    PrintHelp();
  }
}
void UserInterFace::PrintHelp(){
  std::cout << "Usage: " << std::endl;
  std::cout << "Execute Commands : process, run, start, exe, exec, execute" 
	    << std::endl;
  std::cout << "Quit Commands    : quit, q, .q, exit, end, terminate "
	    << std::endl;
  std::cout << "Setting Commands : input [Files] " << std::endl;
  std::cout << "                   treename [TreeName] " << std::endl;
  std::cout << "                   treename2 [TreeName2] " << std::endl;
  std::cout << "                   output [File] " << std::endl;
  std::cout << "                   from [event] " << std::endl;
  std::cout << "                   to [event] " << std::endl;
}
