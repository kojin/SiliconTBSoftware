#ifndef _HistogramTools_hh_
#define _HistogramTools_hh_

//#include "Common/header.hh"
#include <iostream>
#include "TH1D.h"
#include "THStack.h"
#include "TMath.h"

class HistogramTools {
private:
public:

  void ScaleToEstimate(TH1D*hist,double value,double error,bool sysonly=false);
  void addOverFlow(TH1D* hist);
  void setXRange(TH1D* hist,double xmin, double xmax,bool addoverflow=true);
  //  double getEntries(TH1D* hist,double thr);
  double cdfKSDist(const TH1 *parent, const TH1 *test); 
  double cdfKS(TH1 *parent, const TH1 *test);
  double *MyChisq(TH1 *parent, const TH1 *test,bool syserr=false);
  double getEntries(TH1D* hist,double thr);
  double getEntries(TH1D* hist);
  double getEntryErrors(TH1D* hist,double thr);
  double getEntryErrors(TH1D* hist);
  double *getRatio(double rr[3], double nOS, double nOSe, double nSS, double nSSe);
  void BinLogX(TH1*h);



//  void FillScaledHistogram(TH1D*hist[3],int lepcateg,double value,float eventweight);
//  void FillScaledHistogram(TH1D*hist,double value,float eventweight);
//  void FillScaledHistogram(TH2D*hist[3],int lepcateg,double value1,double value2,float eventweight);
//  void FillScaledHistogram(TH2D*hist,double value1,double value2,float eventweight);
//  void FillScaledFourVectorHistogram(TH1D*hist[4][3],int lepcateg,TLorentzVector*lv,float eventweight);
//  void FillScaledFourVectorHistogram(TH1D*hist[4],TLorentzVector*lv,float eventweight);
//  void FillScaledFourVectorHistogram(std::vector<TH1D*>hist,TLorentzVector*lv,float eventweight);
  
};

#endif
