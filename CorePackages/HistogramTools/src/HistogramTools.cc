#include "HistogramTools.hh"
void HistogramTools::BinLogX(TH1*h)
{
  TAxis *axis = h->GetXaxis();
  int bins = axis->GetNbins();

  Axis_t from = axis->GetXmin();
  Axis_t to = axis->GetXmax();
  Axis_t width = (to - from) / bins;
  Axis_t *new_bins = new Axis_t[bins + 1];

  for (int i = 0; i <= bins; i++) {
    new_bins[i] = TMath::Power(10, from + i * width);

  }
  axis->Set(bins, new_bins);
  delete new_bins;
} 
void HistogramTools::ScaleToEstimate(TH1D*hist,double value,double error,bool sysonly){
  if(hist->Integral()==0)return;
  hist->Scale(value/hist->Integral());
  double staterr2=0;
  for(int ibin=1;ibin<=hist->GetNbinsX();ibin++)
    staterr2+=hist->GetBinError(ibin)*hist->GetBinError(ibin);
  double staterr=sqrt(staterr2);
  double syserr;
  if(sysonly)syserr=error;
  else {
    if(error*error-staterr2<0)syserr=0;
    else syserr=sqrt(error*error-staterr2);
  }
  for(int ibin=1;ibin<=hist->GetNbinsX();ibin++){
    double binvalue=hist->GetBinContent(ibin);
    double binstaterror=hist->GetBinError(ibin);
    double binsyserror=binvalue*syserr/hist->Integral();
    hist->SetBinError(ibin,sqrt(binstaterror*binstaterror+binsyserror*binsyserror));
  }
  if(0)std::cerr <<hist->Integral()<< "+-" << error << "("<< staterr << ", " << syserr << ")"<< std::endl;
}

void HistogramTools::addOverFlow(TH1D* hist){
  int nbins=hist->GetNbinsX();
  double lastbin=0;
  double lastbine=0;
  lastbin+=hist->GetBinContent(nbins);
  lastbine+=pow(hist->GetBinError(nbins),2);
  lastbin+=hist->GetBinContent(nbins+1);
  lastbine+=pow(hist->GetBinError(nbins+1),2);
  hist->SetBinContent(nbins,lastbin);
  hist->SetBinError(nbins,sqrt(lastbine));
  hist->SetBinContent(nbins+1,0);
  hist->SetBinError(nbins+1,0);
}

//double HistogramTools::getEntries(TH1D* hist,double thr){
//  int nbins=hist->GetNbinsX();
//  double nevent=0;
//  for(int ibin=1;ibin<=nbins;ibin++){
//    if(hist->GetXaxis()->GetBinLowEdge(ibin)>=thr)
//      nevent+=hist->GetBinContent(ibin);
//  }
//  return nevent;
//}

void HistogramTools::setXRange(TH1D* hist,double xmin, double xmax,bool addoverflow){
  if(xmin<hist->GetXaxis()->GetXmin()
     ||xmax>hist->GetXaxis()->GetXmax()) return;
  double overflow=0;
  for(int ibin=hist->FindBin(xmax*0.9999999)+1;ibin<=hist->GetNbinsX()+1;ibin++){
    overflow+=hist->GetBinContent(ibin);
  }
  hist->GetXaxis()->SetRangeUser(xmin,xmax*0.9999999);
  if(addoverflow)hist->AddBinContent(hist->FindBin(xmax*0.9999999),overflow);
  return;
}



double HistogramTools::cdfKSDist(const TH1 *parent, const TH1 *test) {

  if (parent->GetNbinsX() != test->GetNbinsX()) return -9E20;
  
  //Get the KS distance
  double sumParent = 0.0, sumTest = 0.0;

  for (int i = 1; i <= parent->GetNbinsX(); ++i) {
    sumParent += parent->GetBinContent(i);
    sumTest += test->GetBinContent(i);
  }

  double sParent = 1/sumParent;
  double sTest = 1/sumTest;

  double rSumParent = 0.0, rSumTest = 0.0, maxDiff = -9E20;
  for (int i = 1; i <= parent->GetNbinsX(); ++i) {
    rSumParent += sParent*parent->GetBinContent(i);
    rSumTest += sTest*test->GetBinContent(i);
    double diff = TMath::Abs(rSumParent-rSumTest);
    if (diff > maxDiff) maxDiff = diff;
  }

  return maxDiff;

}

double HistogramTools::cdfKS(TH1 *parent, const TH1 *test) {

  double ksDist = cdfKSDist(parent,test);

  //Run the pseudo-experiments
  TH1 *peTest = (TH1 *)test->Clone("peTest");
  peTest->SetDirectory(0); //We'll delete this ourselves at the end

  int nBigger = 0;
  for (int i = 0; i<1000; ++i) {
    peTest->Reset();
    peTest->FillRandom(parent,(int)test->Integral());
    double ksPEDist = cdfKSDist(parent,peTest);
    if (ksPEDist > ksDist) nBigger++;
  }

  delete peTest;

  return ((double)nBigger)/1000;

}

double mychisq[2] = {-9E20,0};
double *HistogramTools::MyChisq(TH1 *parent, const TH1 *test,bool syserr){
  if (parent->GetNbinsX() != test->GetNbinsX()) return mychisq;
  mychisq[0]=0.;mychisq[1]=0;
  for (int i = 1; i <= parent->GetNbinsX(); ++i) {
    double ParentCont = parent->GetBinContent(i);
    double ParentContErr = parent->GetBinError(i);
    double TestCont  = test->GetBinContent(i);
    if(ParentCont>0){
      if(syserr)mychisq[0]+=(TestCont-ParentCont)*(TestCont-ParentCont)/(ParentContErr*ParentContErr+ParentCont);
      else mychisq[0]+=(TestCont-ParentCont)*(TestCont-ParentCont)/ParentCont;
      mychisq[1]++;
    }
  }
  mychisq[1]--;
  return mychisq;
}


double HistogramTools::getEntries(TH1D* hist,double thr){
  int nbins=hist->GetNbinsX();
  double nevent=0;
  for(int ibin=0;ibin<=nbins;ibin++){
    if(hist->GetXaxis()->GetBinLowEdge(ibin)>=thr)
      nevent+=hist->GetBinContent(ibin);
  }
  return nevent;
}

double HistogramTools::getEntries(TH1D* hist){
  int nbins=hist->GetNbinsX();
  double nevent=0;
  for(int ibin=0;ibin<=nbins;ibin++){
      nevent+=hist->GetBinContent(ibin);
  }
  return nevent;
}

double HistogramTools::getEntryErrors(TH1D* hist,double thr){
  int nbins=hist->GetNbinsX();
  double nevent=0;
  for(int ibin=1;ibin<=nbins;ibin++){
    if(hist->GetXaxis()->GetBinLowEdge(ibin)>=thr)
      nevent+=hist->GetBinError(ibin)*hist->GetBinError(ibin);
  }
  return sqrt(nevent);
}
double HistogramTools::getEntryErrors(TH1D* hist){
  int nbins=hist->GetNbinsX();
  double nevent=0;
  for(int ibin=1;ibin<=nbins;ibin++){
      nevent+=hist->GetBinError(ibin)*hist->GetBinError(ibin);
  }
  return sqrt(nevent);
}

double *HistogramTools::getRatio(double rr[3], double nOS, double nOSe, double nSS, double nSSe){
  //  double rr[3]={0,0,0};
  rr[0]=nOS/nSS;
  rr[1]=sqrt(nOSe/nSS*nOSe/nSS+nOS*nOS*nSSe*nSSe/nSS/nSS/nSS/nSS);
  rr[2]=sqrt(sqrt(nOS)/nSS*sqrt(nOS)/nSS+nOS*nOS*sqrt(nSS)*sqrt(nSS)/nSS/nSS/nSS/nSS);
  return rr;
}


//void HistogramTools::FillScaledFourVectorHistogram(TH1D*hist[4][3],int lepcateg,TLorentzVector *lv,float eventweight){
//  FillScaledHistogram(hist[0],lepcateg,lv->Pt()/GeV,eventweight);
//  FillScaledHistogram(hist[1],lepcateg,lv->Eta(),eventweight);
//  FillScaledHistogram(hist[2],lepcateg,lv->Phi(),eventweight);
//  FillScaledHistogram(hist[3],lepcateg,lv->M()/GeV,eventweight);
//}
//void HistogramTools::FillScaledFourVectorHistogram(TH1D*hist[4],TLorentzVector *lv,float eventweight){
//  FillScaledHistogram(hist[0],lv->Pt()/GeV,eventweight);
//  FillScaledHistogram(hist[1],lv->Eta(),eventweight);
//  FillScaledHistogram(hist[2],lv->Phi(),eventweight);
//  FillScaledHistogram(hist[3],lv->M()/GeV,eventweight);
//}
//void HistogramTools::FillScaledFourVectorHistogram(std::vector<TH1D*>hist,TLorentzVector *lv,float eventweight){
//  if(hist.size()!=4){
//    std::cerr << "invalid histogram vector size..." << std::endl;
//    return;
//  }
//  FillScaledHistogram(hist[0],lv->Pt()/GeV,eventweight);
//  FillScaledHistogram(hist[1],lv->Eta(),eventweight);
//  FillScaledHistogram(hist[2],lv->Phi(),eventweight);
//  FillScaledHistogram(hist[3],lv->M()/GeV,eventweight);
//}
//void HistogramTools::FillScaledHistogram(TH1D*hist[3],int lepcateg,double value,float eventweight){
//  
//  FillScaledHistogram(hist[0],value,eventweight);
//  if(lepcateg&0x1)FillScaledHistogram(hist[1],value,eventweight);
//  if(lepcateg&0x2)FillScaledHistogram(hist[2],value,eventweight);
//}
//void HistogramTools::FillScaledHistogram(TH1D*hist,double value,float eventweight,float SampleWeight){
//  hist->Fill(value,eventweight*SampleWeight);
//  return;
//}
//
//void HistogramTools::FillScaledHistogram(TH2D*hist[3],int lepcateg,double value1,double value2,float eventweight){
//  FillScaledHistogram(hist[0],value1,value2,eventweight);
//  if(lepcateg&0x1)FillScaledHistogram(hist[1],value1,value2,eventweight);
//  if(lepcateg&0x2)FillScaledHistogram(hist[2],value1,value2,eventweight);
//}
//void HistogramTools::FillScaledHistogram(TH2D*hist,double value1,double value2,float eventweight,float SampleWeight){
//  hist->Fill(value1,value2,eventweight*SampleWeight);
//  return;
//}
