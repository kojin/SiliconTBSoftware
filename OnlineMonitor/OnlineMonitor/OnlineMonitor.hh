#ifndef __ONLINEMONITOR_HH__
#define __ONLINEMONITOR_HH__

#include <iostream>
#include <vector>
#include <thread>
#include "EventLoopManager/EventLoopManager.hh"

class OnlineMonitor : public EventLoopManager{
private:
  std::unique_ptr<std::thread> m_thread;
public:
  OnlineMonitor(){}
  ~OnlineMonitor();
  void OnlineMonThread();
  void InitializeThread();
};

#endif
