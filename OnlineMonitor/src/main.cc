#include <iostream>
#include <string>
#include "Common/AtlasStyle.hh"
#include "OnlineMonitor.hh"
#include "TROOT.h"
#include "TCanvas.h"
#include "TApplication.h"


int main(int argc, const char ** argv){
  SetAtlasStyle();
  gStyle->SetOptStat(10);
  gStyle->SetOptTitle(1);
  try{
    std::string configfile=argv[1];
    TApplication theApp("App", &argc, const_cast<char**>(argv),0,0);
    std::cout << argc << " " << argv[1] << std::endl;
    OnlineMonitor * om = new OnlineMonitor();
    om->RunOnlineMonitor();
    
    if(argc<1){
      std::cout << "running Example "<< std::endl;
      std::string BaseDir="/home/atlasj/work/FNALtestbeam/";
      om->AddDAQsystem("SeabasTLU",BaseDir+"SeabasTLU/SoftwareSeabasTLU-FNAL_20161205/SoftwareSeabasTLU-trunk/data/rawData_tlu_run0000018.bin");
      om->AddDAQsystem("SPEC",BaseDir+"SPEC/yarr-fnal/src/data/data000154_exttrigger_LBLFE65-16/rawData.dat");
      om->AddDAQsystem("HSIO2",BaseDir+"HSIO2/fnal_20161220/KEK83_KEK101_KEK104_KEK113_KEK114_KEK115_KEK112/data/cosmic_000101/cosmic_000101_000000.dat");
      om->AddDAQsystem("SVX4",BaseDir+"SVX4/old/data/run_00000025.dat");
    }else{
      std::cout << "reading config file : " << configfile << std::endl;
      std::ifstream fs(configfile);
      std::string detname="";
      std::string filename="";
      int boadnum=0;
      std::vector<std::string> filelist; filelist.clear();
      bool isnotnew=false;
      while(!fs.eof()){
	fs >> detname >> boadnum >> filename ;
	if(detname.substr(0,1)=="#")continue;
	isnotnew=false;
	for(auto x : filelist){
	  if(filename==x)isnotnew=true;
	}
	if(detname=="")break;
	if(isnotnew)continue;
	filelist.push_back(filename);
	om->AddDAQsystem(detname, filename, boadnum);
      }
      std::cout << "list of files : "  <<std::endl;
      for(auto x : filelist)std::cout << x << std::endl;
      std::cout << "=====" << std::endl;
      om->Initialize();
      om->InitializeThread();
      
      theApp.Run(); //execute
    }
  }catch(...){
    return -1;
  }
  return 0;
}
