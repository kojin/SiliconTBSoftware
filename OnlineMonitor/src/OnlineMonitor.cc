#include "OnlineMonitor.hh"
void *OnlineMonitor_thread(void *arg){
  OnlineMonitor * om = static_cast<OnlineMonitor*>(arg);
  try{
    om->OnlineMonThread();
  }catch (const std::exception &e) { 
    std::cout << "OnlineMon Thread exiting due to exception:\n" << e.what()<< std::endl;
  }catch (...){
    std::cout << "OnlineMon Thread exiting due to unknown exception." << std::endl;
  }
  return 0;
}

void OnlineMonitor::InitializeThread(){
  m_thread = std::unique_ptr<std::thread>(new std::thread(OnlineMonitor_thread,this));
}
void OnlineMonitor::OnlineMonThread(){
  Execute();
}

OnlineMonitor::~OnlineMonitor(){
  m_thread->join();
}
