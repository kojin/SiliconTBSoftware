#ifndef __SVXCONVERTER_HH__
#define __SVXCONVERTER_HH__

#include <fstream>
#include <iostream>
#include <utility>
#include <sstream>
#include <string>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <vector>
#include "TrackHitEvent/TrackHitEvent.hh"
#include "OnlineMonWindow/OnlineMonWindow.hh"
#include "HistogramTools/HistogramVariableStrage.hh"

#ifdef __linux
  #include <malloc.h>
#endif
#ifdef __APPLE__
  #include <malloc/malloc.h>
#endif

// The number of telescopes available for analysis.
#define TELESCOPE_NUM 3

struct SVX_Tree {
  int eventnum;
  int timestamp;
  int status;
  short chip_ID[4][4];
  short pipeline_ID[4][4];
  short hit_num[4][4];
  short adc[4][4][128];
};

struct EB_ev_data {
  unsigned int event_number;
  unsigned int timestamp;
};

struct EB_mod_data {
  unsigned short magicdata;
  unsigned short mod_totalsize;
};

struct EB_HEADER {
  unsigned int event_number;
  unsigned int timestamp;
  unsigned short totalsize;
  unsigned short mod_count;
  unsigned char data_check;
  unsigned char data_type;
  unsigned short geo_id;
};

struct SVX_EF_HEADER {
  // Common Data Region
  unsigned short magicdata;
  unsigned short totalsize;
  unsigned int event_number;
  unsigned int time_stamp;

  // SVX4 Data Region
  unsigned short ef_number;
  unsigned short ef_length[4];
  unsigned char data_quality;
  unsigned char data_type;
};

struct SVXdata {
  unsigned short hitch;
  unsigned char hitadc;
};

struct SVX_EF {
  unsigned short telescope_No;
  unsigned char chipID[4];
  unsigned char pipelineID[4];
  unsigned short hitnum;
  SVXdata svx_data[1];
};

class SVXconverter {
private:
  int runnumber;
  time_t starttime;
  std::string filename;
  std::ifstream infile;
  struct SVX_Tree svx_treedata;

  int tmp_evnum;
  int tmp_ts;

  EB_HEADER     header;
  EB_mod_data   *mod_data;
  SVX_EF_HEADER svx_ef_header;
  char* ef_data;
  char* ef_data_tele[TELESCOPE_NUM];

  int mod_size;
  int sizeOfModEvData;
  int sizeOfHeaderRemain;
  TrackHitEvent *thisevent;
  HistogramVariableStrage *hvs;
public:
  SVXconverter(std::string filename);
  ~SVXconverter();
  int readInFile(char *data, int len);

  int SeekAnEvent();
  int SeekEvent(int EventNumber);
  void PrintEvent();
  void PrintEvent(int EventNumber);
  void WriteEvent(int EventNumber);
  void print_SVXTree();
  void reset_SVXData();

  int GetAnEvent(int &eventnum, int *xx);
  int GetEvent(int EventNumber);
  int GetNextEvent();
  void SetThisEvent(TrackHitEvent *eve){thisevent=eve;}
  void SetHistogram(HistogramVariableStrage *_hvs);
  void SetHistogram(HistogramVariableStrage *_hvs,OnlineMonWindow *om);
  void FillHistogram();
  void ExtractData(struct SVX_EF *ef_data,std::vector<int> &linklist);
// Functions to get data from binary data
  int get_EventNumber(struct EB_HEADER &header);
  int get_TimeStamp(struct EB_HEADER &header);
  short get_TotalSize(struct EB_HEADER &header);
  short get_MultiEFCount(struct EB_HEADER &header);
  int get_DataCheck(struct EB_HEADER &header);
  int get_DataType(struct EB_HEADER &header);
  short get_GEOID(struct EB_HEADER &header);
  short get_MagicData(struct EB_mod_data *mod_data, int mod_count);
  short get_TotalEFSize(struct EB_mod_data *mod_data, int mod_count);
  short get_EF_MagicData(struct SVX_EF_HEADER &ef_header);
  short get_EF_TotalSize(struct SVX_EF_HEADER &ef_header);
  int get_EF_EventNumber(struct SVX_EF_HEADER &ef_header);
  int get_EF_TimeStamp(struct SVX_EF_HEADER &ef_header);
  int get_EF_TDC(struct SVX_EF_HEADER &ef_header);
  short get_SVX_Num_Telescope(struct SVX_EF_HEADER &ef_header);
  short get_SVX_EF_Length(struct SVX_EF_HEADER &ef_header, int tele_num);
  int get_SVX_EF_DataQuality(struct SVX_EF_HEADER &ef_header);
  int get_SVX_EF_DataType(struct SVX_EF_HEADER &ef_header);
  void get_SVXEFSVXData(struct SVX_EF *ef_data);
  short get_HitChannel(struct SVXdata *svx_data);
  int get_HitADC(struct SVXdata *svx_data);
  short get_Telescope_No(struct SVX_EF *ef_data);
  int get_ChipID(struct SVX_EF *ef_data, int chip_i);
  int get_PipelineID(struct SVX_EF *ef_data, int pipe_i);
  short get_HitNumber(struct SVX_EF *ef_data);

// Functions to write tree data from binary data.
  void  write_EFPreHead(struct SVX_EF_HEADER &ef_header);
  void  write_SVXEFHead(struct SVX_EF_HEADER &ef_header);
  void  write_SVXEFData(struct SVX_EF *ef_data, int ef_number);
  void  write_SVXEFSVXData(struct SVXdata *svx_data, int ef_number);

// For checking functions whether binary data can be correctly read.
  void  print_EBHeader(struct EB_HEADER &header);
  void  print_EBModHeader(struct EB_mod_data *mod_data, int mod_count);
  void  print_EFPreHead(struct SVX_EF_HEADER &ef_header);
  void  print_SVXEFRemainHead(struct SVX_EF_HEADER &ef_header);
  void  print_SVXEFData(struct SVX_EF *ef_data, int ef_number);
  void  print_SVXEFSVXData(struct SVXdata *svx_data);
};

#endif
