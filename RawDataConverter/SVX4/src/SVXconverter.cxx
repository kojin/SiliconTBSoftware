#include "SVX4/SVXconverter.hh"

SVXconverter::SVXconverter(std::string filename) {
  std::cerr << "Opening file :" << filename << " ...." << std::endl;
  infile.open(filename.c_str(), std::ios::in | std::ios_base::binary );
  if(!infile.is_open()) {
    std::cerr << "file :" << filename << " cannot be opened." << std::endl;
  }
  // infile.read((char*)&runnumber, sizeof(int));
  // infile.read((char*)&starttime, sizeof(time_t));
}

SVXconverter::~SVXconverter() {
  free(mod_data);
  free(ef_data);
  infile.close();
}

int SVXconverter::readInFile(char *data, int len) {
  infile.read(data, len);
  if(infile)return 1;
  else return 0;
}

int SVXconverter::SeekAnEvent() {
  int isOK=0;
  isOK=readInFile((char *)&header, sizeof(struct EB_HEADER));

  mod_size = header.mod_count*sizeof(struct EB_mod_data);
  mod_data = (struct EB_mod_data *)malloc(mod_size);

  isOK=readInFile((char *)mod_data, mod_size);

  sizeOfModEvData = sizeof(struct EB_mod_data) + sizeof(struct EB_ev_data);

  isOK=readInFile((char *)&svx_ef_header, sizeOfModEvData);

  sizeOfHeaderRemain = sizeof(struct SVX_EF_HEADER) - sizeOfModEvData;
  isOK=readInFile((char *)&svx_ef_header + sizeOfModEvData, sizeOfHeaderRemain);
  std::vector<int> linklist; linklist.clear();
  for (int i = 0; i < svx_ef_header.ef_number; i++) {
     ef_data = (char *)malloc(svx_ef_header.ef_length[i]);
     readInFile(ef_data, svx_ef_header.ef_length[i]);
     ef_data_tele[i] = ef_data;
     ExtractData((struct SVX_EF *)(ef_data_tele[i]),linklist);
     //     std::sort(linklist.begin(),linklist.end(),linklist);
     //     std::cout << "nlink = " << linklist.size() << std::endl;
     thisevent->setSVX4Linklist(linklist);
  }
  //  std::cout << svx_ef_header.data_quality << std::endl;
  //  thisevent->setSVX4TDC(svx_ef_header.data_quality);

  
  return isOK;
}
void SVXconverter::ExtractData(struct SVX_EF *ef_data,std::vector<int> &linklist){
  struct SVXdata *svx_data = (SVXdata *)((char*)ef_data + sizeof(struct SVX_EF) - sizeof(struct SVXdata));
  //  std::cout << "start an event " << std::endl;
  //  std::cout << get_HitNumber(ef_data) << " hit in a event " << std::endl;
  for (int i = 0; i < get_HitNumber(ef_data); i++) {
    //    std::cout << get_HitChannel(svx_data) << std::endl;
    
    int istrip=get_HitChannel(svx_data)%256;
    //    if(istrip<128)istrip=127-istrip;
    int ii=get_HitChannel(svx_data)/128;
    int ich=get_ChipID(ef_data, ii);
    int hadc=get_HitADC(svx_data);
    int ily=get_Telescope_No(ef_data);
    StripHit ahit(ich,(ii%4<2?istrip:0),(ii%4>1?istrip:0),
		  (ii%4<2?hadc:0),(ii%4>1?hadc:0),
		  ily,"SVX4",true);
    //    std::cout << ily << " " << ii << " " ;
    //    std::cout << "ily = " << ily << " " <<linklist.size() << std::endl;
    //    ahit.PrintStripHit();
    
    bool isnewlink=true;
    for(auto aa :linklist){
      if(aa==ily)isnewlink=false;
    }
    if(isnewlink){
      //      std::cout << "push_back" << std::endl;
      linklist.push_back(ily);
    }
    //    std::cout << "add this event : " ;
    //    ahit.PrintStripHit();
    if(hadc>15) // for online monitor !!!!!!
      thisevent->addSVX4Hits(ahit);
    svx_data += 1; // 1 means the size of a SVXdata data structure
  }
  //  std::cout << "end event " << std::endl;
  
}

int SVXconverter::SeekEvent(int EventNumber) {
  int ev_counter = 0;
  int isOK=0;
  while (ev_counter < EventNumber + 1) {
    isOK=SeekAnEvent();
    ev_counter++;
    if(!isOK)break;
  }
  return isOK;
}


void SVXconverter::SetHistogram(HistogramVariableStrage *_hvs){
  hvs=_hvs;
  std::stringstream ss; 
  std::stringstream ss2; 
  for(auto x : thisevent->getSVX4Linklist()){
    ss.str("");
    ss<< "DetDetail/SVX4/SVX4tel" <<  x;
    ss2<< "HitMap/SVX4/SVX4tel" <<  x;
    hvs->Add2DHistogram(ss.str()+"/Hitmap",256,-0.5,255.5,256,-0.5,255.5);
    hvs->AddHistogram(ss.str()+"/HitmapX",256,-0.5,255.5);
    hvs->AddHistogram(ss.str()+"/HitmapY",256,-0.5,255.5);
    hvs->AddHistogram(ss.str()+"/AdcDistX",128,-0.5,127.5);
    hvs->AddHistogram(ss.str()+"/AdcDistY",128,-0.5,127.5);
    hvs->Add2DHistogram(ss.str()+"/AdcX",256,-0.5,255.5,128,-0.5,127.5);
    hvs->Add2DHistogram(ss.str()+"/AdcY",256,-0.5,255.5,128,-0.5,127.5);
    hvs->AddHistogram(ss.str()+"/nHits",256,-0.5,255.5);
    hvs->AddHistogram(ss.str()+"/nCluster",30,-0.5,29.5);
  }
}
void SVXconverter::SetHistogram(HistogramVariableStrage *_hvs,OnlineMonWindow *om){
  SetHistogram(_hvs);
  std::stringstream ss; 
  std::stringstream ss2; 
  for(auto x : thisevent->getSVX4Linklist()){
    ss.str("");
    ss<< "DetDetail/SVX4/SVX4tel" <<  x;
    ss2<< "HitMap/SVX4/SVX4tel" <<  x;
    for(auto x : hvs->GetHistName()){
      om->registerTreeItem(x);
      om->registerHisto(x,hvs->GetHistogram(x),"HIST",0);
    }
    for(auto y : hvs->Get2DHistName()){
      om->registerTreeItem(y);
      om->registerHisto(y,hvs->Get2DHistogram(y),"COLZ",4);
    }
    om->makeTreeItemSummary(ss.str());

    om->registerTreeItem(ss2.str()+"Hitmap");
    om->registerHisto(ss2.str()+"Hitmap",hvs->Get2DHistogram(ss.str()+"/Hitmap"),"COLZ",4);

  }
  om->makeTreeItemSummary("HitMap/SVX4");
  om->makeTreeItemSummary("DetDetail/SVX4");
}

void SVXconverter::FillHistogram(){
  //  std::cout << "Fill one event " << std::endl;
  std::stringstream ss;
  int nhitsperevent=0;
  for(auto x : thisevent->getSVX4Linklist()){
    //    std::cout << x <<std::endl;
    ss.str("");
    ss<< "DetDetail/SVX4/SVX4tel" <<  x;
    std::vector<StripHit> svx4hit=thisevent->getSVX4Hits(x);
    hvs->GetHistogram(ss.str()+"/nHits")->Fill(svx4hit.size());
    hvs->GetHistogram(ss.str()+"/nCluster")->Fill(svx4hit.size());
    //    std::cout << "nhit " << svx4hit.size() <<  std::endl;
    std::vector<StripHit> colhit; colhit.clear();
    std::vector<StripHit> rowhit; rowhit.clear();
    for(auto ihit : svx4hit){
      nhitsperevent++;
      //      ihit.PrintStripHit();
      if((ihit.Channel()-239)%4<2){
	//	std::cout << "isCol" << std::endl;
	//	if(ihit.ADC()<10) continue;
	if(ihit.ADC()==0||ihit.ADC()==255) continue;
	if(ihit.Col()==63||ihit.Col()==127||ihit.Col()==191||ihit.Col()==255) continue;
	colhit.push_back(ihit);
	hvs->GetHistogram(ss.str()+"/AdcDistX")->Fill(ihit.ADC());
	hvs->Get2DHistogram(ss.str()+"/AdcX")->Fill(ihit.Col(),ihit.ADC());
	hvs->GetHistogram(ss.str()+"/HitmapX")->Fill(ihit.Col());
      } else {
	//	std::cout << "isRow" << std::endl;
	//	if(ihit.ADC2()<10) continue;
	if(ihit.ADC2()==0||ihit.ADC2()==255) continue;
	if(ihit.Row()==63||ihit.Row()==127||ihit.Row()==191||ihit.Row()==255) continue;
	rowhit.push_back(ihit);
	hvs->GetHistogram(ss.str()+"/AdcDistY")->Fill(ihit.ADC2());
	hvs->Get2DHistogram(ss.str()+"/AdcY")->Fill(ihit.Row(),ihit.ADC2());
	hvs->GetHistogram(ss.str()+"/HitmapY")->Fill(ihit.Row());

      }
      //std::cout << colhit.size() << " " << rowhit.size() << std::endl;
      if(colhit.size()>0 && rowhit.size()>0){
	int adc1=0;
	int adc2=0;
	int icolnum=0;
	int irownum=0;
	for(auto icol : colhit){
	  if(icol.ADC()>adc1){
	    adc1=icol.ADC();
	    icolnum=icol.Col();
	  }
	}
	for(auto irow : rowhit){
	  if(irow.ADC2()>adc2){
	    adc2=irow.ADC();
	    irownum=irow.Row();
	  }
	}
	hvs->Get2DHistogram(ss.str()+"/Hitmap")->Fill(icolnum,irownum);
      }


    }
  }
  //  std::cout << "filled " << nhitsperevent << " event to histo" << std::endl;
  //  std::cout << "Filled " << std::endl;
}


void SVXconverter::PrintEvent(int EventNumber) {
  SeekEvent(EventNumber);
  print_EBHeader(header);
  print_EBModHeader(mod_data, header.mod_count);
  print_EFPreHead(svx_ef_header);
  print_SVXEFRemainHead(svx_ef_header);
  for (int i = 0; i < svx_ef_header.ef_number; i++) {
    print_SVXEFData((struct SVX_EF *)(ef_data_tele[i]), i);
  }
}
void SVXconverter::PrintEvent() {
  print_EBHeader(header);
  print_EBModHeader(mod_data, header.mod_count);
  print_EFPreHead(svx_ef_header);
  print_SVXEFRemainHead(svx_ef_header);
  for (int i = 0; i < svx_ef_header.ef_number; i++) {
    print_SVXEFData((struct SVX_EF *)(ef_data_tele[i]), i);
  }
}

void SVXconverter::WriteEvent(int EventNumber) {
  SeekEvent(EventNumber);
  write_EFPreHead(svx_ef_header);
  write_SVXEFHead(svx_ef_header);
  for (int i = 0; i < svx_ef_header.ef_number; i++) {
    write_SVXEFData((struct SVX_EF *)(ef_data_tele[i]), i);
  }
}

void SVXconverter::reset_SVXData() {
  svx_treedata.eventnum = -1;
  svx_treedata.timestamp = -1;
  svx_treedata.status = -1;

  for(int tele_i = 0; tele_i < 4; tele_i++) {
    for(int chip_i = 0; chip_i < 4; chip_i++) {
      svx_treedata.chip_ID[tele_i][chip_i] = -1;
      svx_treedata.pipeline_ID[tele_i][chip_i] = -1;
      svx_treedata.hit_num[tele_i][chip_i] = -1;
      for(int ch = 0; ch < 128; ch++) {
        svx_treedata.adc[tele_i][chip_i][ch] = -1;
      }
    }
  }
}

int SVXconverter::GetAnEvent(int &eventnum, int *xx) {
/*
  eventnum = -1;
  infile.read((char*)&eventnum, sizeof(int));

  for(int ii = 0; ii < 6; ii++)
  {
    xx[ii] = -1;
    infile.read((char*)&xx[ii],sizeof(char));
    xx[ii] = xx[ii] < 0 ? xx[ii] + 256 : xx[ii];
  }
*/
  return 0;
}

int SVXconverter::GetEvent(int EventNumber) {
  reset_SVXData();
  return SeekEvent(EventNumber);

/*  int eventnumber = -1;
  int xx[6] = {-1, -1, -1, -1, -1, -1};
  while(1)
  {
    GetAnEvent(eventnumber, xx);
    if (eventnumber == EventNumber)
      break;
  }
*/
}

void SVXconverter::print_SVXTree() {
  std::cout << "eventnum : " << svx_treedata.eventnum << std::endl;
  std::cout << "timestamp: " << svx_treedata.timestamp << std::endl;

  for (int tele_i = 0; tele_i < TELESCOPE_NUM; tele_i++) {
    for (int chip_i = 0; chip_i < 4; chip_i++) {
      std::cout << "chip_ID[" << tele_i << "][" << chip_i << "]: " << svx_treedata.chip_ID[tele_i][chip_i] << std::endl;
      std::cout << "pipeline_ID[" << tele_i << "][" << chip_i << "]: " << svx_treedata.pipeline_ID[tele_i][chip_i] << std::endl;
      std::cout << "hit_num[" << tele_i << "][" << chip_i << "]: " << svx_treedata.hit_num[tele_i][chip_i] << std::endl;
      for (int ch = 0; ch < 128; ch++) {
        std::cout << "adc[" << tele_i << "][" << chip_i << "][" << ch << "]: " << svx_treedata.adc[tele_i][chip_i][ch] << std::endl;
      }
    }
  }
}

int SVXconverter::GetNextEvent() {
  return SeekAnEvent();
/*
  int eventnumber = -1;
  int xx[6] = {-1, -1, -1,-1,-1,-1};
  GetAnEvent(eventnumber, xx);

  //////// print event
  cout << "Event " << eventnumber << " " ;
  for (int ii = 0; ii < 6; ii++)
    cout << hex << setw(2) << xx[ii] << " ";
  cout << dec << endl;
*/
}

// ----------------------------------------------------------------------------
// Functions to get data from binary data
// ----------------------------------------------------------------------------

int SVXconverter::get_EventNumber(struct EB_HEADER &header) {
  return (int)header.event_number;
}

int SVXconverter::get_TimeStamp(struct EB_HEADER &header) {
  return (int)header.timestamp;
}

short SVXconverter::get_TotalSize(struct EB_HEADER &header) {
  return (short)header.totalsize;
}

short SVXconverter::get_MultiEFCount(struct EB_HEADER &header) {
  return (short)header.mod_count;
}

int SVXconverter::get_DataCheck(struct EB_HEADER &header) {
  return (int)header.data_check;
}

int SVXconverter::get_DataType(struct EB_HEADER &header) {
  return (int)header.data_type;
}

short SVXconverter::get_GEOID(struct EB_HEADER &header) {
  return (short)header.geo_id;
}

short SVXconverter::get_MagicData(struct EB_mod_data *mod_data, int mod_count) {
  // Magic data is Hex, but this value is integer.
  return mod_data[mod_count].magicdata;
}

short SVXconverter::get_TotalEFSize(struct EB_mod_data *mod_data, int mod_count) {
  return mod_data[mod_count].mod_totalsize;
}

short SVXconverter::get_EF_MagicData(struct SVX_EF_HEADER &ef_header) {
  // Magic data is Hex, but this value is integer.
  return ef_header.magicdata;
}

short SVXconverter::get_EF_TotalSize(struct SVX_EF_HEADER &ef_header) {
  return ef_header.totalsize;
}

int SVXconverter::get_EF_EventNumber(struct SVX_EF_HEADER &ef_header) {
  return ef_header.event_number;
}

int SVXconverter::get_EF_TimeStamp(struct SVX_EF_HEADER &ef_header) {
  return ef_header.time_stamp;
}

int SVXconverter::get_EF_TDC(struct SVX_EF_HEADER &ef_header) {
  return ef_header.data_quality;
}

short SVXconverter::get_SVX_Num_Telescope(struct SVX_EF_HEADER &ef_header) {
  return ef_header.ef_number;
}

short SVXconverter::get_SVX_EF_Length(struct SVX_EF_HEADER &ef_header, int tele_num) {
  return ef_header.ef_length[tele_num];
}

int SVXconverter::get_SVX_EF_DataQuality(struct SVX_EF_HEADER &ef_header) {
  return (int)ef_header.data_quality;
}

int SVXconverter::get_SVX_EF_DataType(struct SVX_EF_HEADER &ef_header) {
  return (int)ef_header.data_type;
}

short SVXconverter::get_HitChannel(struct SVXdata *svx_data) {
  return svx_data->hitch;
}

int SVXconverter::get_HitADC(struct SVXdata *svx_data) {
  return (int)svx_data->hitadc;
}

short SVXconverter::get_Telescope_No(struct SVX_EF *ef_data) {
  return ef_data->telescope_No;
}

int SVXconverter::get_ChipID(struct SVX_EF *ef_data, int chip_i) {
  return (int)ef_data->chipID[chip_i];
}

int SVXconverter::get_PipelineID(struct SVX_EF *ef_data, int pipe_i) {
  return (int)ef_data->pipelineID[pipe_i];
}

short SVXconverter::get_HitNumber(struct SVX_EF *ef_data) {
  return ef_data->hitnum;
}

// ----------------------------------------------------------------------------
// Functions to write tree data from binary data.
// ----------------------------------------------------------------------------

void SVXconverter::write_EFPreHead(struct SVX_EF_HEADER &ef_header) {
  tmp_evnum = ef_header.event_number;
  tmp_ts = ef_header.time_stamp;
}

void SVXconverter::write_SVXEFHead(struct SVX_EF_HEADER &ef_header) {
  svx_treedata.eventnum = tmp_evnum;
  svx_treedata.timestamp = tmp_ts;
  svx_treedata.status = ef_header.data_quality;
}

void SVXconverter::write_SVXEFData(struct SVX_EF *ef_data, int ef_number) {
  for(int i = 0; i < 4; i++) {
    svx_treedata.chip_ID[ef_number][i] = ef_data->chipID[i];
    svx_treedata.pipeline_ID[ef_number][i] = ef_data->pipelineID[i];
  }

  struct SVXdata *svx_data = (SVXdata *)((char*)ef_data + sizeof(struct SVX_EF) - sizeof(struct SVXdata));
  for (int i = 0; i < ef_data->hitnum; i++) {
    write_SVXEFSVXData(svx_data, ef_number);
    svx_data += 1; // 1 means the size of a SVXdata data structure
  }
}

void SVXconverter::write_SVXEFSVXData(struct SVXdata *svx_data, int ef_number) {
  svx_treedata.adc[ef_number][(svx_data->hitch)/128][(svx_data->hitch)%128] = svx_data->hitadc;
  svx_treedata.hit_num[ef_number][(svx_data->hitch)/128]++;
}

// ----------------------------------------------------------------------------
// For checking functions whether binary data can be correctly read.
// ----------------------------------------------------------------------------

void SVXconverter::print_EBHeader(struct EB_HEADER &header) {
  std::cout << std::endl << "### Event Data Header ###" << std::endl;
  std::cout << "Event Number  : " << get_EventNumber(header)  << std::endl;
  std::cout << "Time Stamp    : " << get_TimeStamp(header)    << std::endl;
  std::cout << "Total Size    : " << get_TotalSize(header)    << std::endl;
  std::cout << "MultiEF count : " << get_MultiEFCount(header) << std::endl;
  std::cout << "Data Check    : " << get_DataCheck(header)    << std::endl;
  std::cout << "Data Type     : " << get_DataType(header)     << std::endl;
  std::cout << "GEO ID        : " << get_GEOID(header)        << std::endl;
}

void SVXconverter::print_EBModHeader(struct EB_mod_data *mod_data, int mod_count) {
  std::cout << "### Event Mod Header ###" << std::endl;
  for (int i = 0; i < mod_count; i++) {
    std::cout << "Magic data    : 0x" << std::hex << std::uppercase << get_MagicData(mod_data, i) << std::dec << std::nouppercase << std::endl;
    std::cout << "Total EF size : "   << get_TotalEFSize(mod_data, i) << std::endl;
  }
}

void SVXconverter::print_EFPreHead(struct SVX_EF_HEADER &ef_header) {
  std::cout << "### Header of Event Fragment ###" << std::endl;
  std::cout << "Magic Data   : 0x"  << std::hex << std::uppercase << get_EF_MagicData(ef_header) << std::dec << std::nouppercase << std::endl;
  std::cout << "Total size   : "    << get_EF_TotalSize(ef_header)    << std::endl;
  std::cout << "Event Number : "    << get_EF_EventNumber(ef_header)  << std::endl;
  std::cout << "Time Stamp   : "    << get_EF_TimeStamp(ef_header)    << std::endl;
}

void SVXconverter::print_SVXEFRemainHead(struct SVX_EF_HEADER &ef_header) {
  std::cout << "### SVXEF Remain Head ###" << std::endl;
  std::cout << "number of telescope : " << get_SVX_Num_Telescope(ef_header) << std::endl;
  for (int i = 0; i < get_SVX_Num_Telescope(ef_header); i++) {
    std::cout << "telescope  : " << "no. : " << i << ", the length : " << get_SVX_EF_Length(ef_header, i) << std::endl;
  }
  std::cout << "data quality : " << get_SVX_EF_DataQuality(ef_header) << std::endl;
  std::cout << "data type    : " << get_SVX_EF_DataType(ef_header)    << std::endl;
}

void SVXconverter::print_SVXEFSVXData(struct SVXdata *svx_data) {
  std::cout << "hit channel : " << get_HitChannel(svx_data) << "  ADC value : " << get_HitADC(svx_data) << std::endl;
}

void SVXconverter::print_SVXEFData(struct SVX_EF *ef_data, int ef_number) {
  std::cout << "### SVX Event Fragment ###" << std::endl;
  std::cout << "telescope no.  : " << get_Telescope_No(ef_data) << std::endl;
  for (int i = 0; i < 4; i++) {
    std::cout << "chip id : " << get_ChipID(ef_data, i) << " pipeline id : " << get_PipelineID(ef_data, i) << std::endl;
  }
  std::cout << "number of hits : " << get_HitNumber(ef_data) << std::endl;
  // calculate the address where SVX data are located at
  struct SVXdata *svx_data = (SVXdata *)((char*)ef_data + sizeof(struct SVX_EF) - sizeof(struct SVXdata));
  for (int i = 0; i < get_HitNumber(ef_data); i++) {
    print_SVXEFSVXData(svx_data);
    svx_data += 1; // 1 means the size of a SVXdata data structure
  }
}
