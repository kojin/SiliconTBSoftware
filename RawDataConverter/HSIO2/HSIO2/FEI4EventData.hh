#ifndef __FEI4EventData__HH__
#define __FEI4EventData__HH__

//#include <deque>
#include <list>
#include <vector>
#include <string>
//#include <unistd.h>

struct FEI4Hit {
    unsigned col : 7;
    unsigned row : 9;
    unsigned tot : 5;
    unsigned unused : 11;
};

class FEI4Cluster {
public:
  FEI4Cluster() {
    nHits = 0;
  }
  ~FEI4Cluster() {}
  
  void addHit(FEI4Hit* hit) {
    hits.push_back(hit);
    nHits++;
  }
  
  unsigned getColLength() {
    int min = 999999;
    int max = -1;
    for (unsigned i=0; i<hits.size(); i++) {
      if (hits[i]->col > max)
	max = hits[i]->col;
      if (hits[i]->col < min)
	min = hits[i]->col;
    }
    return max-min+1;
  }
  
  unsigned getRowWidth() {
    int min = 999999;
    int max = -1;
    for (unsigned i=0; i<hits.size(); i++) {
      if (hits[i]->row > max)
	max = hits[i]->row;
      if (hits[i]->row < min)
	min = hits[i]->row;
    }
    return max-min+1;
  }
  
  unsigned nHits;
  std::vector<FEI4Hit*> hits;
};


class FEI4Event{
public:
  FEI4Event() {
    tag = 0;
    l1id = 0;
    bcid = 0;
    nHits = 0;
    nClusters = 0;
  }
  FEI4Event(unsigned arg_tag, unsigned arg_l1id, unsigned arg_bcid) {
    tag = arg_tag;
    l1id = arg_l1id;
    bcid = arg_bcid;
    nHits = 0;
    nClusters = 0;
  }
  ~FEI4Event() {}
  void addHit(unsigned arg_row, unsigned arg_col, unsigned arg_tot) {
    struct FEI4Hit tmp = {arg_col, arg_row, arg_tot, 0};
    hits.push_back(FEI4Hit(tmp));
    nHits++;
  }
  void doClustering();
  
  //  void toFileBinary(std::fstream &handle);
  //  void fromFileBinary(std::fstream &handle);
  
  uint16_t l1id;
  uint16_t bcid;
  uint32_t tag;
  uint16_t nHits;
  uint16_t nClusters;
  std::vector<FEI4Hit> hits;
  std::vector<FEI4Cluster> clusters;
  
};
#endif
