#ifndef __HSIO2CONVERTER_HH__
#define __HSIO2CONVERTER_HH__

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "FormattedRecord.hh"
#include "FEI4EventData.hh"
#include "Configuration/DAQLinkInfo.hh"
#include "Hit/PixelHit.hh"
#include "TrackHitEvent/TrackHitEvent.hh"
#include "OnlineMonWindow/OnlineMonWindow.hh"
#include "HistogramTools/HistogramVariableStrage.hh"

class HSIO2converter {

private:
  std::fstream infile;
  HistogramVariableStrage *hvs;
  //  std::vector<FEI4Event> fei4eve;
  TrackHitEvent *thisevent;
  std::vector<DAQLinkInfo> hli;
  int tmplastl1id;
public:
  HSIO2converter(){}
  HSIO2converter(std::string filename);
  ~HSIO2converter();
  void SetThisEvent(TrackHitEvent *_eve);
  void SetHSIO2Info(std::vector<DAQLinkInfo> _hli){hli=_hli;}
  void SetHistogram(HistogramVariableStrage *_hvs);
  void SetHistogram(HistogramVariableStrage *_hvs,OnlineMonWindow *om);
  int GetAnEvent();
  int GetEvent(int EventNumber);
  int GetNextEvent();
  void FillHistogram();
  void PrintInfo();
};


#endif
