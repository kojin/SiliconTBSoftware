/*===============================================
 
  Adapted raw data to text converter
Original authors: Jens Dopke & Matevz

Original usecase: high eta testbeam 
data converter, for use with RCE & cosmicsGui

Date modified: November 2014
By: RMDCarney
=================================================
*/

#include "HSIO2/FormattedRecord.hh"
#include <iostream>
#include <string>
#include <fstream>

int main(int argc, char *argv[]) {
  //if (argc != 2) {
  //  std::cout << "Specify input file" << std::endl;
  //  return 0;
  //}
    
  //std::string filename = argv[1];
  std::string filename = "cosmic_000613_000000.dat";
  std::cout << "Opening file: " << filename << std::endl;
  std::fstream infile(filename.c_str(), std::fstream::in | std::fstream::binary);

  std::cout << "... reading file ..." << std::endl;
    
  // Timestamp 4 byte
  unsigned timestamp = 0;
  infile.read((char*)&timestamp, 4);
  std::cout << "Timestamp: " << timestamp << std::endl;
    
  // Number of RCEs 4 byte
  unsigned num_rces = 0;
  infile.read((char*)&num_rces, 4);
  std::cout << "Number of RCEs: " << num_rces << std::endl;

  // Event loop
  while(infile) {
    // Event size 4 byte
    unsigned event_size = 0;
    infile.read((char*)&event_size, 4);
    std::cout << "Size of event: " << event_size << std::endl;
        
    // Break loop here if reached end if file
    if(!infile) break;

    // Event number 4 byte
    unsigned event_number = 0;
    infile.read((char*)&event_number, 4);
    std::cout << "Event Number: " << event_number << std::endl;

    // RCE number 4 byte
    unsigned rce_number = 0;
    infile.read((char*)&rce_number, 4);
    std::cout << "RCE Number: " << rce_number << std::endl;

    // Trigger information 9*4byte
    char trig_info[9*4];
    infile.read(trig_info, 9*4);
    // To be decoded
        
    event_size -= 12*4; // Subtract data which we already read

    // Read FE data
    char *fe_data = new char[event_size];
    infile.read(fe_data, event_size);

    for (unsigned i=0; i<event_size; i+=4) {
      FormattedRecord rec(*(unsigned*)&fe_data[i]);
      if (rec.isHeader()) {
	//std::cout << "Data Header: " << std::endl;
	//std::cout << "  Link = " << rec.getLink() << std::endl;
	//std::cout << "  L1ID = " << rec.getL1id() << std::endl;
	//std::cout << "  BCID = " << rec.getBxid() << std::endl;
      } else if (rec.isData()) {
	//std::cout << "Data:  " << rec.getCol() << "\t" << rec.getRow() << "\t" << rec.getToT() << std::endl;
      }
    }
    delete fe_data;
  }

  return 0;
}


