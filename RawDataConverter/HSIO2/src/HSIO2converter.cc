#include "HSIO2/HSIO2converter.hh"


HSIO2converter::HSIO2converter(std::string filename){
  std::cout << "Opening file: " << filename << std::endl;
  infile.open(filename.c_str(), std::fstream::in | std::fstream::binary);

  std::cout << "... reading file ..." << std::endl;
    
  // Timestamp 4 byte
  time_t timestamp = 0;
  infile.read((char*)&timestamp, 4);
  std::cout << "Timestamp: " << ctime(&timestamp);
    
  // Number of RCEs 4 byte
  unsigned num_rces = 0;
  infile.read((char*)&num_rces, 4);
  std::cout << "Number of RCEs: " << num_rces << std::endl;
  
  tmplastl1id=0;
}

HSIO2converter::~HSIO2converter(){
  infile.close();
}

void HSIO2converter::SetThisEvent(TrackHitEvent *_eve){
  thisevent=_eve;
}
void HSIO2converter::SetHistogram(HistogramVariableStrage *_hvs){

  hvs=_hvs;
  std::stringstream ss; 
  std::stringstream ss2; 
    //  for(auto x : thisevent->getFEI4Linklist()){
  std::vector<int> added; added.clear();
  for(auto x : hli){
    if(x.type.substr(0,4)!="FEI4")continue;
    bool isadded=false;
    for(auto ad : added){
      if(x.layer==ad){
	isadded=true;
	break;
      }
    }
    if(isadded)continue;
    added.push_back(x.layer);
    //    std::cout <<  x.layer << std::endl;
    
    ss.str("");
    ss<< "DetDetail/HSIO2/FEI4tel" <<  x.layer;
    ss2<< "HitMap/HSIO2/FEI4tel" <<  x.layer;
    if(x.type.substr(0,8)=="FEI4SING")
      hvs->Add2DHistogram(ss.str()+"/Hitmap",80,-0.5,79.5,336,-0.5,335.5);
    //    else hvs->Add2DHistogram(ss.str()+"/Hitmap",160,-0.5,159.5,672,-0.5,671.5);
    else if (x.type.substr(0,8)=="FEI4DOUB") 
      hvs->Add2DHistogram(ss.str()+"/Hitmap",80,-0.5,79.5,672,-0.5,671.5);
    else if (x.type.substr(0,8)=="FEI4QUAD") hvs->Add2DHistogram(ss.str()+"/Hitmap",160,-0.5,159.5,672,-0.5,671.5);
    else std::cout << "strange type : " << x.type <<  std::endl;
    hvs->AddHistogram(ss.str()+"/TotDist",16,-0.5,15.5);
    hvs->AddHistogram(ss.str()+"/L1id",16,-0.5,15.5);
    hvs->AddHistogram(ss.str()+"/nHits",50,-0.5,49.5);
    hvs->AddHistogram(ss.str()+"/nCluster",30,-0.5,29.5);
  }
}
void HSIO2converter::SetHistogram(HistogramVariableStrage *_hvs,OnlineMonWindow *om){

  hvs=_hvs;
  SetHistogram(hvs);
  std::stringstream ss; 
  std::stringstream ss2; 


  //  for(auto x : thisevent->getFEI4Linklist()){
  std::vector<int> added; added.clear();
  for(auto x : hli){
    if(x.type.substr(0,4)!="FEI4")continue;
    bool isadded=false;
    for(auto ad : added){
      if(x.layer==ad){
	isadded=true;
	break;
      }
    }
    if(isadded)continue;
    added.push_back(x.layer);
    //  for(auto x : hli){
    ss.str("");
    ss<< "DetDetail/HSIO2/FEI4tel" <<  x.layer;
    ss2<< "HitMap/HSIO2/FEI4tel" <<  x.layer;
    for(auto y : hvs->GetHistName()){
      om->registerTreeItem(y);
      om->registerHisto(y,hvs->GetHistogram(y),"HIST",0);
    }
    for(auto y : hvs->Get2DHistName()){
      om->registerTreeItem(y);
      om->registerHisto(y,hvs->Get2DHistogram(y),"COLZ",4);
    }
    om->makeTreeItemSummary(ss.str());

    om->registerTreeItem(ss2.str()+"Hitmap");
    om->registerHisto(ss2.str()+"Hitmap",hvs->Get2DHistogram(ss.str()+"/Hitmap"),"COLZ",4);

  }
  om->makeTreeItemSummary("HitMap/HSIO2");
  om->makeTreeItemSummary("DetDetail/HSIO2");
}

int HSIO2converter::GetAnEvent(){
  bool debug=false;
  if(debug) std::cout << "======== starting one event =============="  << std::endl;
  unsigned event_size = 0;
  infile.read((char*)&event_size, 4);
  //  std::cout << "Size of event: " << event_size << std::endl;
  
  // Break loop here if reached end if file
  if(!infile) return 0;
  
  // Event number 4 byte
  unsigned event_number = 0;
  infile.read((char*)&event_number, 4);
  if(debug)std::cout << "Event Number: " << event_number << std::endl;
  thisevent->setFEI4EventNumber(event_number);
  // RCE number 4 byte
  unsigned rce_number = 0;
  infile.read((char*)&rce_number, 4);
  if(debug)std::cout << "RCE Number: " << rce_number << std::endl;
  
  // Trigger information 9*4byte
  char trig_info[9*4];
  infile.read(trig_info, 9*4);
  // To be decoded
  
  event_size -= 12*4; // Subtract data which we already read
  
  // Read FE data
  char *fe_data = new char[event_size];
  infile.read(fe_data, event_size);
  int Link=0;
  //  int L1id=0;
  int Bxid=0;
  unsigned int firstbcid=0;
  std::vector<DAQLinkInfo> linklist; linklist.clear();
  DAQLinkInfo ahli;
  for (unsigned i=0; i<event_size; i+=4) {
    FormattedRecord rec(*(unsigned*)&fe_data[i]);
    if (rec.isHeader()) {
      Link=rec.getLink();
      //      L1id=rec.getL1id();
      Bxid=rec.getBxid();
      bool isnewlink=true;
      for(auto x :linklist){
	if(x.link==Link)isnewlink=false;
      }
      //      std::cout << "test " << Link << std::endl;
      for(auto info : hli){
	if(info.type.substr(0,4)!="FEI4")continue;
	if(info.link==Link){
	  //	  info.PrintInfo();
	  ahli.setInfo(info);
	  break;
	}
      }
      if(isnewlink){
	//	std::cout  << "c1 " << ahli.link << std::endl;
	linklist.push_back(ahli);
	//	std::cout << linklist[linklist.size()-1].link << std::endl;
	firstbcid=rec.getBxid();
      }
      if(rec.getLink()==0 && isnewlink){
	int nskip=(int)rec.getL1id()-tmplastl1id-1;
	if(nskip<0)nskip+=32;
	if(nskip!=0){
	    std::cout << "$$$$$$$$$$$$$$$$$$$$$$$$ HSIO2 skipped " << nskip << " event !!!" << std::endl;
	    std::cout << thisevent->getEventNumber() << " " << event_number << "  L1ID = " << rec.getL1id() << " " <<  tmplastl1id << std::endl;
	  if(nskip!=1&&rec.getL1id()==1){
	    std::cout << "ops... l1id reset to 1 !!" << std::endl;
	  }else{
	    thisevent->setnSkipbyHSIO2(nskip);
	  }
	}
	tmplastl1id=rec.getL1id();
      }

      //      if(debug){
      //      if(Link==0&&isnewlink){
      if(Link!=ahli.link){
	std::cout << "wrong linklist !!" << std::endl;
	std::cout << "Data Header: " ;
	std::cout << "  Link = " << rec.getLink() ;
	std::cout << "  L1ID = " << rec.getL1id() ;
	std::cout << "  BCID = " << rec.getBxid() << std::endl;
	ahli.PrintInfo();
	std::cout << std::endl;
      }
    } else if (rec.isData()) {

      PixelHit ahit(ahli.channel,rec.getCol(),rec.getRow(),rec.getToT(),0,Bxid-firstbcid,ahli.layer,ahli.link,ahli.type,ahli.name,true);
      ahit.SetBCID(rec.getBxid());
      //      std::cout << ahit.DAQLink() << std::endl;
      //      if(Link==4)ahli.PrintInfo();
      //      if(ahit.DAQLink()==3&&(ahit.Col()<80||ahit.Row()>336))ahit.PrintPixelHit();
      thisevent->addFEI4Hits(ahit);
      if(debug){
	//      if(1){
	std::cout << "Data:  link=" << Link << "\t l1id=" << Bxid - firstbcid /*L1id */<< "\t Bxid=" << Bxid << "\t col=" << rec.getCol() << "\t row=" << rec.getRow() << "\t tot=" << rec.getToT() << std::endl;
	//	std::cout << thisevent->getEventNumber() << std::endl;
	
	//	thisevent->getFEI4Hits()[0][0].PrintPixelHit();
	
      }
    }
  }
  std::sort(linklist.begin(),linklist.end(),std::greater<DAQLinkInfo>());
  //  thisevent->SortFEI4Hits();
  //  std::cout << "linklist size = " << linklist.size() << std::endl;
  thisevent->setFEI4Linklist(linklist);
  delete fe_data;
  return 1;
}
int HSIO2converter::GetEvent(int EventNumber){
  return 1;
}
int HSIO2converter::GetNextEvent(){
  return GetAnEvent();
}
void HSIO2converter::FillHistogram(){
  std::stringstream ss;
  //  for(auto x : thisevent->getFEI4Linklist()){
  for(auto x : hli){
    if(x.type.substr(0,4)!="FEI4")continue;
    //  for(unsigned int ily=0;ily<thisevent->getFEI4Hits().size();ily++){
    
    //    std::cout << "testtesttest" << x.link << std::endl;
    ss.str("");
    //    ss<< "DetDetail/HSIO2/FEI4tel" <<  x.link;
    ss<< "DetDetail/HSIO2/FEI4tel" <<  x.layer;
    //    std::cout << ss.str() << std::endl;
    //    std::vector<PixelHit> fei4hit=thisevent->getFEI4Hits(x.link);
    std::vector<PixelHit> fei4hit=thisevent->getFEI4Hits(x.layer);
    hvs->GetHistogram(ss.str()+"/nHits")->Fill(fei4hit.size());
    hvs->GetHistogram(ss.str()+"/nCluster")->Fill(fei4hit.size());
    //    std::cout << fei4hit.size() << std::endl;
    for(auto ihit : fei4hit){
      //      std::cout << ihit.DAQLink() << std::endl;
      //      std::cout << ss.str() << std::endl;
      //      if(ihit.Name()!="KEK101")
      //      if(ihit.DAQLink()==3&&(ihit.Col()<80||ihit.Row()>336))ihit.PrintPixelHit();
      //      if(ihit.DAQLink()==3&&(ihit.Col()<80||ihit.Row()>336))ihit.PrintPixelHit();
      hvs->Get2DHistogram(ss.str()+"/Hitmap")->Fill(ihit.Col(),ihit.Row());
      hvs->GetHistogram(ss.str()+"/TotDist")->Fill(ihit.ToT());
      hvs->GetHistogram(ss.str()+"/L1id")->Fill(ihit.LV1());
    }
  }
}
void HSIO2converter::PrintInfo(){
  for(auto fei4ly : thisevent->getFEI4Hits()){
    for(auto fei4hit : fei4ly){
      fei4hit.PrintPixelHit();
    }
  }
}
