#include "DRS4Converter.hh"

DRS4Converter::DRS4Converter(std::string inputFileName,std::string configName){
  initialize(inputFileName,configName);
}
void DRS4Converter::initialize(std::string inputFileName,std::string configName){
  boardNumber = "1";
  std::cout << "[INFO]: will use calibration files for board number " << boardNumber << "\n";
  ifileName=inputFileName;
  ReadBinFile(inputFileName);
  ReadConfigFile(configName);
  Calibration();
  nGoodEvents=0;
}
void DRS4Converter::ReadBinFile(std::string inputFileName){
  std::cout << "Opening file: " << inputFileName << std::endl;
  //  infile.open(inputFileName.c_str(), std::fstream::in | std::fstream::binary);
  fpin = fopen(inputFileName.c_str(),"r");
  if (!fpin)
    {
      std::cerr << "[ERROR]: !USAGE! Input file does not exist. Please enter valid file name" << std::endl;
      std::cerr << inputFileName << std::endl;
      exit(0);
    }
  std::cout << "... reading file ..." << std::endl;
    
}
void DRS4Converter::ReadConfigFile(std::string configName){
  config = new Config(configName.c_str());
  if ( !config->hasChannels() || !config->isValid() ) {
    std::cerr << "\nFailed to load channel information from config " << configName << std::endl;
    //    return -1;
  }

}

void DRS4Converter::Calibration(){
  //**************************************
  // Load Voltage Calibration
  //**************************************
  
  FILE* fp1;
  char stitle[200];
  int dummy;
  double fdummy;
  std::cout << "\n=== Loading voltage calibration ===\n" << std::endl;
  for( int i = 0; i < 4; i++ ){
    sprintf( stitle, "%s/RawDataConverter/DRS4/share/v1740_bd%s_group_%d_offset.txt",PACKAGE_SOURCE_DIR ,boardNumber.c_str(), i );
      fp1 = fopen( stitle, "r" );
      printf("Loading offset data from %s\n", stitle);

      for( int k = 0; k < 1024; k++ ) {     
          for( int j = 0; j < 9; j++ ){      
              dummy = fscanf( fp1, "%lf ", &off_mean[i][j][k] ); 
          }
      }
      fclose(fp1);
  }

  //**************************************
  // Load Time Calibration
  //**************************************

  std::cout << "\n=== Loading time calibration ===\n" << std::endl;
  for( int i = 0; i < 4; i++ ) {
    sprintf( stitle, "%s/RawDataConverter/DRS4/share/v1740_bd%s_group_%d_dV.txt",PACKAGE_SOURCE_DIR, boardNumber.c_str(), i );
      fp1 = fopen( stitle, "r" );
      printf("Loading dV data from %s\n", stitle);

      for( int k = 0; k < 1024; k++)      
          dummy = fscanf( fp1, "%lf %lf %lf %lf %lf ", 
                  &fdummy, &fdummy, &fdummy, &fdummy, &tcal_dV[i][k] ); 
      fclose(fp1);
  }
  for( int i = 0; i < 4; i++ ) {
      for( int j = 0; j < 1024; j++ )
          dV_sum[i] += tcal_dV[i][j];
  }

  for( int i = 0; i < 4; i++) {
      for( int j = 0; j < 1024; j++) {
          tcal[i][j] = tcal_dV[i][j] / dV_sum[i] * 200.0;
      }
  }
  
}

int DRS4Converter::GetAnEvent(){
  bool drawDebugPulses = false;
  int dummy;
  uint   event_header;
  uint   temp[3];
  ushort samples[9][1024];

  led.event = nGoodEvents; // for output tree
  // first header word
  dummy = fread( &event_header, sizeof(uint), 1, fpin);
  // second header word
  dummy = fread( &event_header, sizeof(uint), 1, fpin);  
  uint grM     = event_header & 0x0f; // 4-bit channel group mask
  // third and fourth header words
  dummy = fread( &event_header, sizeof(uint), 1, fpin);  
  dummy = fread( &event_header, sizeof(uint), 1, fpin);  
  
  // check for end of file
  if (feof(fpin)) return -1;
  
  //*************************
  // Parse group mask into channels
  //*************************
  
  bool _isGR_On[4];
  _isGR_On[0] = (grM & 0x01);
  _isGR_On[1] = (grM & 0x02);
  _isGR_On[2] = (grM & 0x04);
  _isGR_On[3] = (grM & 0x08);
  
  int activeGroupsN = 0;
  int realGroup[4] = {-1, -1, -1, -1};
  for ( int l = 0; l < 4; l++ ) {
    if ( _isGR_On[l] ) 
      {
	realGroup[activeGroupsN] = l; 
	activeGroupsN++;
      }
  }
    
  //************************************
  // Loop over channel groups
  //************************************
  
  for ( int group = 0; group < activeGroupsN; group++ ) {
    // Read group header
    dummy = fread( &event_header, sizeof(uint), 1, fpin);  
    ushort tcn = (event_header >> 20) & 0xfff; // trigger counter bin
    led.tc[realGroup[group]] = tcn;
    
    // Check if all channels were active (if 8 channels active return 3072)
    int nsample = (event_header & 0xfff) / 3;
    
    // Define time coordinate
    led.time[realGroup[group]][0] = 0.0;
    for( int i = 1; i < 1024; i++ ){
      led.time[realGroup[group]][i] = float(i);
      led.time[realGroup[group]][i] = float(tcal[realGroup[group]][(i-1+tcn)%1024] 
					+ led.time[realGroup[group]][i-1]);
    }      
    
    //************************************
    // Read sample info for group
    //************************************      
    
    for ( int i = 0; i < nsample; i++ ) {
      dummy = fread( &temp, sizeof(uint), 3, fpin );  
      samples[0][i] =  temp[0] & 0xfff;
      samples[1][i] = (temp[0] >> 12) & 0xfff;
      samples[2][i] = (temp[0] >> 24) | ((temp[1] & 0xf) << 8);
      samples[3][i] = (temp[1] >>  4) & 0xfff;
      samples[4][i] = (temp[1] >> 16) & 0xfff;
      samples[5][i] = (temp[1] >> 28) | ((temp[2] & 0xff) << 4);
      samples[6][i] = (temp[2] >>  8) & 0xfff;
      samples[7][i] =  temp[2] >> 20;	
    }
    
    // Trigger channel
    for(int j = 0; j < nsample/8; j++){
      fread( &temp, sizeof(uint), 3, fpin);  
      samples[8][j*8+0] =  temp[0] & 0xfff;
      samples[8][j*8+1] = (temp[0] >> 12) & 0xfff;
      samples[8][j*8+2] = (temp[0] >> 24) | ((temp[1] & 0xf) << 8);
      samples[8][j*8+3] = (temp[1] >>  4) & 0xfff;
      samples[8][j*8+4] = (temp[1] >> 16) & 0xfff;
      samples[8][j*8+5] = (temp[1] >> 28) | ((temp[2] & 0xff) << 4);
      samples[8][j*8+6] = (temp[2] >>  8) & 0xfff;
      samples[8][j*8+7] =  temp[2] >> 20;
    }
    
    //************************************
    // Loop over channels 0-8
    //************************************      
    
    for(int i = 0; i < 9; i++) {
      
      int totalIndex = realGroup[group]*9 + i;
      
      // Do not analyze disabled channels
      if ( !config->hasChannel(totalIndex) ) {
	for ( int j = 0; j < 1024; j++ ) {
	  led.raw[totalIndex][j] = 0;
	  led.channel[totalIndex][j] = 0;
	  
	}
	led.xmin[totalIndex] = 0.;
	led.amp [totalIndex] = 0.;
	led.base[totalIndex] = 0.;
	led.integral[totalIndex] = 0.;
	led.integralFull[totalIndex] = 0.;
	led.gauspeak[totalIndex] = 0.;
	led.sigmoidTime[totalIndex] = 0.;
	led.linearTime0[totalIndex] = 0.;
	led.linearTime15[totalIndex] = 0.;
	led.linearTime30[totalIndex] = 0.;
	led.linearTime45[totalIndex] = 0.;
	led.linearTime60[totalIndex] = 0.;
	led.risetime[totalIndex] = 0.;
	led.constantThresholdTime[totalIndex] = 0.;
	continue;
      }
      
      // Fill pulses
      for ( int j = 0; j < 1024; j++ ) {
	led.raw[totalIndex][j] = (short)(samples[i][j]);
	led.channel[totalIndex][j] = (short)((double)(samples[i][j]) 
					 - (double)(off_mean[realGroup[group]][i][(j+tcn)%1024]));
      }
      
      // Make pulse shape graph
      TString pulseName = Form("pulse_event%d_group%d_ch%d", led.event, realGroup[group], i);
      TGraphErrors* pulse = new TGraphErrors( GetTGraph( led.channel[totalIndex], led.time[realGroup[group]] ) );
      
      // Estimate baseline
      float baseline;
      baseline = GetBaseline( pulse, 5 ,150, pulseName );
      led.base[totalIndex] = baseline;
      
      // Correct pulse shape for baseline offset + amp/att
      for(int j = 0; j < 1024; j++) {
	float multiplier = config->getChannelMultiplicationFactor(totalIndex);
	led.channel[totalIndex][j] = multiplier * (short)((double)(led.channel[totalIndex][j]) + baseline);
      }
      
      //Apply HighPass Filter (clipping circuit)
      //HighPassFilter( channel[totalIndex], channelFilter[totalIndex],  time[realGroup[group]], 1000., 22.*1e-12 );
      
      //Apply Notch Filter
      //NotchFilter( channel[totalIndex], channelFilter[totalIndex],  time[realGroup[group]], 10, 10.*1e-12, 5.*1e-7 );
      
      // Find the absolute minimum. This is only used as a rough determination 
      // to decide if we'll use the early time samples
      // or the late time samples to do the baseline fit
      //std::cout << "---event "  << event << "-------ch#: " << totalIndex << std::endl;
      
      int index_min = FindMinAbsolute(1024, led.channel[totalIndex]);//Short version
      //int index_min = FindMinAbsolute(1024, channelFilter[totalIndex]);//Float version
      
      // DRS-glitch finder: zero out bins which have large difference
      // with respect to neighbors in only one or two bins
      for(int j = 0; j < 1024; j++) {
	short a0 = abs(led.channel[totalIndex][j-1]);
	short a1 = abs(led.channel[totalIndex][j]);
	short a2 = abs(led.channel[totalIndex][j+1]);
	short a3 = abs(led.channel[totalIndex][j+2]);
	
	if ( ( a1>3*a0 && a2>3*a0 && a2>3*a3 && a1>30) )
	  {
	    led.channel[totalIndex][j] = 0;
	    led.channel[totalIndex][j+1] = 0;
	  }
	
	if ( ( a1>3*a0 && a1>3*a2 && a1>30) )
	  led.channel[totalIndex][j] = 0;
      }
      
      // Recreate the pulse TGraph using baseline-subtracted channel data
      delete pulse;
      
      pulse = new TGraphErrors( GetTGraph( led.channel[totalIndex], led.time[realGroup[group]] ) );//Short Version
      //pulse = new TGraphErrors( *GetTGraph( channelFilter[totalIndex], time[realGroup[group]] ) );//Float Version
      led.xmin[totalIndex] = index_min;
      
      float filterWidth = config->getFilterWidth(totalIndex);
      if (filterWidth) {
	WeierstrassTransform( led.channel[totalIndex], led.channelFilter[totalIndex], led.time[realGroup[group]], 
			      pulseName, filterWidth);
	pulse = WeierstrassTransform( led.channel[totalIndex], led.time[realGroup[group]], 
				      pulseName, filterWidth, false );
      }
      
      //Compute Amplitude : use units V
      Double_t tmpAmp = 0.0;
      Double_t tmpMin = 0.0;
      pulse->GetPoint(index_min, tmpMin, tmpAmp);
      led.amp[totalIndex] = tmpAmp * (1.0 / 4096.0); 
      
      // Get pulse integral
      if ( led.xmin[totalIndex] != 0 ) {
	led.integral[totalIndex] = GetPulseIntegral( index_min, 10, 25, led.channel[totalIndex], led.time[realGroup[group]] );
	led.integralFull[totalIndex] = GetPulseIntegral( index_min , led.channel[totalIndex], "full");
      }
      else {
	led.integral[totalIndex] = 0.0;
	led.integralFull[totalIndex] = 0.0;
      }
      
      // Gaussian time stamp and constant-fraction fit
      Double_t min = 0.; Double_t low_edge = 0.; Double_t high_edge = 0.; Double_t y = 0.; 
      pulse->GetPoint(index_min, min, y);	
      pulse->GetPoint(index_min-4, low_edge, y); // time of the low edge of the fit range
      pulse->GetPoint(index_min+4, high_edge, y);  // time of the upper edge of the fit range	
      
      float timepeak   = 0;
      bool isTrigChannel = ( totalIndex == 8 || totalIndex == 17 
			     || totalIndex == 26 || totalIndex == 35 );
      float fs[6]; // constant-fraction fit output
      float fs_falling[6]; // falling exp timestapms
      if ( !isTrigChannel ) {
      	if( drawDebugPulses ) {
      	  if ( led.xmin[totalIndex] != 0.0 ) {
      	    timepeak =  GausFit_MeanTime(pulse, low_edge, high_edge, "plots/"+pulseName);
      	    RisingEdgeFitTime( pulse, index_min, 0.2, 0.80, fs, led.event, "plots/linearFit_" + pulseName, 
	    		       true );
      	    //RisingEdgeFitTime( pulse, index_min, fs, event, "linearFit_" + pulseName, true );
      	    //TailFitTime( pulse, index_min, fs_falling, event, "expoFit_" + pulseName, true );
      	    //sigmoidTime[totalIndex] = SigmoidTimeFit( pulse, index_min, event, "linearFit_" + pulseName, true );
      	    //fullFitTime[totalIndex] = FullFitScint( pulse, index_min, event, "fullFit_" + pulseName, true );
      	  }
      	}
      	else {
      	  if ( led.xmin[totalIndex] != 0.0 ) {
	    //       	    timepeak =  GausFit_MeanTime(pulse, low_edge, high_edge);
      	    RisingEdgeFitTime( pulse, index_min, 0.2, 0.60, fs, led.event, "plots/linearFit_" + pulseName, 
	    		       false );
      	    //RisingEdgeFitTime( pulse, index_min, fs, event, "linearFit_" + pulseName, false );
      	    //TailFitTime( pulse, index_min, fs_falling, event, "expoFit_" + pulseName, false );
      	    //sigmoidTime[totalIndex] = SigmoidTimeFit( pulse, index_min, event, "linearFit_" + pulseName, false );
      	    //fullFitTime[totalIndex] = FullFitScint( pulse, index_min, event, "fullFit_" + pulseName, false );
      	  }
      	}
      }
      
      else {
      	for ( int kk = 0; kk < 5; kk++ )
      	  {
      	    fs[kk] = -999;
      	    fs_falling[kk] = -999;
      	  }
      }
      
      led._isRinging[totalIndex] = isRinging( index_min, led.channel[totalIndex] );
      // for output tree
      led.gauspeak[totalIndex] = timepeak;
      led.risetime[totalIndex] = fs[0];
      led.linearTime0[totalIndex] = fs[1];
      led.linearTime15[totalIndex] = fs[2];
      led.linearTime30[totalIndex] = fs[3];
      led.linearTime45[totalIndex] = fs[4];
      led.linearTime60[totalIndex] = fs[5];
      led.fallingTime[totalIndex] = fs_falling[0];
      led.constantThresholdTime[totalIndex] = ConstantThresholdTime( pulse, 50);
      
      delete pulse;
    }
    
    dummy = fread( &event_header, sizeof(uint), 1, fpin);
  }
  
  //  tree->Fill();
  nGoodEvents++;
  return 1;
}
int DRS4Converter::GetEvent(int EventNumber){
  ReadBinFile(ifileName);
  for(int ii=0;ii<EventNumber;ii++)GetAnEvent();
  return GetAnEvent();
}
int DRS4Converter::GetNextEvent(){
  return GetAnEvent();
}

void DRS4Converter::SetThisEvent(TrackHitEvent *_eve){
  thisevent=_eve;
}

void DRS4Converter::SetHistogram(HistogramVariableStrage *_hvs){
  hvs=_hvs;
  std::stringstream ss;
  for(int ich=0;ich<36;ich++){
    if(ich!=5&ich!=6&ich!=7&ich!=20&ich!=22&ich!=23&ich!=24)continue;
    ss.str("");
    ss<< "DetDetail/DRS4/AmpCh" <<  ich << "/";
    hvs->AddHistogram((ss.str()+"ADCdist"),100,-0.1,1.0,0.1);
    hvs->AddHistogram((ss.str()+"MPTime"),20,0,200,0);
  }
}
void DRS4Converter::SetHistogram(HistogramVariableStrage *_hvs,OnlineMonWindow *om){
  SetHistogram(_hvs);
  std::stringstream ss;
  for(auto y : hvs->GetHistName()){
    om->registerTreeItem(y);
    if(y.find("ADCdist")!=std::string::npos)
      om->registerHisto(y,hvs->GetHistogram(y),"HIST",2);
    else
      om->registerHisto(y,hvs->GetHistogram(y),"HIST",0);
  }
//  for(auto y : hvs->Get2DHistName()){
//    om->registerTreeItem(y);
//    om->registerHisto(y,hvs->GetHistogram(y),"COLZ",4);
//  }
  for(int ich=0;ich<36;ich++){
    if(ich!=5&ich!=6&ich!=7&ich!=20&ich!=22&ich!=23&ich!=24)continue;
    ss.str("");
    ss<< "DetDetail/DRS4/AmpCh" <<  ich;
    om->makeTreeItemSummary(ss.str());
  }
  om->makeTreeItemSummary("DetDetail/DRS4");

}

void DRS4Converter::FillHistogram(){
  std::stringstream ss;
  for(int ich=0;ich<36;ich++){
    if(ich!=5&ich!=6&ich!=7&ich!=20&ich!=22&ich!=23&ich!=24)continue;
    ss.str("");
    ss<< "DetDetail/DRS4/AmpCh" <<  ich << "/";
    double maxvalue=-1;
    int imax=0;
    for(int ii=0;ii<1024;ii++){
      //      hvs->GetHistogram(ss.str())->Fill(1.2);
      if(maxvalue<led.channel[ich][ii]/4096.){
	maxvalue=led.channel[ich][ii]/4096.;
	imax=ii;
      }
    }
    hvs->GetHistogram((ss.str()+"ADCdist"))->Fill(maxvalue);
    if(maxvalue>0.1)
      hvs->GetHistogram((ss.str()+"MPTime"))->Fill(imax*200./1024.);
  }
}
void DRS4Converter::FillThisEvent(){
  int iadcstrip[3]={5,6,7};
  int iadc2x2s1[2]={23,24};
  int iadc2x2s2=22;
  for(auto h : hli){
    if(h.type.substr(0,4)!="LGAD")continue;
    if(h.name=="LGAD50D"){
      for(int ich=0;ich<3;ich++){
	int ichadc=iadcstrip[ich];
	bool ishit=false;
	for(int ii=0;ii<1024;ii++){
	  //led.channel[iadcstrip[ich]][ii]; selection to fill LGAD hit in thisevent
	}
	if(ishit){
	  LGADHit tmphit;
	  tmphit.SetLGADHit(ichadc,ichadc,0,&led.channel[ichadc][0],h.layer,h.type,h.name);
	  thisevent->addLGADHits(tmphit);
	}
      }
    }else if(h.name=="LGAD2x2s1"){
      for(int ich=0;ich<2;ich++){
	int ichadc=iadc2x2s1[ich];
	bool ishit=false;
	for(int ii=0;ii<1024;ii++){
	  //led.channel[iadc2x2s1[ich]][ii]; selection to fill LGAD hit in thisevent
	}
	if(ishit){
	  LGADHit tmphit;
	  tmphit.SetLGADHit(ichadc,ichadc,0,&led.channel[ichadc][0],h.layer,h.type,h.name);
	  thisevent->addLGADHits(tmphit);
	}
      }
    }else if(h.name=="LGAD2x2s2"){
      int ichadc=iadc2x2s2;
      bool ishit=false;
      for(int ii=0;ii<1024;ii++){
	//led.channel[iadc2x2s1[ich]][ii]; selection to fill LGAD hit in thisevent
      }
      if(ishit){
	LGADHit tmphit;
	tmphit.SetLGADHit(ichadc,ichadc,0,&led.channel[ichadc][0],h.layer,h.type,h.name);
	thisevent->addLGADHits(tmphit);
      }
    }else{
      std::cout << "uknown LGAD name " << h.name << std::endl;
    }
  }
}

void DRS4Converter::PrintInfo(){
  std::cout << "Event Number : " << led.event << std::endl;
  int jj=0; int imax=-1;
  for(int ii=100;ii<105;ii++){
    if(imax<led.channel[22][ii]){
      imax=ii;
      jj=ii;
    }
  }
  std::cout << led.raw[22][jj] << " " << led.channel[22][jj] << std::endl;
}
