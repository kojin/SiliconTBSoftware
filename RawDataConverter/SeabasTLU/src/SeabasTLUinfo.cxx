#include "SeabasTLU/SeabasTLUinfo.hh"

SeabasTLUinfo::SeabasTLUinfo(int evtnum,int *xx){
  m_EventNumber=evtnum;
  if((xx[0]&0xff)!=0xff || 
     (xx[1]&0xff)!=0xff || 
     (xx[2]&0xff)!=0xff || 
     (xx[3]&0xff)!=0xff){
    std::cerr << "strange header of data " << std::endl;
    for(int ii=0;ii<11;ii++)std::cout << std::hex<< xx[ii] << " " ;
    std::cout << std::dec << std::endl;
    return;
  }
  m_MPPC=xx[4]&0x0f;
  m_TrigID=xx[5]*256+xx[6];
  m_TimeStamp=xx[8]*256*256+xx[9]*256+xx[10];
}
