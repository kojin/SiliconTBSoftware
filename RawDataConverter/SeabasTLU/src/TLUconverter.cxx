#include "SeabasTLU/TLUconverter.hh"

TLUconverter::TLUconverter(std::string filename){
  infile.open(filename.c_str(), std::ios::in | std::ios::binary);
  if(!infile.is_open()){
    std::cerr << "file :" << filename << " cannot be opened." << std::endl;
  }
  //  int indata;
  infile.read((char*)&runnumber,sizeof(int));
  infile.read((char*)&starttime,sizeof(time_t));
  for(int ii=0;ii<4;ii++){
    m_MPPCdeno[ii]=0.;
    m_MPPCnume[ii]=0.;
  }
}
TLUconverter::~TLUconverter(){
  infile.close();
}

void TLUconverter::SetHistogram(HistogramVariableStrage *_hvs){
  hvs=_hvs;
  hvs->AddHistogram("SeabasTLU/MPPChit",4,-0.5,3.5);
  hvs->AddHistogram("SeabasTLU/MPPCeffi",4,-0.5,3.5);
  hvs->AddHistogram("SeabasTLU/TriggerID",100,0,pow(2,16));
  hvs->AddHistogram("SeabasTLU/TimeStamp",100,0,pow(2,24));
}
void TLUconverter::SetHistogram(HistogramVariableStrage *_hvs,OnlineMonWindow *om){
  SetHistogram(_hvs);
  for(auto x : hvs->GetHistName()){
    om->registerTreeItem(x);
    om->registerHisto(x,hvs->GetHistogram(x),"HIST",0);
  }
  for(auto y : hvs->Get2DHistName()){
    om->registerTreeItem(y);
    om->registerHisto(y,hvs->Get2DHistogram(y),"COLZ",0);
  }
  om->makeTreeItemSummary("SeabasTLU");
}
void TLUconverter::FillHistogram(){
  int mppc=stlui.MPPC();
  int mh[4]={0,0,0,0};
  
  for(int ii=0;ii<4;ii++){
    if((mppc&0xf)%2){
      hvs->GetHistogram("SeabasTLU/MPPChit")->Fill(3-ii);
      mh[3-ii]=1;
    }
    mppc=mppc>>1;
  }
  if(0){
    for(int ii=0;ii<4;ii++){
      std::cout << mh[ii] << " ";
    }
    std::cout << std::endl;
  }

  for(int ii=0;ii<4;ii++){
    bool deno=true;
    for(int jj=0;jj<4;jj++){
      if(jj==ii)continue;
      if(jj==2) continue;
      if(!mh[jj])deno=false;
    }
    if(deno){
      //      std::cout << "is deno for " << ii << std::endl;
      m_MPPCdeno[ii]+=1.;
      if(mh[ii])m_MPPCnume[ii]+=1.;
    }
    if(m_MPPCdeno[ii]>0){
      //      std::cout << m_MPPCnume[ii]<< " " << m_MPPCdeno[ii] << std::endl;
      hvs->GetHistogram("SeabasTLU/MPPCeffi")->SetBinContent(ii+1,(double)m_MPPCnume[ii]/m_MPPCdeno[ii]);
      //      hvs->GetHistogram("SeabasTLU/MPPCeffi")->SetBinContent(ii+1,m_MPPCdeno[ii]);
    }
  }

  hvs->GetHistogram("SeabasTLU/TriggerID")->Fill(stlui.TrigID());
  hvs->GetHistogram("SeabasTLU/TimeStamp")->Fill(stlui.TimeStamp());
}

int TLUconverter::GetAnEvent(int &eventnum,int *xx){
  eventnum=-1;
  infile.read((char*)&eventnum,sizeof(int));
  //  std::cout << eventnum << std::endl;
  if(infile){
    for(int ii=0;ii<11;ii++){
      xx[ii]=-1;
      infile.read((char*)&xx[ii],sizeof(char));
      xx[ii]=xx[ii]<0?xx[ii]+256:xx[ii];
    }
    return 1;
  } else{
    return 0;
  }
}
int TLUconverter::GetEvent(int EventNumber){
  int eventnumber=-1;
  int xx[11]={-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  while(1){
    int isOK=GetAnEvent(eventnumber,xx);
    if(!isOK)return 0;
    if(eventnumber==EventNumber)break;
  }

  SeabasTLUinfo tmpinfo(eventnumber,xx);
  stlui=tmpinfo;
  return 1;
  //////// print event 
  //  std::cout << "Event " << eventnumber << "  : " ;
  //  for(int ii=0;ii<6;ii++) std::cout << std::hex << std::setw(2)<< xx[ii] << " ";
  //  std::cout << std::dec << std::endl;
}

int TLUconverter::GetNextEvent(){
  int eventnumber=-1;    int xx[11]={-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  int isOK=GetAnEvent(eventnumber,xx);
  if(!isOK)return 0 ;
  SeabasTLUinfo tmpinfo(eventnumber,xx);
  stlui=tmpinfo;
  return 1;
  //////// print event 
  //  std::cout << "Event " << eventnumber << " " ;
  //  for(int ii=0;ii<6;ii++)std::cout << std::hex << std::setw(2)<< xx[ii] << " ";
  //  std::cout << std::dec << std::endl;
}
void TLUconverter::PrintTLUinfo(){
  std::cout << "Event " << stlui.EventNumber() << " "
	    << "MPPC " << std::bitset<4>(stlui.MPPC()) << std::dec<< " " 
	    << "TrigID " << stlui.TrigID() << " " 
	    << "TimeStamp " << stlui.TimeStamp() << std::endl;
}

