#include "XpressK7/XpressK7converter.hh"

XpressK7converter::XpressK7converter(std::string filename, int link){
  RD53Alink=link;
  eventnumber=-1;
  thiseventtag=0;
  file.open(filename, std::fstream::in | std::fstream::binary);
  file.seekg(0, std::ios_base::end);
  int size = file.tellg();
  file.seekg(0, std::ios_base::beg);
  std::cout << "file : " << filename << " openend" << std::endl;
  std::cout << "Size: " << size/1024.0/1024.0 << " MB" << std::endl;
}
XpressK7converter::~XpressK7converter(){
  file.close();
}
void XpressK7converter::SetHistogram(HistogramVariableStrage *_hvs){
  hvs=_hvs;
  std::stringstream ss ;
  ss.str("");
  ss << "DetDetail/XpressK7/Port"<<RD53Alink;
  std::stringstream ss2 ;
  ss2.str("");
  ss2 << "HitMap/XpressK7/Port"<<RD53Alink;
  hvs->Add2DHistogram(ss.str()+"/Hitmap",401,-0.5,400.5,193,-0.5,192.5);
  hvs->AddHistogram(ss.str()+"/TotDist",16,-0.5,15.5);
  hvs->AddHistogram(ss.str()+"/L1id",32,-0.5,31.5);
  hvs->AddHistogram(ss.str()+"/nHits",50,-0.5,49.5);
  hvs->AddHistogram(ss.str()+"/nCluster",30,-0.5,29.5);
  hvs->AddHistogram(ss.str()+"/hitperkeve",10000,0,10e6);
}





void XpressK7converter::SetHistogram(HistogramVariableStrage *_hvs,OnlineMonWindow *om){
  hvs=_hvs;
  SetHistogram(hvs);
  std::stringstream ss ;
  ss.str("");
  ss << "DetDetail/XpressK7/Port"<<RD53Alink;
  std::stringstream ss2 ;
  ss2.str("");
  ss2 << "HitMap/XpressK7/Port"<<RD53Alink;
  for(auto x : hvs->GetHistName()){
    om->registerTreeItem(x);
    om->registerHisto(x,hvs->GetHistogram(x),"HIST",0);
  }
  for(auto y : hvs->Get2DHistName()){
    om->registerTreeItem(y);
    om->registerHisto(y,hvs->Get2DHistogram(y),"COLZ",4);
  }
  om->registerTreeItem(ss2.str()+"Hitmap");
  om->registerHisto(ss2.str()+"Hitmap",hvs->Get2DHistogram(ss.str()+"/Hitmap"),"COLZ",4);

  om->makeTreeItemSummary(ss.str());
  om->makeTreeItemSummary("DetDetail/XpressK7");
  om->makeTreeItemSummary("HitMap/XpressK7");

}

void XpressK7converter::FillHistogram(){
  //  hvs->GetHistogram("SeabasTLU/TriggerID")->Fill(stlui.TrigID());
  //  hvs->GetHistogram("SeabasTLU/TimeStamp")->Fill(stlui.TimeStamp());
  bool debug=false;
  std::stringstream ss ;
  ss.str("");
  ss << "DetDetail/XpressK7/Port"<<RD53Alink;

  for( auto x : rd53aeve){
    if(x.nHits>50)continue;
    if(x.nHits)hvs->GetHistogram(ss.str()+"/nHits")->Fill(x.nHits);
    if(x.nClusters)hvs->GetHistogram(ss.str()+"/nCluster")->Fill(x.nClusters);
    if(debug)std::cout << "BCID = " << x.bcid << " nHits = " << x.nHits << " nCluster = " << x.nClusters<< std::endl;
    if(x.nHits>0){
      int ilv1=0;
      for(auto h : x.hits){
	hvs->GetHistogram(ss.str()+"/hitperkeve")->Fill(eventnumber);
	hvs->GetHistogram(ss.str()+"/L1id")->Fill(lv1[ilv1]);
	hvs->GetHistogram(ss.str()+"/TotDist")->Fill(h.tot);
	hvs->Get2DHistogram(ss.str()+"/Hitmap")->Fill(h.col,h.row);
	ilv1++;
	if(debug)std::cout << "L1id=" << x.bcid << "-" << rd53aeve[0].bcid << " col = " << h.col << " row=" << h.row << std::endl;
      }
    }
  }
}
int XpressK7converter::GetAnEvent(){
  bool debug=false;
  if(debug) std::cout << " start reading an event for RD53A" << std::endl;
  const std::array<unsigned, 16> l1ToTag = {{0,0,1,1,1,1,2,2,2,2,3,3,3,3,0,0}};


  rd53aeve.clear();
  lv1.clear();
  int count = 0;
  int nonZero_cnt = 0;
  int plotIt = 0;
  int old_bcid = 0;
  int other_old_bcid = 0;
  int max_bcid = 0;
  int trigger = 0;
  int l1_count = 0;
  
  //  Rd53aEvent *multiEvent=NULL;
  Rd53aEvent *multiEvent=NULL;


  int link=1;
  int iRD53Alayer=0;
  int layer=0;
  DAQLinkInfo thislink;
  for(auto h : hli){
    if(h.type.substr(0,5)=="RD53A"){
      if(debug)std::cout << "found RD53A module at layer " << h.layer << std::endl;
      iRD53Alayer++;
      layer=h.layer;
      if(h.link==RD53Alink)thislink=h;
    }
  }
  //  if(iRD53Alayer>1)std::cout << "more than one RD53A layer ??" << std::endl;

  while(1){
    Rd53aEvent *event=new Rd53aEvent();
    event->fromFileBinary(file);
    // Skip if not valid event
    if (l1ToTag[event->l1id%16] != event->tag) {
      //      skipped++;
      if(0)std::cout << "skipped xpressk7 tag : " << l1ToTag[event->l1id%16] << " " <<  event->tag << std::endl;
      continue;
    }
    if (!file)return 0;

    if(multiEvent==NULL){
      multiEvent = event;
      for(auto x : event->hits) lv1.push_back(event->l1id);
      if(debug){
	if(multiEvent->hits.size()!=0){
	  std::cout << "event created " << multiEvent->hits.size() << " " << event->l1id << std::endl;
	}
      }
    }else{
      multiEvent->addEvent(*event);
      for(auto x : event->hits) lv1.push_back(event->l1id);
      if(debug){
	if(multiEvent->hits.size()!=0){
	  std::cout << "event added " << multiEvent->hits.size() << " " << event->l1id << std::endl;
	}
      }
      delete event;
    }
    l1_count++;

    // First event should have l1id 0/16
    if (l1_count == 1 && event->l1id%16 != 0) {
      std::cout << "... wierd first event does not have the right l1id" << std::endl;
    }
    // Print event
    if(debug)std::cout << "L1 count: " << l1_count << " at event " << count << " L1ID(" << event->l1id <<") BCID(" << event->bcid << ") TAG(" << event->tag << ") HITS(" << event->nHits << ")" << std::endl;
    
    // Start new multi-event container after 16 events

    if (l1_count == 16) {
      //      //      eventList.push_back(multiEvent);
      if(multiEvent->hits.size()!=0 && debug)std::cout << "nHits in this event = " <<  multiEvent->hits.size() << std::endl;
      if(multiEvent->hits.size()!=lv1.size())std::cout << "strange lv1 infor" << std::endl;
      int ilv1=0;
      for(auto hit : multiEvent->hits){
	//	PixelHit ahit(0,hit.col,hit.row,hit.tot,0,lv1[ilv1],layer,link,"RD53A","KEKRD53A-8",true);
	PixelHit ahit(0,hit.col,hit.row,hit.tot,0,lv1[ilv1],thislink.layer,thislink.link,thislink.type,thislink.name,true);
	ahit.SetBCID(event->bcid);
	//	ahit.PrintPixelHit();
	thisevent->addRD53AHits(ahit);
	ilv1++;
      }
      //      trigger++;
      rd53aeve.push_back(*multiEvent);
      break;
    }
  }
  l1_count = 0;
  //  multiEvent = NULL;

  eventnumber++;
  return 1;
}
int XpressK7converter::GetNextEvent(){
  return GetAnEvent();
}


void XpressK7converter::PrintXpressK7info(){
  std::cout << "Event " << eventnumber << " " << std::endl;
  for( auto x : rd53aeve){
    std::cout << "l1id " << x.l1id << " " 
	      << "bcid " << x.bcid << " "
	      << "tag " << x.tag << " " 
	      << "nHits " << x.nHits << " " 
	      << "nClusters " << x.nClusters << std::endl;
  }
}
