#ifndef RD53AEVENTDATA_H
#define RD53AEVENTDATA_H

// #################################
// # Author: Timon Heim
// # Email: timon.heim at cern.ch
// # Project: Yarr
// # Description: Container for Rd53a data
// # Comment: Splits events by L1ID
// ################################

#include <deque>
#include <list>
#include <vector>
#include <string>
#include <unistd.h>

#include "EventDataBase.h"
#include "LoopStatus.h"

struct Rd53aHit {
    uint16_t col;
    uint16_t row;
    uint16_t tot;
};

class Rd53aCluster {
    public:
        Rd53aCluster() {
            nHits = 0;
        }
        ~Rd53aCluster() {}

        void addHit(Rd53aHit* hit) {
            hits.push_back(hit);
            nHits++;
        }

        unsigned getColLength() {
            int min = 999999;
            int max = -1;
            for (unsigned i=0; i<hits.size(); i++) {
                if ((int)hits[i]->col > max)
                    max = hits[i]->col;
                if ((int)hits[i]->col < min)
                    min = hits[i]->col;
            }
            return max-min+1;
        }
        
        unsigned getRowWidth() {
            int min = 999999;
            int max = -1;
            for (unsigned i=0; i<hits.size(); i++) {
                if ((int)hits[i]->row > max)
                    max = hits[i]->row;
                if ((int)hits[i]->row < min)
                    min = hits[i]->row;
            }
            return max-min+1;
        }

        unsigned nHits;
        std::vector<Rd53aHit*> hits;
};

class Rd53aEvent {
    public:
        Rd53aEvent() {
            tag = 0;
            l1id = 0;
            bcid = 0;
            nHits = 0;
            nClusters = 0;
        }
        Rd53aEvent(unsigned arg_tag, unsigned arg_l1id, unsigned arg_bcid) {
            tag = arg_tag;
            l1id = arg_l1id;
            bcid = arg_bcid;
            nHits = 0;
            nClusters = 0;
        }
        ~Rd53aEvent() {
            //while(!hits.empty()) {
                //Rd53aHit *tmp = hits.back();
                //hits.pop_back();
                //delete tmp;
            //}
        }

        void addHit(unsigned arg_row, unsigned arg_col, unsigned arg_tot) {
            struct Rd53aHit tmp;// = {arg_col, arg_row, arg_tot};
            tmp.col = arg_col;
            tmp.row = arg_row;
            tmp.tot = arg_tot;
            //tmp.unused = 0;
            hits.push_back(Rd53aHit(tmp));
            nHits++;
        }

        void addEvent(const Rd53aEvent &event) {
            hits.insert(hits.end(), event.hits.begin(), event.hits.end());
            nHits+=event.nHits;
        }

        void doClustering();

        void toFileBinary(std::fstream &handle);
        void fromFileBinary(std::fstream &handle);

        uint16_t l1id;
        uint16_t bcid;
        uint32_t tag;
        uint16_t nHits;
        uint16_t nClusters;
        std::list<Rd53aHit> hits;
        std::vector<Rd53aCluster> clusters;
};

class Rd53aData : public EventDataBase {
    public:
        static const unsigned numServiceRecords = 32;
        Rd53aData() {
            //curEvent = NULL;
            for(unsigned i=0; i<numServiceRecords; i++)
                serviceRecords.push_back(0);
        }
        ~Rd53aData() {
            //while(!events.empty()) {
            //    Rd53aEvent *tmp = events.back();
            //    events.pop_back();
            //     delete tmp;
            //}
        }

        void newEvent(unsigned arg_tag, unsigned arg_l1id, unsigned arg_bcid) {
            events.push_back(Rd53aEvent(arg_tag, arg_l1id, arg_bcid));
            curEvent = &events.back();
        }

        void delLastEvent() {
            //delete curEvent;
            events.pop_back();
            curEvent = &events.back();
        }

        void toFile(std::string filename);

        Rd53aEvent *curEvent;

        LoopStatus lStat;
        std::list<Rd53aEvent> events;
        std::vector<int> serviceRecords;
};



#endif
