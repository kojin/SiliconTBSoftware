#ifndef __XpressK7converter_HH__
#define __XpressK7converter_HH__
#include <iostream>
#include <fstream>
#include <sstream>
#include "XpressK7/Rd53aEventData.h"
#include "TrackHitEvent/TrackHitEvent.hh"
#include "OnlineMonWindow/OnlineMonWindow.hh"
#include "HistogramTools/HistogramVariableStrage.hh"

class XpressK7converter{
private:
  std::fstream file;
  std::vector<Rd53aEvent> rd53aeve;
  int eventnumber;
  unsigned int thiseventtag;
  Rd53aEvent lastbcid;
  HistogramVariableStrage *hvs;
  TrackHitEvent *thisevent;
  std::vector<DAQLinkInfo> hli;
  std::vector<int> lv1;
  int RD53Alink;
public:

  XpressK7converter(){}
  XpressK7converter(std::string filename,int link=0);
  ~XpressK7converter();
  void SetHistogram(HistogramVariableStrage *_hvs);
  void SetHistogram(HistogramVariableStrage *_hvs,OnlineMonWindow *om);
  void FillHistogram();
  void SetThisEvent(TrackHitEvent *_eve){thisevent=_eve;}
  void SetDAQInfo(std::vector<DAQLinkInfo>_hli){hli=_hli;}
  int GetAnEvent();
  int GetEvent(int EventNumber){return 1;}
  int GetNextEvent();

  int EventNumber(){return eventnumber;}
  std::vector<Rd53aEvent> GetXpressK7info(){return rd53aeve;}
  void PrintXpressK7info();

};



#endif
