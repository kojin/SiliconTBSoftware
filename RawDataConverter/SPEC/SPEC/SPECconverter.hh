#ifndef __SPECconverter_HH__
#define __SPECconverter_HH__
#include <iostream>
#include <fstream>
#include <sstream>
#include "SPEC/Fe65EventData.h"
#include "TrackHitEvent/TrackHitEvent.hh"
#include "OnlineMonWindow/OnlineMonWindow.hh"
#include "HistogramTools/HistogramVariableStrage.hh"

class SPECconverter{
private:
  std::fstream file;
  std::vector<Fe65Event> fe65eve;
  int eventnumber;
  unsigned int thiseventtag;
  Fe65Event lastbcid;
  HistogramVariableStrage *hvs;
  TrackHitEvent *thisevent;
  std::vector<DAQLinkInfo> hli;
public:

  SPECconverter(){}
  SPECconverter(std::string filename);
  ~SPECconverter();
  void SetHistogram(HistogramVariableStrage *_hvs);
  void SetHistogram(HistogramVariableStrage *_hvs,OnlineMonWindow *om);
  void FillHistogram();
  void SetThisEvent(TrackHitEvent *_eve){thisevent=_eve;}
  void SetDAQInfo(std::vector<DAQLinkInfo>_hli){hli=_hli;}
  int GetAnEvent();
  int GetEvent(int EventNumber){return 1;}
  int GetNextEvent();

  int EventNumber(){return eventnumber;}
  std::vector<Fe65Event> GetSPECinfo(){return fe65eve;}
  void PrintSPECinfo();

};



#endif
