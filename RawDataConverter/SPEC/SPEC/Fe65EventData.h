#ifndef FE65EVENTDATA_H
#define FE65EVENTDATA_H

// #################################
// # Author: Timon Heim
// # Email: timon.heim at cern.ch
// # Project: Yarr
// # Description: Container for Fe65 data
// # Comment: Splits events by L1ID
// ################################

#include <deque>
#include <list>
#include <vector>
#include <string>
#include <unistd.h>

#include "LoopStatus.h"

struct Fe65Hit {
    unsigned col : 7;
    unsigned row : 9;
    unsigned tot : 5;
    unsigned unused : 11;
};

class Fe65Cluster {
    public:
        Fe65Cluster() {
            nHits = 0;
        }
        ~Fe65Cluster() {}

        void addHit(Fe65Hit* hit) {
            hits.push_back(hit);
            nHits++;
        }

        unsigned getColLength() {
            int min = 999999;
            int max = -1;
            for (unsigned i=0; i<hits.size(); i++) {
                if (hits[i]->col > max)
                    max = hits[i]->col;
                if (hits[i]->col < min)
                    min = hits[i]->col;
            }
            return max-min+1;
        }
        
        unsigned getRowWidth() {
            int min = 999999;
            int max = -1;
            for (unsigned i=0; i<hits.size(); i++) {
                if (hits[i]->row > max)
                    max = hits[i]->row;
                if (hits[i]->row < min)
                    min = hits[i]->row;
            }
            return max-min+1;
        }

        unsigned nHits;
        std::vector<Fe65Hit*> hits;
};

class Fe65Event {
    public:
        Fe65Event() {
            tag = 0;
            l1id = 0;
            bcid = 0;
            nHits = 0;
            nClusters = 0;
        }
        Fe65Event(unsigned arg_tag, unsigned arg_l1id, unsigned arg_bcid) {
            tag = arg_tag;
            l1id = arg_l1id;
            bcid = arg_bcid;
            nHits = 0;
            nClusters = 0;
        }
        ~Fe65Event() {
            //while(!hits.empty()) {
                //Fe65Hit *tmp = hits.back();
                //hits.pop_back();
                //delete tmp;
            //}
        }
	void PrintEvent(){
	  std::cout << "(tag, l1id, bcid, nHits, nClusters) = ("
		    << tag << ", " << l1id << ", " << bcid << ", " << nHits << ", " << nClusters << std::endl;
	  for(auto h :hits) {
	    std::cout << "(col, row, tot) = (" 
		      <<  h.col << " " << h.row << " " << h.tot << std::endl;
	  }
	    
	  
	}
        void addHit(unsigned arg_row, unsigned arg_col, unsigned arg_tot) {
            struct Fe65Hit tmp = {arg_col, arg_row, arg_tot, 0};
            //tmp.col = arg_col;
            //tmp.row = arg_row;
            //tmp.tot = arg_tot;
            //tmp.unused = 0;
            hits.push_back(Fe65Hit(tmp));
            nHits++;
        }

        void doClustering();

        void toFileBinary(std::fstream &handle);
        void fromFileBinary(std::fstream &handle);

        uint16_t l1id;
        uint16_t bcid;
        uint32_t tag;
        uint16_t nHits;
        uint16_t nClusters;
        std::vector<Fe65Hit> hits;
        std::vector<Fe65Cluster> clusters;
};

class Fe65Data {
    public:
        static const unsigned numServiceRecords = 32;
        Fe65Data() {
            //curEvent = NULL;
            for(unsigned i=0; i<numServiceRecords; i++)
                serviceRecords.push_back(0);
        }
        ~Fe65Data() {
            //while(!events.empty()) {
            //    Fe65Event *tmp = events.back();
            //    events.pop_back();
            //     delete tmp;
            //}
        }

        void newEvent(unsigned arg_tag, unsigned arg_l1id, unsigned arg_bcid) {
            events.push_back(Fe65Event(arg_tag, arg_l1id, arg_bcid));
            curEvent = &events.back();
        }

        void delLastEvent() {
            //delete curEvent;
            events.pop_back();
            curEvent = &events.back();
        }

        void toFile(std::string filename);

        Fe65Event *curEvent;

        LoopStatus lStat;
        std::list<Fe65Event> events;
        std::vector<int> serviceRecords;
};



#endif
