#include <iostream>
#include <string>
#include "Common/AtlasStyle.hh"
#include "OfflineAnalysis.hh"
#include "TROOT.h"
#include "TCanvas.h"
#include "TApplication.h"


int main(int argc, const char ** argv){
  SetAtlasStyle();
  gStyle->SetOptStat(10);
  gStyle->SetOptTitle(1);
  std::string configfile=argv[1];
  OfflineAnalysis * oa = new OfflineAnalysis();
  
  if(argc<1){
    std::cout << "Set configuration file !!" << std::endl;
    exit(-1);
  }else{
    std::cout << "reading config file : " << configfile << std::endl;
    std::ifstream fs(configfile);
    std::string detname="";
    std::string filename="";
    std::vector<std::string> filelist; filelist.clear();
    bool isnotnew=false;
    while(!fs.eof()){
      fs >> detname >> filename ;
      if(detname.substr(0,1)=="#")continue;
      for(auto x : filelist){
	if(detname==x)isnotnew=true;
      }
      if(detname=="")break;
      if(isnotnew)continue;
      filelist.push_back(detname);
      oa->AddDAQsystem(detname,filename);
    }
    oa->Initialize();
    oa->setHistFileName("hist.root");
    oa->Execute();
    oa->Finalize();
  }
  return 0;
}
