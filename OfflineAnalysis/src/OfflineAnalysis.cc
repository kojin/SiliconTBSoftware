#include "OfflineAnalysis.hh"

OfflineAnalysis::~OfflineAnalysis(){
}

void OfflineAnalysis::Finalize(){

  std::cout << "saving histgram....  " << std::endl;
  TFile *fp = new TFile(histfilename.c_str(),"RECREATE");
  for(auto hs : hvsv){
    for(auto name : hs->GetHistName()){
      hs->GetHistogram(name)->Write();
    }
    for(auto name : hs->Get2DHistName()){
      hs->Get2DHistogram(name)->Write();
    }
    for(auto name : hs->Get3DHistName()){
      hs->Get3DHistogram(name)->Write();
    }
  }
  fp->Close();
  std::cout << "done" << std::endl;
}
