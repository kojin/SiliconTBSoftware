void checkPrealign(){
  const int nlayer=7;
  TFile *fp = new TFile("../src/hist.root");
  std::stringstream ss,ss2;
  ss.str(""); ss2.str("");
  TCanvas *c1 = new TCanvas("c1","c1",1000,300);
  c1->Divide(nlayer-1,2);
  TH1D *hist[2][nlayer-1];
  double mean[2][nlayer];
  double align[2][nlayer];
  for(int ii=0;ii<nlayer-1;ii++){
    for(int jj=0;jj<2;jj++){
      mean[jj][ii]=0;
      align[jj][ii]=0;
    }
  }

  
  TF1 *func=new TF1("func","gaus");
  for(int ii=0;ii<nlayer-1;ii++){
    ss.str(""); ss2.str("");
    ss << "PrealignerX" << ii << "-"<< ii+1 << "_TrackHitMaker";
    ss2 << "PrealignerY" << ii << "-"<< ii+1 << "_TrackHitMaker";
    hist[0][ii] = (TH1D*)fp->Get(ss.str().c_str());
    hist[1][ii] = (TH1D*)fp->Get(ss2.str().c_str());
    
    for(int jj=0;jj<2;jj++){
      c1->cd(ii+1+(nlayer-1)*jj);
      int maxbin=hist[jj][ii]->GetMaximumBin();
      double peak=hist[jj][ii]->GetBinCenter(maxbin);
      func->SetParameter(1,peak);
      func->SetParameter(2,0.01);
      hist[jj][ii]->GetXaxis()->SetRangeUser(peak-0.5,peak+0.5);
      hist[jj][ii]->Fit("func");
      hist[jj][ii]->Draw();
      double fitmean=func->GetParameter(1);
      mean[jj][ii+1]=fitmean;
      for(int kk=ii+1;kk<nlayer;kk++)align[jj][kk]+=fitmean;
    }

  }
  c1->Update();
  c1->Print("Prealign.pdf");
  c1->Print("Prealign.png");
  
  for(int ii = 0;ii <nlayer ;ii++){
    
    std::cout << mean[0][ii] << " " << mean[1][ii] << std::endl;
  }
  std::cout << "==============" << std::endl;
  for(int ii = 0;ii <nlayer ;ii++){
    
    std::cout << std::setprecision(4) << std::setw(10) << align[0][ii] << " " << std::setw(10)<< align[1][ii] << std::endl;
  }
  
}
