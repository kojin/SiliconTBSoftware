#ifndef  __EFFICIENCYCALC_HH__
#define  __EFFICIENCYCALC_HH__
#include <iostream>
#include <limits>
#include "HistogramTools/HistogramVariableStrage.hh"
#include "TrackHitEvent/TrackHitEvent.hh"
#include "Tracking/TrackHitMaker.hh"
#include "Alignment/Align.hh"

class EfficiencyCalc {
private :
  HistogramVariableStrage *hvs;
  TrackHitEvent *thisevent;
  TrackHitMaker *trkmk;
  std::vector<DAQLinkInfo> laylist;
  std::vector<std::vector<DAQLinkInfo>> laycomb;
  std::vector<DAQLinkInfo> dutlay;
public :
  EfficiencyCalc(){
    trkmk = new TrackHitMaker();
  }
  ~EfficiencyCalc(){delete trkmk;}
  void SetHistogram(HistogramVariableStrage *_hvs);
  void SetEvent(TrackHitEvent *eve){thisevent=eve;}
  void SetDAQLinkInfo(std::vector<DAQLinkInfo>_ly){
    laylist=_ly;dutlay.clear();
    for(auto h:laylist){
      std::vector<DAQLinkInfo> tmplay; tmplay.clear();
      for(auto h2 :laylist){
	if(h.layer==h2.layer){
	  dutlay.push_back(h2);
	  continue;
	}
	tmplay.push_back(h2);
      }
      laycomb.push_back(tmplay);
    }
  }
  void CalcEfficiency();
};






#endif
