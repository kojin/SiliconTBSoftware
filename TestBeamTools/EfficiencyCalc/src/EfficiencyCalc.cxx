#include "EfficiencyCalc/EfficiencyCalc.hh"


void EfficiencyCalc::SetHistogram(HistogramVariableStrage *_hvs){
  hvs=_hvs;
  for(auto dut : dutlay){
    hvs->AddHistogram("DutPixelToT_"+dut.name,16,-0.5,15.5);
    hvs->AddHistogram("DutSumToT_"+dut.name,31,-0.5,30.5);
    hvs->AddHistogram("DutClusterSize_"+dut.name,16,-0.5,15.5);
    hvs->AddHistogram("DutClusterSizeX_"+dut.name,16,-0.5,15.5);
    hvs->AddHistogram("DutClusterSizeY_"+dut.name,16,-0.5,15.5);
    hvs->AddHistogram("DutLv1_"+dut.name,16,-0.5,15.5);

    hvs->AddHistogram("DutResidualX_"+dut.name,600,-0.3,0.3);
    hvs->AddHistogram("DutResidualY_"+dut.name,600,-0.3,0.3);
    hvs->AddHistogram("DutResidual_"+dut.name,100,0.,0.5);
    hvs->Add2DHistogram("DutTrackMap_"+dut.name,100,15,25,100,18,30);
    hvs->Add2DHistogram("DutTrackMapWhit_"+dut.name,100,15,25,100,18,30);

    if(dut.type.substr(0,8)=="FEI4SING"){
      hvs->Add2DHistogram("DutTrackMapLocal_"+dut.name,2000,0,20,2000,0,20);
      hvs->Add2DHistogram("DutTrackMapLocalWhit_"+dut.name,2000,0,20,2000,0,20);
      hvs->Add2DHistogram("DutTrackPixelMapLocal_"+dut.name,200,-125,375,200,-25,75);
      hvs->Add2DHistogram("DutTrackPixelMapLocalWhit_"+dut.name,200,-125,375,200,-25,75);
    }else if(dut.type.substr(0,8)=="FEI4QUAD"){
      hvs->Add2DHistogram("DutTrackMapLocal_"+dut.name,4000,0,40,4000,0,40);
      hvs->Add2DHistogram("DutTrackMapLocalWhit_"+dut.name,4000,0,40,4000,0,40);
      hvs->Add2DHistogram("DutTrackPixelMapLocal_"+dut.name,200,-125,375,200,-25,75);
      hvs->Add2DHistogram("DutTrackPixelMapLocalWhit_"+dut.name,200,-125,375,200,-25,75);
    }else if(dut.type.substr(0,4)=="FE65"){
      hvs->Add2DHistogram("DutTrackMapLocal_"+dut.name,320,0,3.2,320,0,3.2);
      hvs->Add2DHistogram("DutTrackMapLocalWhit_"+dut.name,320,0,3.2,320,0,3.2);
      hvs->Add2DHistogram("DutTrackPixelMapLocal_"+dut.name,200,-25,75,200,-25,75);
      hvs->Add2DHistogram("DutTrackPixelMapLocalWhit_"+dut.name,200,-25,75,200,-25,75);
    }
  }

}
void EfficiencyCalc::CalcEfficiency(){
  //  std::cout << "fei4 cluster : "<< thisevent->getFEI4Clustering().size()<< std::endl;
  //  std::cout << "fe65 cluster : "<< thisevent->getFE65Clustering().size()<< std::endl;

  //  trkmk
  Align ar0=thisevent->getAlign();
  //  std::cout << laycomb.size() << std::endl;
  
  for(unsigned int ilaycomb=0;ilaycomb<laycomb.size();ilaycomb++){
    //    std::cout << "==" << std::endl;
    std::vector<std::vector <Position>>pos; pos.clear();
    std::vector<int> iclu; iclu.clear();
    std::vector<int> iclucnt; iclucnt.clear();
    double dutZ=0;
    for(unsigned int iar=0;iar<ar0.modname[0].size();iar++){
      if(ar0.modname[0][iar]==dutlay[ilaycomb].name){
	dutZ=ar0.shiftZ[0][iar];
	break;
      }
    }
    //    std::cout << dutZ << std::endl;

    for(auto lay : laycomb[ilaycomb]){
      //      lay.PrintInfo();
      std::vector<Position> tmppos;
      if(lay.type.substr(0,4)=="FEI4"){
	for(auto clu : thisevent->getFEI4Clustering(lay.layer)){
	  tmppos.push_back(clu);
	  if(0){
	    std::cout << "--" << std::endl;
	    for(auto hit : clu.GetHitsForCluster()){
	      std::cout << hit.Col() << " " << hit.Row() << std::endl;
	    }
	    std::cout << "--" << std::endl;
	  }
	}
      }else if(lay.type.substr(0,4)=="FE65"){
	for(auto clu : thisevent->getFE65Clustering(lay.layer)){
	  tmppos.push_back(clu);
	}
      }
      pos.push_back(tmppos);
      iclu.push_back(tmppos.size());
      iclucnt.push_back(0);
    }
    bool incZeroLay=false;
    for(auto cc : iclu){
      if(cc==0)incZeroLay=true;
      // to speed up.
      // if(cc>10)incZeroLay=true;

    }
    if(incZeroLay)continue;
    if(0){
      int ncomb=1;
      std::cout << "hit per layer" << std::endl;
      for(auto aa:pos){
	ncomb*=aa.size();
	std::cout << aa.size() << " " ;
      }
      std::cout << std::endl;
      std::cout << "number of combination : " << ncomb<< std::endl;
    }
    bool isEnd=false;
    std::vector<Tracking> tracks; tracks.clear();
    std::vector<std::vector<int>> lyhitnum; lyhitnum.clear();
    while(!isEnd){
      std::vector<Position> postrk; postrk.clear();
      for(unsigned int ilay=0;ilay<pos.size();ilay++){
	postrk.push_back(pos[ilay][iclucnt[ilay]]);
      }
      //      if(postrk.size()!=6)std::cout << "postrk.size()!=6" << std::endl;
      trkmk->setNLayer(postrk.size());
      trkmk->SetHitPosition(postrk);
      bool isNAN=false;
      //      std::cout << "fitting ... " << std::endl;
      for(auto p :postrk){
	if(isnan(p.GlobalPosX())||isnan(p.GlobalPosY()))isNAN=true;
      }
      if(!isNAN){
      //      trkmk->PrintHitinTrack();
	//      trkmk->do2DTracking(0,0.05);
      trkmk->do2DTracking(0);
      //      std::cout << "done " << std::endl;
      Tracking *track=trkmk->GetTrack();

      if(track->GetChisqX()<1.5&&track->GetChisqY()<1.5){
	if(0){
	  std::cout << "LOOP : " ;
	  for(auto cc : iclucnt)std::cout << cc << " " ;
	  std::cout << " : chisq = " << track->GetChisqX() << " " <<  track->GetChisqY() << " x,y = " << track->GetPointFromZ(dutZ).X() << " " << track->GetPointFromZ(dutZ).Y() << std::endl;
	}
	tracks.push_back(*track);
	lyhitnum.push_back(iclucnt);
      }
      }

      for(unsigned int ii=iclu.size()-1;ii>=0;ii--){
	if(iclucnt[ii]!=iclu[ii]-1){
	  iclucnt[ii]++;
	  break;
	}else{
	  if(ii==0){
	    isEnd=true;
	    break;
	  }
	  iclucnt[ii]=0;
	}
      }
      if(0)std::cout << "isEnd? " << isEnd << std::endl;
    }
    bool debugprint=false;
    //    if(tracks.size()>1)debugprint=true;
    if(debugprint){
      std::cout << "--" << std::endl;
      for(unsigned int ii=0;ii<tracks.size();ii++){
	for(auto cc : lyhitnum[ii])std::cout << cc << " " ;
	std::cout << " : chisq = " << tracks[ii].GetChisqX() << " " <<  tracks[ii].GetChisqY() << " x,y = " << tracks[ii].GetPointFromZ(dutZ).X() << " " << tracks[ii].GetPointFromZ(dutZ).Y() << std::endl;
      }
    }
    // to be improved for two track case 
    std::vector<int> erasecand; erasecand.clear();
    for(unsigned int ii=0;ii<tracks.size();ii++){
      if(ii==tracks.size()-1)break;
      for(unsigned int jj=ii+1;jj<tracks.size();jj++){
	int nsharedhit=0;
	for(unsigned int ily=0;ily<lyhitnum[ii].size();ily++){
	  if(lyhitnum[ii][ily]==lyhitnum[jj][ily])nsharedhit++;
	}
	if(debugprint)std::cout << "# of shared hit (" << ii << "," << jj << ")=" << nsharedhit << std::endl;
	if(nsharedhit>0){
	  int ec=0;bool iserased=false;
	  if(tracks[ii].GetChisqX()+tracks[ii].GetChisqY()>tracks[jj].GetChisqX()+tracks[jj].GetChisqY())ec=ii;
	  else ec=jj;
	  for(auto e:erasecand){
	    if(e==ec)iserased=true;
	  }
	  if(!iserased)erasecand.push_back(ec);
	}
      }
    }
    std::sort(erasecand.begin(),erasecand.end());
    if(debugprint&&erasecand.size()){
      std::cout << "erase candidate " ;
      for(auto e : erasecand)std::cout << e << " " ;
      std::cout << std::endl;
    }
    int index=0;
    std::vector<Tracking>::iterator it = tracks.begin();
    std::vector<std::vector<int>>::iterator itc = lyhitnum.begin();
    while(it != tracks.end()){
      bool iserased=false;
      for(auto e :erasecand){
	if(index==e){
	  it = tracks.erase(it);
	  itc = lyhitnum.erase(itc);
	  iserased=true;
	  break;
	}
      }
      if(iserased==false){
	++it;
	++itc;
      }
      index++;
    }
    if(debugprint){
      std::cout << "after erase--" << std::endl;
      for(unsigned int ii=0;ii<tracks.size();ii++){
	for(auto cc : lyhitnum[ii])std::cout << cc << " " ;
	std::cout << " : chisq = " << tracks[ii].GetChisqX() << " " <<  tracks[ii].GetChisqY() << " x,y = " << tracks[ii].GetPointFromZ(dutZ).X() << " " << tracks[ii].GetPointFromZ(dutZ).Y() << std::endl;
      }
    }
    
    

    //    std::vector<Position> tmppos;
    std::vector<Cluster> tmppos;
    if(dutlay[ilaycomb].type.substr(0,4)=="FEI4"){
      for(auto clu : thisevent->getFEI4Clustering(dutlay[ilaycomb].layer)){
	tmppos.push_back(clu);
      }
    }else if(dutlay[ilaycomb].type.substr(0,4)=="FE65"){
      for(auto clu : thisevent->getFE65Clustering(dutlay[ilaycomb].layer)){
	tmppos.push_back(clu);
      }
    }
    for(auto trk : tracks){
      bool isOnTrack=false;
      Position trkpoint;
      trkpoint.SetLayer(dutlay[ilaycomb].layer);
      trkpoint.SetGlobalPosX(trk.GetPointFromZ(dutZ).X());
      trkpoint.SetGlobalPosY(trk.GetPointFromZ(dutZ).Y());
      trkpoint.SetGlobalPosZ(trk.GetPointFromZ(dutZ).Z());
      trkpoint.SetLocalPosition(ar0,dutlay[ilaycomb].name);
      if(0){ // double check if local position correctly calculated
	std::cout << "==== setting local position for track pointing  : "  << dutlay[ilaycomb].name<< std::endl;
	std::cout << "==== track position "  << std::endl;
	trkpoint.PrintPosition();
	trkpoint.SetGlobalPosition(ar0,dutlay[ilaycomb].name);
	std::cout << "==== track position after Global Position set"  << std::endl;
	trkpoint.PrintPosition();
	std::cout << " --- "  << std::endl;
      }
      for(auto ihit : tmppos){
	double dx=trk.GetPointFromZ(dutZ).X()-ihit.GlobalPosX();
	double dy=trk.GetPointFromZ(dutZ).Y()-ihit.GlobalPosY();
	double dist=sqrt(dx*dx+dy*dy);
	if(dist<0.2)isOnTrack=true;
	hvs->GetHistogram("DutResidualX_"+dutlay[ilaycomb].name)->Fill(dx);
	hvs->GetHistogram("DutResidualY_"+dutlay[ilaycomb].name)->Fill(dy);
	hvs->GetHistogram("DutResidual_"+dutlay[ilaycomb].name)->Fill(dist);
	if(isOnTrack){
	  hvs->GetHistogram("DutSumToT_"+dutlay[ilaycomb].name)->Fill(ihit.SumToT());
	  hvs->GetHistogram("DutClusterSize_"+dutlay[ilaycomb].name)->Fill(ihit.GetClusterSize());
	  hvs->GetHistogram("DutClusterSizeX_"+dutlay[ilaycomb].name)->Fill(ihit.GetClusterSize(1));
	  hvs->GetHistogram("DutClusterSizeY_"+dutlay[ilaycomb].name)->Fill(ihit.GetClusterSize(2));
	  for(auto ih : ihit.GetHitsForCluster()){
	    hvs->GetHistogram("DutPixelToT_"+dutlay[ilaycomb].name)->Fill(ih.ToT());
	    hvs->GetHistogram("DutLv1_"+dutlay[ilaycomb].name)->Fill(ih.LV1());
	    
	  }
	}
	//	std::cout << dist << std::endl;
      }
      hvs->Get2DHistogram("DutTrackMap_"+dutlay[ilaycomb].name)->Fill(trk.GetPointFromZ(dutZ).X(),trk.GetPointFromZ(dutZ).Y());
      hvs->Get2DHistogram("DutTrackMapLocal_"+dutlay[ilaycomb].name)->Fill(trkpoint.PosX(),trkpoint.PosY());
      
      int nSkipRow=0;
      double btwbumpCol=0;
      if(dutlay[ilaycomb].type=="FEI4QUADA"){
	nSkipRow=8;
	btwbumpCol=0.350;
      }else if(dutlay[ilaycomb].type=="FEI4QUADB"){
	nSkipRow=6;
	btwbumpCol=0.350;
      }else if(dutlay[ilaycomb].type=="FEI4QUADC"){
	nSkipRow=4;
	btwbumpCol=0.250;
      }
      double xx=0; double yy=0;
      xx=trkpoint.PosX();
      yy=trkpoint.PosY();
      bool isActiveArea=false;
      //      std::cout << "aa " << xx << " " <<  yy << std::endl;
      if(dutlay[ilaycomb].type.substr(0,8)=="FEI4SING"){
	if(xx>0&&yy>0&&xx<80*0.25&&yy<336*0.05)isActiveArea=true;
	while(xx>0.375)xx-=0.5;
	while(yy>0.075)yy-=0.1;
      }else if(dutlay[ilaycomb].type.substr(0,8)=="FEI4QUAD"){
	if(xx>0&&yy>0&&xx<160*0.25+btwbumpCol-0.05&&yy<(672+nSkipRow)*0.05)isActiveArea=true;
	if(xx>20)xx-=20+btwbumpCol-0.050;
	if(yy>0.05*336)yy-=0.05*336+nSkipRow*0.050;
	while(xx>0.375)xx-=0.5;
	while(yy>0.075)yy-=0.1;
      }else if(dutlay[ilaycomb].type.substr(0,4)=="FE65"){
	if(xx>0&&yy>0&&xx<64*0.05&&yy<64*0.05)isActiveArea=true;
	while(xx>0.075)xx-=0.1;
	while(yy>0.075)yy-=0.1;
      }

      //      std::cout << "bb " << xx << " " <<  yy << std::endl;
      if(isActiveArea)hvs->Get2DHistogram("DutTrackPixelMapLocal_"+dutlay[ilaycomb].name)->Fill(xx*1000,yy*1000);
      if(isOnTrack){
	hvs->Get2DHistogram("DutTrackMapWhit_"+dutlay[ilaycomb].name)->Fill(trk.GetPointFromZ(dutZ).X(),trk.GetPointFromZ(dutZ).Y());
	hvs->Get2DHistogram("DutTrackMapLocalWhit_"+dutlay[ilaycomb].name)->Fill(trkpoint.PosX(),trkpoint.PosY());
	hvs->Get2DHistogram("DutTrackPixelMapLocalWhit_"+dutlay[ilaycomb].name)->Fill(xx*1000,yy*1000);
      }
      

    }
    

  }
  
  
}
  
