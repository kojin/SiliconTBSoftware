#ifndef __LGADHIT_HH__
#define __LGADHIT_HH__

#include <iostream>
#include "Hit.hh"
#include "Configuration/Module.hh"

class LGADHit : public Hit{
private:
  int iChannel; // ADC channel number
  int iLayer;
  int iCol;
  int iRow;
  short int ADC[1024];
  std::string type;
  std::string name;
  Module mod;  

public:  
  LGADHit(){
    iChannel=-1;  iLayer=-1;  iCol=-1; iRow = -1;
    type="";  name="";
    for(int ii =0;ii<1024;ii++) ADC[ii]=-1;
  }
  LGADHit(int ich,int icol, int irow, short int *adc,int ily, std::string _type, std::string _name,bool rowdata=true){
    iChannel=-1;  iLayer=-1;  iCol=-1; iRow = -1;
    type="";  name="";
    for(int ii =0;ii<1024;ii++) ADC[ii]=-1;
    SetLGADHit(ich,icol,irow,adc,ily,_type,_name,rowdata);
  }
  void SetLGADHit(int ich,int icol, int irow, short int *adc,int ily, std::string _type, std::string _name,bool rowdata=true);
  void SetLayer(int ilay){iLayer=ilay;}
  int GetLayer(){return iLayer;}
  void PrintLGADHit();
  
  ~LGADHit(){}
  Module GetModuleInfo(){return mod;}
  
};


#endif
