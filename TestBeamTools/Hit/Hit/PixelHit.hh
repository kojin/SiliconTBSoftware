#ifndef __PixelHit__HH__
#define __PixelHit__HH__
#include <iostream>
#include <vector>
#include <cmath>
#include "Hit.hh"
#include "Configuration/Module.hh"

class PixelHit : public Hit{
private:
  int iChannel; // RJ
  int iDAQLink;
  int iLayer;
  int iCol;
  int iRow;
  int nToT;
  int nToT2;
  int nLV1;
  int bcid;
  std::string type;
  std::string name;
  Module mod;

public:
  PixelHit(){
    iCol=-1; iRow=-1; iChannel=-1; nToT=-1; nToT2=-1;
    nLV1=-1; iLayer=-1; iDAQLink=-1; type=""; name="";
  }
  PixelHit(int ich,int icol,int irow, int ntot, int ntot2, int lv1, int Layer, int daqlink,std::string _type, std::string _name,bool rowdata=true){
    iCol=-1; iRow=-1; iChannel=-1; nToT=-1; nToT2=-1;
    nLV1=-1; iLayer=-1; iDAQLink=-1; type=""; name="";
    SetPixelHit(ich,icol,irow,ntot,ntot2,lv1,Layer,daqlink,_type,_name,rowdata);
  }

  void SetPixelHit(int ich,int icol,int irow, int ntot, int ntot2, int lv1, int Layer, int daqlink,std::string _type,std::string _name,bool rowdata);
  ~PixelHit(){}
  void SetLayer(int ilay){iLayer=ilay;}
  int GetLayer(){return iLayer;}
  void SetBCID(int _bcid){bcid=_bcid;}
  int GetBCID(){return bcid;}
  Module GetModuleInfo(){return mod;}

  bool isNext(PixelHit nexthit);

  void PrintPixelHit();
  int Channel(){return iChannel;}
  int Col(){return iCol;}
  int Row(){return iRow;};
  int ToT(){return nToT;};
  int LV1(){return nLV1;};
  int ToT2(){return nToT2;};
  int DAQLink(){return iDAQLink;}
  std::string Type(){return type;};
  std::string Name(){return name;};
  
//  bool operator > (const PixelHit &li1) const{
//    return (iDAQLink > li1.DAQLink());
//  }
//  bool operator < (const PixelHit &li1) const{
//    return (iDAQLink < li1.DAQLink());
//  }
};

#endif
