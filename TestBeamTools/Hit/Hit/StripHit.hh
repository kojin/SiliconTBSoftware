#ifndef __StripHit__HH__
#define __StripHit__HH__
#include <iostream>
#include <vector>
#include <cmath>
#include "Hit.hh"

class StripHit : public Hit{
private:
  int iChannel;
  int iLayer;
  int iCol;
  int iRow;
  int nADC;   // Corresponds to Columns
  int nADC2;  // Corresponds to Rows
//  double  posX;
//  double  posY;
//  double  posZ;
  std::string Type;
public:
  StripHit(){
    iChannel=-1;  iLayer=-1;  iCol=-1;  iRow=-1;  nADC=-1;  nADC2=-1; 
  };
  StripHit(int ich,int icol,int irow,int nadc,int nadc2,int Layer,
	   std::string _Type,  bool rowdata = true){
    SetStripHit(ich, icol, irow, nadc, nadc2, Layer, _Type, rowdata);
  }

  void SetStripHit(int ich, int icol, int irow, int nadc, int nadc2,
		   int Layer, std::string _Type, bool rowdata);
  ~StripHit(){}
  bool isNext(StripHit nexthit){
    if(nexthit.GetLayer() != iLayer) return false;
    if((std::fabs(iCol-nexthit.Col()) == 0 && std::fabs(iRow-nexthit.Row()) == 1)
       || (std::fabs(iCol-nexthit.Col()) == 1 && std::fabs(iRow-nexthit.Row()) == 0))
      return true;
    else return false;
  }
  void PrintStripHit(){
    std::cout << "Layer : " << iLayer 
	      << "  channel : " << iChannel 
	      << " (col,row,posX,posY,posZ,nadc,nadc2)=("
	      <<  iCol <<"," << iRow << ","
	      << PosX() << "," << PosY() << "," <<PosZ()  << ","
	      << nADC << "," << nADC2 << ")" << std::endl;
  }
  void SetLayer(int ilay){iLayer = ilay;}
  int GetLayer(){return iLayer;}
  int Channel(){return iChannel;}
  int Col(){return iCol;}
  int Row(){return iRow;};
  int ADC(){return nADC;};
  int ADC2(){return nADC2;};
//  double PosX(){return posX;};
//  double PosY(){return posY;};
//  void SetPosZ(double posz){posZ=posz;};
//  double PosZ(){return posZ;};
};

#endif
