#include "LGADHit.hh"


void LGADHit::SetLGADHit(int ich,int icol, int irow, short int *adc,int ily, std::string _type, std::string _name,bool rowdata){
  iChannel=ich;  iLayer=ily;  iCol=icol; iRow = irow;
  type=_type;  name=_name;
  for(int ii=0;ii<1024;ii++)ADC[ii]=adc[ii];
  if(type!="LGADpad"
     &&type!="LGAD2x2"
     &&type!="LGAD4x4"
     &&type!="LGAD5x5"
     &&type!="LGADstrip"){
    std::cerr << "unknown type" <<type << std::endl;
  }

  double _posX=0;
  double _posY=0;
  if(type.substr(4,3)=="2x2"
     ||type.substr(4,3)=="4x4"
     ||type.substr(4,3)=="5x5"){
    _posX=0.65+iCol*1.3;
    _posY=0.65+iRow*1.3;
  }else if(type.substr(4,5)=="strip"){
    _posX=0.040+iCol*0.080;
    _posY=0.;
  }
  double _posZ=0;
  SetPosX(_posX);
  SetPosY(_posY);
  SetPosZ(_posZ);
}

void LGADHit::PrintLGADHit(){
}
