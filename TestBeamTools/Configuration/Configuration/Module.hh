#ifndef __Module_HH__
#define __Module_HH__
#include <iostream>
#include <string>

class Module{
public:
  Module(){;}
  Module(std::string type){
    SetModuleType(type);
  }
  void SetModuleType(std::string type){
    if(type!="FE65"
       &&type!="RD53A"
       &&type!="FEI4SING"
       &&type!="FEI4QUADA"
       &&type!="FEI4QUADB"
       &&type!="FEI4QUADC")
      std::cerr << "unknown type : " << type << std::endl;
    modtype=type;
  }
    
  ~Module(){;}
  void SetDefaultModule();
  std::string FEtype;  // e.g. FEI4
  std::string modtype;   // e.g. FEI4QUADA
  std::string modid; // e.g. KEK113
  int nCol;
  int nRow;
  double colsize; // [mm]
  double rowsize; // [mm]
  int nSkipRow;   // 
  double btwbumpCol; // [mm]
};

#endif
