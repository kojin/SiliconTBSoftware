#ifndef __DAQLINKINFO_HH__
#define __DAQLINKINFO_HH__
#include <iostream>
#include <string>
#include <vector>

class DAQLinkInfo{
public:
  int link;
  int layer;
  int channel;
  std::string type;
  std::string name;
  DAQLinkInfo();
  void PrintInfo();
  
  void setInfo(DAQLinkInfo a);
  void setInfo(std::string _name,int _link,int _layer, std::string _type, int _channel);

  bool operator > (const DAQLinkInfo &li1) const{
    return (link > li1.link);
  }
  bool operator < (const DAQLinkInfo &li1) const{
    return (link < li1.link);
  }
};

#endif
