#include "TrackHitMaker.hh"
void TrackHitMaker::do2DTracking(int Debug,double error){
  double trkposx,trkposy,trktheta,trkphi;
  if(positions.size()==0)return;
  std::vector <double>posx;
  std::vector <double>posy;
  std::vector <double>posz;
  std::vector <double>posxe;
  std::vector <double>posye;
  std::vector <double>posze;
  for(auto p :positions){
    posx.push_back(p.GlobalPosX());
    posy.push_back(p.GlobalPosY());
    posz.push_back(p.GlobalPosZ());
    if(error==-999.){
      posxe.push_back(p.GlobalPosXe());
      posye.push_back(p.GlobalPosYe());
      posze.push_back(p.GlobalPosZe());
    }else{
      posxe.push_back(error);
      posye.push_back(error);
      posze.push_back(1);
    }
    if(Debug){
      std::cout << "Hit Pos (x,y,z) = "
		<< p.GlobalPosX() << "+-" << p.GlobalPosXe() << " "
		<< p.GlobalPosY() << "+-" << p.GlobalPosYe() << " "
		<< p.GlobalPosZ() << std::endl;
    }
  }
  
  TGraphErrors * grX= new TGraphErrors(positions.size(),&posz[0],&posx[0],&posze[0],&posxe[0]);
  TGraphErrors * grY= new TGraphErrors(positions.size(),&posz[0],&posy[0],&posze[0],&posye[0]);
  TF1 *f1 = new TF1("f1","[0]+x*[1]");
  f1->SetParameter(0,posx[0]);
  f1->SetParameter(1,0);
  grX->Fit(f1,"Q");
  trkposx=f1->GetParameter(0);
  //  trkposx1000=f1->GetParameter(0)+1000*f1->GetParameter(1);
  trktheta=atan(f1->GetParameter(1));
  track->SetChisqX(f1->GetChisquare()/f1->GetNDF());
  if(Debug)std::cout << "x fit : " << f1->GetParameter(0) << " + x * " << f1->GetParameter(1) << "  chi2=" << f1->GetChisquare()<< "/" << f1->GetNDF()<< std::endl;
  f1->SetParameter(0,posy[0]);
  f1->SetParameter(1,0);
  grY->Fit(f1,"Q");

  trkposy=f1->GetParameter(0);
  //  trkposy1000=f1->GetParameter(0)+1000*f1->GetParameter(1);
  trkphi=atan(f1->GetParameter(1));
  track->SetChisqY(f1->GetChisquare()/f1->GetNDF());
  if(Debug)std::cout << "y fit " << f1->GetParameter(0) << " + x * " << f1->GetParameter(1) << "  chi2=" << f1->GetChisquare()<< "/" << f1->GetNDF() << std::endl;

  track->SetApoint(trkposx,trkposy,0);
  track->SetDirection(trktheta,trkphi);
  //  track->SetTwoPoint(trkposx,trkposy,0,trkposx1000,trkposy1000,1000);
  if(Debug&&track->GetChisqX()<3&&track->GetChisqY()<3){
    std::cout << "[Tracking]" << std::endl;
    track->PrintPointsOnLine(10,100,true);

    TCanvas *trkcan1 = new TCanvas("trkcan1","trkcan1",1000,500);
    trkcan1->Divide(1,2);
    trkcan1->cd(1);
    grX->Draw("AP");
    trkcan1->cd(2);
    grY->Draw("AP");
    trkcan1->Update();
    std::stringstream ss;
    ss << "tracking" << Debug << ".png";
    trkcan1->Print(ss.str().c_str());
    delete trkcan1;
  }

  delete f1;
  delete grX;
  delete grY;
}


void TrackHitMaker::do3DTracking(){
  LineFitter lf;
  lf.SetClusterPoints(positions);
  double xx[5],xxe[5],chisq;
  lf.PerformChisqFit(0,xx,xxe,chisq);
  std::cout << "Fit done... chisq = " << chisq << std::endl;
  for(int ii=0;ii<5;ii++){
    std::cout << xx[ii] << " " << xxe[ii] << std::endl;
  }
  
}
void TrackHitMaker::SetHistogram(HistogramVariableStrage *_hvs){
  hvs=_hvs;
  hvs->Add3DHistogram("TrackHitMaker/geometory",50,-25,25,50,-25,25,1500,-50,1450);
  std::stringstream ss,ss2;
  if(nlayer<2)return;
  for(unsigned int ipos=0;ipos<nlayer;ipos++){
    ss.str("");
    ss <<  ipos <<"-"<<ipos+1;
    ss2.str("");
    ss2 <<  ipos ;
    hvs->AddHistogram("ResidualX"+ss2.str(),1000,-0.2,0.2);
    hvs->AddHistogram("ResidualY"+ss2.str(),1000,-0.2,0.2);
    hvs->Add2DHistogram("ResidualX_dX"+ss2.str(),400,-10,30,1000,-0.2,0.2);
    hvs->Add2DHistogram("ResidualY_dY"+ss2.str(),400,-10,30,1000,-0.2,0.2);
    hvs->Add2DHistogram("ResidualY_dX"+ss2.str(),400,-10,30,1000,-0.2,0.2);
    hvs->Add2DHistogram("ResidualX_dY"+ss2.str(),400,-10,30,1000,-0.2,0.2);
    if(ipos==nlayer-1)continue;
    hvs->AddHistogram("PrealignerX"+ss.str(),1000,-50,50);
    hvs->AddHistogram("PrealignerY"+ss.str(),1000,-50,50);
    if(0){ // just for fun
      hvs->Add2DHistogram("PrealignerX_dX"+ss.str(),1000,-50,50,1000,-200,200);
      hvs->Add2DHistogram("PrealignerY_dY"+ss.str(),1000,-50,50,1000,-200,200);
      hvs->Add2DHistogram("PrealignerY_dX"+ss.str(),1000,-50,50,1000,-200,200);
      hvs->Add2DHistogram("PrealignerX_dY"+ss.str(),1000,-50,50,1000,-200,200);
    }
  }
  hvs->AddHistogram("TrackChisqX",100,0,5);
  hvs->AddHistogram("TrackChisqY",100,0,5);
  hvs->AddHistogram("TrackChisq3D",100,0,5);
  

}
void TrackHitMaker::FillHistogram(){
  hvs->GetHistogram("TrackChisqX")->Fill(track->GetChisqX());
  hvs->GetHistogram("TrackChisqY")->Fill(track->GetChisqY());
  hvs->GetHistogram("TrackChisq3D")->Fill(track->GetChisq3D());
  std::stringstream ss,ss2;
  for(unsigned int ipos=0;ipos<positions.size();ipos++){
    hvs->Get3DHistogram("TrackHitMaker/geometory")
      ->Fill(positions[ipos].GlobalPosX(),
	     positions[ipos].GlobalPosY(),
	     positions[ipos].GlobalPosZ());
    ss.str("");
    ss << ipos <<"-"<<ipos+1;
    ss2.str("");
    ss2 << ipos ;
    if(track->GetChisqX()<3&&track->GetChisqY()<3){
      double dX=positions[ipos].GlobalPosX()
	-track->GetPointFromZ(positions[ipos].GlobalPosZ()).X();
      double dY=positions[ipos].GlobalPosY()
	-track->GetPointFromZ(positions[ipos].GlobalPosZ()).Y();
      hvs->GetHistogram("ResidualX"+ss2.str())
	->Fill(dX);
      hvs->GetHistogram("ResidualY"+ss2.str())
	->Fill(dY);
      hvs->Get2DHistogram("ResidualX_dX"+ss2.str())
	->Fill(positions[ipos].GlobalPosX(),dX);
      hvs->Get2DHistogram("ResidualY_dY"+ss2.str())
	->Fill(positions[ipos].GlobalPosY(),dY);
      hvs->Get2DHistogram("ResidualY_dX"+ss2.str())
	->Fill(positions[ipos].GlobalPosY(),dX);
      hvs->Get2DHistogram("ResidualX_dY"+ss2.str())
	->Fill(positions[ipos].GlobalPosX(),dY);
    }
    if(ipos==positions.size()-1)continue;
    double dX=positions[ipos].GlobalPosX()
      -positions[ipos+1].GlobalPosX();
    double dY=positions[ipos].GlobalPosY()
      -positions[ipos+1].GlobalPosY();
    hvs->GetHistogram("PrealignerX"+ss.str())
      ->Fill(dX);
    hvs->GetHistogram("PrealignerY"+ss.str())
      ->Fill(dY);
    if(0){ // just for fun
      hvs->Get2DHistogram("PrealignerX_dX"+ss.str())
	->Fill(positions[ipos].GlobalPosX(),dX);
      hvs->Get2DHistogram("PrealignerY_dY"+ss.str())
	->Fill(positions[ipos].GlobalPosY(),dY);
      hvs->Get2DHistogram("PrealignerY_dX"+ss.str())
	->Fill(positions[ipos].GlobalPosY(),dX);
      hvs->Get2DHistogram("PrealignerX_dY"+ss.str())
	->Fill(positions[ipos].GlobalPosX(),dY);
    }
  }
    
}

void TrackHitMaker::PrintHitinTrack(){
  for(auto hit : positions){
    std::cout << hit.GlobalPosX() << " " << hit.GlobalPosY()  << hit.GlobalPosZ()  << std::endl;
  }
}
