#include "LineFitFCN.hh"
#include "Tracking/Tracking.hh"

double LineFitFCN::operator() (const std::vector<double> & x) const {
  return GetChisq(x);
}
double LineFitFCN::operator()( const double *  par ) const {
  std::vector<double> p(par, par+5);
  return (*this)(p);
}
double LineFitFCN::GetChisq (const std::vector<double> & x) const {
  // Rosebrock function
  //  return  fA*(x[1] - x[0]*x[0])*(x[1] - x[0]*x[0]) + fB*(1 - x[0])*(1 - x[0]);
  double chi2=0;
  Tracking tr;
  tr.SetMagneticField(fB);
  tr.SetTransverseMomentum(x[4]);
  tr.SetApoint(x[0],x[1],0);
  tr.SetDirection(x[2],x[3]);
  if(clusters.size()==0)return -1;
  for(unsigned int icl=0;icl<clusters.size();icl++){
    TVector3 trkpoint=tr.GetPointFromZinB(((Position)clusters[icl]).PosZ());
    TVector3 res=trkpoint-((Position)clusters[icl]).Pos();
    double resXe=((Position)clusters[icl]).PosXe()*cos(res.Phi());
    double resYe=((Position)clusters[icl]).PosYe()*sin(res.Phi());
    chi2+=res.Mag()*res.Mag()/sqrt(resXe*resXe+resYe*resYe);
  }
  return chi2;
} 
