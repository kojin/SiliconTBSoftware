#ifndef __TrackInB_hh__
#define __TrackInB_hh__

#include "Line3D.hh"

class TrackInB : public Line3D{
private:

  double pT;
  double Bfield;
public:
  TrackInB(){
    Bfield=0;
    pT=1e10;
  }
  TrackInB(double _bf,double _pt){Bfield=_bf;pT=_pt;}
  ~TrackInB(){}
  void SetTransverseMomentum(double _pt){pT=_pt;}
  void SetMagneticField(double _bf){Bfield=_bf;}
  double GetTransverseMomentum(){return pT;}
  TVector3 GetPointFromZinB(double _z);
  std::vector<TVector3> GetPointsOnLineInB(int npoint,double startz,double endz);
  void PrintPointsOnLineInB(int npoint,double startz,double endz);


};



#endif
