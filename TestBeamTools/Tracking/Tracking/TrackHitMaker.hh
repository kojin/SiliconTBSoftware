#ifndef __TRACKHITMAKER_HH__
#define __TRACKHITMAKER_HH__
#include <iostream>
#include <sstream>
#include "Alignment/Position.hh"
#include "HistogramTools/HistogramVariableStrage.hh"
#include "Tracking/Tracking.hh"
#include "Tracking/LineFitter.hh"
#include "TGraphErrors.h"
#include "TCanvas.h"
class TrackHitMaker {
private:
  unsigned int nlayer;
  HistogramVariableStrage *hvs;
  std::vector<Position> positions;
  std::vector<double> residualX;
  std::vector<double> residualY;
  TCanvas *c1;
  Tracking *track;
public:
  TrackHitMaker(){
    track=new Tracking();
    positions.clear();
  }

  TrackHitMaker(std::vector<Position> _pos){
    positions.clear();
    positions=_pos;
  }
  void setNLayer(unsigned int _nlay){nlayer=_nlay;}
  ~TrackHitMaker(){delete track;}
  void SetHistogram(HistogramVariableStrage *_hvs);
  Tracking *GetTrack(){return track;}
  void do2DTracking(int Debug=0,double error=-999.);
  void do3DTracking();
  void FillHistogram();
  
  void SetHitPosition(std::vector<Position> _pos){
    positions=_pos;
    if(positions.size()!=nlayer)std::cerr << "position information set as wrong size " << positions.size() <<  " : should be " << nlayer<< std::endl;
  }
  void PrintHitinTrack();

};


#endif
