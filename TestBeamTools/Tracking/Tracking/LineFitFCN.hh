#ifndef __LineFitFCN_hh__
#define __LineFitFCN_hh__

#include "TH1.h"
#include "TF1.h"
#include "TRandom3.h"
#include "TVirtualFitter.h"
#include "TStyle.h"
#include "Minuit2/FCNBase.h"
//#include "TFitterMinuit.h"
#include "TSystem.h"
#include "TStopwatch.h"
#include "Alignment/Position.hh"
#include <vector>
#include <iostream>

static std::vector<Position> c0;
class LineFitFCN : public ROOT::Minuit2::FCNBase { 

public: 
  LineFitFCN(double a = 100, double b = 1, std::vector<Position>c=c0) : fA(a), fB(b), clusters(c){}

  double GetChisq(const std::vector<double> & x) const ;
  double operator() (const std::vector<double> & x) const ;
  double operator()( const double *  par ) const ;
  
  double Up() const { return 1.; }

private: 

  double fA;
  double fB; 
  std::vector<Position>clusters;

};




#endif
