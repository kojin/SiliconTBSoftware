#ifndef __Sagitta_HH__
#define __Sagitta_HH__

#include <iostream>
#include <vector>
#include "Cluster/Cluster.hh"
#include "Tracking/Line3D.hh"

class Sagitta{
private:
  std::vector<TVector3> clusters;
  double Bfield;
  double pT;
public:
  Sagitta(){}
  ~Sagitta(){}
  void SetClusters(std::vector<Cluster> clu);
  void SetClusters(std::vector<TVector3> clu){clusters=clu;}
  void SetMagneticField(double bf){Bfield=bf;};
  double GetMagneticField(){return Bfield;}
  void CalculateMomentum();
  double GetMomentum(){return pT;};
};


#endif
