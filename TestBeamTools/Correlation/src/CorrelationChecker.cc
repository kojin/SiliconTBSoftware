#include "Correlation/CorrelationChecker.hh"
void CorrelationChecker::SetHistogram(HistogramVariableStrage *_hvs){
  hvs=_hvs;
  std::stringstream ss; 
  std::stringstream ssc0; 
  std::stringstream ssc1; 
  std::stringstream ssc2; 
  std::stringstream ssc3; 

  std::vector<int> added; added.clear();

  //  hvs->Add2DHistogram("Correlation/diffvsevent",500000,-0.0005,49999.5,100,-0.5,200.5);
  hvs->Add2DHistogram("Correlation/diffvsevent",500000,-0.0005,49999.5,100,0,400);
  //  hvs->Add2DHistogram("Correlation/diffbcidvsevent",10000,-0.0005,49999.5,100,0,32768);

  for(unsigned int ii=0;ii<hli.size();ii++){
    if(hli[ii].type.substr(0,4)!="FEI4" && hli[ii].type.substr(0,4)!="RD53")continue;
    bool isadded=false;
    for(auto ad : added){
      if(hli[ii].layer==ad){
	isadded=true;
	break;
      }
    }
    if(isadded)continue;
    added.push_back(hli[ii].layer);
    //    std::cout <<  hli[ii].layer << std::endl;

    //    ss.str("");
    //    ss<< "Correlation/WithFE65hits/FEI4tel" <<  hli[ii].layer;
    ssc0.str("");
    ssc1.str("");
    ssc2.str("");
    ssc3.str("");

    int nbin2x; 
    int nbin2y;
    if(hli[ii].type.substr(0,8)=="FEI4SING"){
      nbin2x=80;nbin2y=336;
    }else if(hli[ii].type.substr(0,5)=="FEI4Q"){
      nbin2x=160;nbin2y=672;
    }else if(hli[ii].type.substr(0,5)=="FEI4D"){
      nbin2x=80;nbin2y=672;
    }else if(hli[ii].type.substr(0,5)=="RD53A"){
      nbin2x=400;nbin2y=192;
    }else {
      std::cout << " strange type : " << hli[ii].type.substr(0,5) << std::endl;
    }

    if(ii!=0){
      ssc0<< "Correlation/Pixel/ly" << hli[ii-1].layer << hli[ii-1].type.substr(0,5) << "xly" << hli[ii].layer << hli[ii].type.substr(0,5)<<"X";
      ssc1<< "Correlation/Pixel/ly" << hli[ii-1].layer << hli[ii-1].type.substr(0,5) << "yly" << hli[ii].layer << hli[ii].type.substr(0,5)<<"Y";
      ssc2<< "Correlation/Pixel/ly" << hli[ii-1].layer << hli[ii-1].type.substr(0,5) << "xly" << hli[ii].layer << hli[ii].type.substr(0,5)<<"Y";
      ssc3<< "Correlation/Pixel/ly" << hli[ii-1].layer << hli[ii-1].type.substr(0,5) << "yly" << hli[ii].layer << hli[ii].type.substr(0,5)<<"X";

      int nbin1x;
      int nbin1y;
      if(hli[ii-1].type.substr(0,8)=="FEI4SING"){
	nbin1x=80;nbin1y=336;
      }else if(hli[ii-1].type.substr(0,5)=="FEI4Q"){
	nbin1x=160;nbin1y=672;
      }else if(hli[ii-1].type.substr(0,5)=="FEI4D"){
	nbin1x=80;nbin1y=672;
      }else if(hli[ii-1].type.substr(0,5)=="RD53A"){
	nbin1x=400;nbin1y=192;
      }else {
	std::cout << " strange type : " << hli[ii].type.substr(0,5) << std::endl;
      }
      hvs->Add2DHistogram(ssc0.str(),nbin1x,-0.5,nbin1x-0.5,nbin2x,-0.5,nbin2x-0.5);
      hvs->Add2DHistogram(ssc1.str(),nbin1y,-0.5,nbin1y-0.5,nbin2y,-0.5,nbin2y-0.5);
      hvs->Add2DHistogram(ssc2.str(),nbin1x,-0.5,nbin1x-0.5,nbin2y,-0.5,nbin2y-0.5);
      hvs->Add2DHistogram(ssc3.str(),nbin1y,-0.5,nbin1y-0.5,nbin2x,-0.5,nbin2x-0.5);
      
      
    }

  }
}

void CorrelationChecker::SetHistogram(HistogramVariableStrage *_hvs,OnlineMonWindow *om){
  SetHistogram(_hvs);
  for(auto x : hvs->GetHistName()){
    om->registerTreeItem(x);
    om->registerHisto(x,hvs->GetHistogram(x),"HIST",0);
  }
  for(auto y : hvs->Get2DHistName()){
    om->registerTreeItem(y);
    om->registerHisto(y,hvs->Get2DHistogram(y),"COLZ",4);
  }
  om->makeTreeItemSummary("Correlation/Pixel");
}


void CorrelationChecker::FillHistogram(){
  bool debug=false;
  if(debug)std::cout << "start Correlator Histogram Filling ..." << std::endl;
  std::stringstream ssc0; 
  std::stringstream ssc1; 
  std::stringstream ssc2; 
  std::stringstream ssc3; 


  std::vector<int> added; added.clear();
  for(unsigned int ii=0;ii<hli.size();ii++){
    if(debug)std::cout << "start module " << hli[ii].type << " " << hli[ii].name << std::endl;
    if(hli[ii].type.substr(0,4)!="FEI4"&& hli[ii].type.substr(0,4)!="RD53")continue;
    bool isadded=false;
    for(auto ad : added){
      if(hli[ii].layer==ad){
	isadded=true;
	break;
      }
    }
    if(isadded)continue;
    added.push_back(hli[ii].layer);
    ssc0.str("");
    ssc1.str("");
    ssc2.str("");
    ssc3.str("");
    
    if(ii==0)continue;
    ssc0<< "Correlation/Pixel/ly" << hli[ii-1].layer << hli[ii-1].type.substr(0,5) << "xly" << hli[ii].layer << hli[ii].type.substr(0,5)<<"X";
    ssc1<< "Correlation/Pixel/ly" << hli[ii-1].layer << hli[ii-1].type.substr(0,5) << "yly" << hli[ii].layer << hli[ii].type.substr(0,5)<<"Y";
    ssc2<< "Correlation/Pixel/ly" << hli[ii-1].layer << hli[ii-1].type.substr(0,5) << "xly" << hli[ii].layer << hli[ii].type.substr(0,5)<<"Y";
    ssc3<< "Correlation/Pixel/ly" << hli[ii-1].layer << hli[ii-1].type.substr(0,5) << "yly" << hli[ii].layer << hli[ii].type.substr(0,5)<<"X";
    

    std::vector<PixelHit> hit1;
    if(hli[ii-1].type.substr(0,4)=="FEI4") hit1=event->getFEI4Hits(hli[ii-1].layer);
    else if(hli[ii-1].type.substr(0,5)=="RD53A")hit1=event->getRD53AHits(hli[ii-1].layer);
    else std::cout << "strange type " << hli[ii].type << std::endl;
    
    for(auto ihit1 : hit1){
      std::vector<PixelHit> hit2;
      if(hli[ii].type.substr(0,4)=="FEI4") hit2=event->getFEI4Hits(hli[ii].layer);
      else if(hli[ii].type.substr(0,5)=="RD53A")hit2=event->getRD53AHits(hli[ii].layer);
      else std::cout << "strange type " << hli[ii].type << std::endl;
      
      for(auto ihit2 : hit2){
	hvs->Get2DHistogram(ssc0.str())->Fill(ihit1.Col(),ihit2.Col());
	hvs->Get2DHistogram(ssc1.str())->Fill(ihit1.Row(),ihit2.Row());
	hvs->Get2DHistogram(ssc2.str())->Fill(ihit1.Col(),ihit2.Row());
	hvs->Get2DHistogram(ssc3.str())->Fill(ihit1.Row(),ihit2.Col());
	
	if(hli[ii-1].layer==1 && hli[ii].layer==2){
	  hvs->Get2DHistogram("Correlation/diffvsevent")->Fill(event->getEventNumber(),ihit2.Col()-ihit1.Row());

	  int bciddiff=0;
	  int bcid1=ihit1.GetBCID();
	  int bcid2=ihit2.GetBCID();
	  
	  if (bcid1- bcid2 < 0 && bcid1-bcid2+32768 > 16) {// wrap around, just reset
	    bciddiff=bcid1-bcid2+32768;
	  } else if (bcid1-bcid2 > 16) { 
	    bciddiff=bcid1-bcid2;
	  }
	  //	  std::cout << bciddiff << std::endl;
	  //	  hvs->Get2DHistogram("Correlation/diffbcidvsevent")->Fill(event->getEventNumber(),bciddiff);
	}
      }
    }
    if(debug)std::cout << "module done" << std::endl;
  }
  
  if(debug)std::cout << "end Correlator Histogram Filling ..." << std::endl;
}


void CorrelationChecker::PrintCorrelation(){
  
  for(auto fei4ly : event->getFEI4Hits()){
    if(fei4ly.size()>0){
      std::cout << "FEI4 : ly=" << fei4ly[0].GetLayer() << " "<< fei4ly.size() << std::endl;
    }
    for(auto fei4hits :fei4ly){
      fei4hits.PrintPixelHit();
    }
  }

  for(auto svx4ly : event->getSVX4Hits()){
    if(svx4ly.size()>0){
      std::cout << "SVX4 : ly=" << svx4ly[0].GetLayer() << " "<< svx4ly.size() << std::endl;
    }
  }
  
}
