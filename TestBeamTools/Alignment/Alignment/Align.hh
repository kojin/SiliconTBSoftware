#ifndef __Align_hh__
#define __Align_hh__

#include <iostream>
#include <fstream>
#include <vector>


class Align {
public:
  std::vector<std::string> alignfilename;
  std::vector<std::vector<std::string>> modname;
  std::vector<std::vector<double>>  shiftX;
  std::vector<std::vector<double>>  shiftY;
  std::vector<std::vector<double>>  shiftZ;
  std::vector<std::vector<double>>  rotateX; 
  std::vector<std::vector<double>>  rotateY;
  std::vector<std::vector<double>>  rotateZ; 
  Align();
  ~Align(){};
  //  int & operator=(const int){};
  void SetAlignmentFile(std::string alignfile,int nlayer);
  void GetAlignment(std::string alignfile,int nlayer, double *para);
  unsigned int GetIndex(int ifile,std::string _name);
  void PrintAlignment();



};

#endif
