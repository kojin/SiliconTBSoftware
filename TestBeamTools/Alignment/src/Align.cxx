#include "Align.hh"

Align::Align(){
  alignfilename.clear();
  shiftX.clear();
  shiftY.clear();
  rotateX.clear();
  rotateY.clear();
  rotateZ.clear();
}
unsigned int Align::GetIndex(int ifile,std::string _name){
  unsigned int index=0;
  for(auto n : modname[ifile]){
    if(n==_name)return index;
    index++;
  }
}

void Align::SetAlignmentFile(std::string alignfile,int nlayer){
  std::ifstream ifs(alignfile);
  std::string name; 
  double shiftx,shifty,shiftz,rotatex,rotatey,rotatez;
  std::vector<std::string> _modname;
  std::vector<double> _shiftX;
  std::vector<double> _shiftY;
  std::vector<double> _shiftZ;
  std::vector<double> _rotateX;
  std::vector<double> _rotateY;
  std::vector<double> _rotateZ;
  for(int ily=0;ily<nlayer;ily++){
    ifs >> name >> shiftx >> shifty >> shiftz >> rotatex >> rotatey >> rotatez;
    //    std::cout << gomi << " " << num << std::endl;
    _modname.push_back(name);
    _shiftX.push_back(shiftx);
    _shiftY.push_back(shifty);
    _shiftZ.push_back(shiftz);
    _rotateX.push_back(rotatex);
    _rotateY.push_back(rotatey);
    _rotateZ.push_back(rotatez);
  }
//  std::cout << alignfile << " " ;
//  for(int ii=0;ii<nlayer;ii++)std::cout << _shiftX[ii] << " " ;
//  for(int ii=0;ii<nlayer;ii++)std::cout << _shiftY[ii] << " " ;
//  std::cout << std::endl;
  alignfilename.push_back(alignfile);
  modname.push_back(_modname);
  shiftX.push_back(_shiftX);
  shiftY.push_back(_shiftY);
  shiftZ.push_back(_shiftZ);
  rotateX.push_back(_rotateX);
  rotateY.push_back(_rotateY);
  rotateZ.push_back(_rotateZ);
}
void Align::GetAlignment(std::string alignfile,int nlayer, double *para){
  
  //  unsigned int iset=0;
  for(unsigned int ii=0; ii<alignfilename.size();ii++){
    if(alignfilename[ii]==alignfile){
      //      iset=ii;
      para[0]=shiftX[ii][nlayer];
      para[1]=shiftY[ii][nlayer];
      para[2]=shiftZ[ii][nlayer];
      para[3]=rotateX[ii][nlayer];
      para[4]=rotateY[ii][nlayer];
      para[5]=rotateZ[ii][nlayer];

      //      std::cout << shift[0] << " " << shift[1] << std::endl;
      return;
    }
  }
  if(alignfile!="")std::cout << alignfile << " not find..."<< std::endl;
  for(int ii=0;ii<6;ii++)para[ii]=0.;
}
void Align::PrintAlignment(){
  for(unsigned int iset=0;iset<alignfilename.size();iset++){
    std::cout << "filename : " << alignfilename[iset] << std::endl;
    for(unsigned int ily=0;ily<shiftX[iset].size();ily++){
      std::cout << shiftX[iset][ily] << " " << shiftY[iset][ily] << std::endl;
    }
  }
}
