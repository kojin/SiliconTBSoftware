#include <iostream>
#include "Position.hh"
#include "TMath.h"

void Position::GetRotatePos(double angle, double &p1,double &p2){
  double tmp1=p1; double tmp2=p2;
  double sinT=TMath::Sin(angle/180*TMath::Pi());
  double cosT=TMath::Cos(angle/180*TMath::Pi());
  tmp1=p1*cosT-p2*sinT;
  tmp2=p1*sinT+p2*cosT;
  p1=tmp1;
  p2=tmp2;
}

Position::Position(){
  posX=0;posY=0;posZ=0;glposX=0;glposY=0;glposZ=0;
  sensorX=20.;  sensorY=20.;
}
void Position::SetGlobalPosition(Align ar,std::string name){
  double tmpX=0;  double tmpY=0;  double tmpZ=0;
  tmpX=posX-sensorX/2.;  tmpY=posY-sensorY/2.;  
  tmpZ=0;
  double tmpXe=posXe; double tmpYe=posYe;
  for(unsigned int ii=0;ii<ar.alignfilename.size();ii++){
    unsigned int index=ar.GetIndex(ii,name);
    GetRotatePos(ar.rotateZ[ii][index],tmpX,tmpY);
    GetRotatePos(ar.rotateX[ii][index],tmpY,tmpZ);
    GetRotatePos(ar.rotateY[ii][index],tmpX,tmpZ);
    GetRotatePos(ar.rotateZ[ii][index],tmpXe,tmpYe);
    
  }
  
  for(unsigned int ii=0;ii<ar.alignfilename.size();ii++){
    unsigned int index=ar.GetIndex(ii,name);
    tmpX+=ar.shiftX[ii][index];
    tmpY+=ar.shiftY[ii][index];
    tmpZ+=ar.shiftZ[ii][index];
  }
  SetGlobalPosX(tmpX);
  SetGlobalPosY(tmpY);
  SetGlobalPosZ(tmpZ);
  SetGlobalPosXe(fabs(tmpXe));
  SetGlobalPosYe(fabs(tmpYe));
  SetGlobalPosZe(posZe);
}


void Position::SetLocalPosition(Align ar,std::string name){
  double tmpX=glposX;  double tmpY=glposY;  double tmpZ=glposZ;
  for(unsigned int ii=0;ii<ar.alignfilename.size();ii++){
    unsigned int index=ar.GetIndex(ii,name);
    tmpX-=ar.shiftX[ii][index];
    tmpY-=ar.shiftY[ii][index];
    tmpZ-=ar.shiftZ[ii][index];
  }
  for(int ii=(int)ar.alignfilename.size()-1;ii>=0;ii--){
    unsigned int index=ar.GetIndex(ii,name);
    GetRotatePos(-1*ar.rotateZ[ii][index],tmpX,tmpY);
    GetRotatePos(-1*ar.rotateX[ii][index],tmpY,tmpZ);
    GetRotatePos(-1*ar.rotateY[ii][index],tmpX,tmpZ);
  }

  posX=tmpX+sensorX/2.;  
  posY=tmpY+sensorY/2.;  
  posZ=tmpZ;
  
//  SetGlobalPosX(tmpX);
//  SetGlobalPosY(tmpY);
//  SetGlobalPosZ(tmpZ);
}


void Position::PrintPosition(){
  std::cout << "----  printing positions... Layer" << iLayer << std::endl;
  std::cout << "Local Pos  : " << std::setprecision(4)<< posX<< " " << posY << " " << posZ << std::endl;
  std::cout << "Local Pos err  : " << std::setprecision(4) << "+-" << posXe << " " << "+-" << posYe << " "  << "+-" << posZe << std::endl;
  std::cout << "Global Pos : " << std::setprecision(4)<< glposX << " " << glposY << " " << glposZ << std::endl; 
}
