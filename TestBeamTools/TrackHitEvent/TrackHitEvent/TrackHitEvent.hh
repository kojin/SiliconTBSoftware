#ifndef __FEI4Event__HH__
#define __FEI4Event__HH__
#include "Cluster/Cluster.hh"
#include "Hit/PixelHit.hh"
#include "Hit/StripHit.hh"
#include "Hit/LGADHit.hh"
#include "Configuration/DAQLinkInfo.hh"
#include <vector>


class TrackHitEvent {
private:
  int evenum;
  int svx4tdc;
  int fe65evenum;
  int rd53aevenum;
  int fei4evenum;
  int svx4evenum;
  int lgadevenum;
  int nSkipbyHSIO2;
  
  Align ar0;
  std::vector<DAQLinkInfo> hli;
  std::vector<std::vector<PixelHit>> fe65hits;
  std::vector<std::vector<PixelHit>> rd53ahits;
  std::vector<std::vector<PixelHit>> fei4hits;
  std::vector<std::vector<StripHit>> svx4hits;
  std::vector<std::vector<LGADHit>> lgadhits;

  std::vector<std::vector<Cluster>> fe65clusters;
  std::vector<std::vector<Cluster>> rd53aclusters;
  std::vector<std::vector<Cluster>> fei4clusters;
  std::vector<std::vector<Cluster>> svx4clusters;
  std::vector<DAQLinkInfo> fei4linklist;
  std::vector<int> fe65linklist;
  std::vector<int> rd53alinklist;
  std::vector<int> svx4linklist;
  std::vector<int> lgadlinklist;
public:
  TrackHitEvent();
  ~TrackHitEvent(){}
  void setEventNumber(int en){evenum=en;}
  void setFE65EventNumber(int en){fe65evenum=en;}
  void setRD53AEventNumber(int en){rd53aevenum=en;}
  void setFEI4EventNumber(int en){fei4evenum=en;}
  void setSVX4EventNumber(int en){svx4evenum=en;}
  void setLGADEventNumber(int en){lgadevenum=en;}
  void setDAQLinkInfo(std::vector<DAQLinkInfo> _hli){hli=_hli;}
  void setAlign(Align _ar){ar0=_ar;}
  Align getAlign(){return ar0;}

  void setFE65Linklist(std::vector<int> ll){fe65linklist=ll;}
  void setRD53ALinklist(std::vector<int> ll){rd53alinklist=ll;}
  void setFEI4Linklist(std::vector<DAQLinkInfo> ll){fei4linklist=ll;}
  void setSVX4Linklist(std::vector<int> ll){svx4linklist=ll;}
  void setSVX4TDC(int _tdc){svx4tdc=_tdc;}
  void setLGADLinklist(std::vector<int> ll){lgadlinklist=ll;}

  int getEventNumber(){return evenum;}
  int getFE65EventNumber(){return fe65evenum;}
  int getRD53AEventNumber(){return rd53aevenum;}
  int getFEI4EventNumber(){return fei4evenum;}
  int getSVX4EventNumber(){return svx4evenum;}
  int getLGADEventNumber(){return lgadevenum;}
  std::vector<int> getFE65Linklist(){return fe65linklist;}
  std::vector<int> getRD53ALinklist(){return rd53alinklist;}
  std::vector<DAQLinkInfo> getFEI4Linklist(){return fei4linklist;}
  std::vector<int> getSVX4Linklist(){return svx4linklist;}
  std::vector<int> getLGADLinklist(){return lgadlinklist;}
  int  getSVX4TDC(){return svx4tdc;}

  void addFEI4Hits(PixelHit ahit);
  void addFE65Hits(PixelHit ahit);
  void addRD53AHits(PixelHit ahit);
  void addSVX4Hits(StripHit ahit);
  void addLGADHits(LGADHit ahit);
  void doClustering(int ilayer,std::vector<PixelHit>hits,std::vector<Cluster>&clusters);
  void doFEI4Clustering(bool debug=false);
  void doFE65Clustering(bool debug=false);
  void doRD53AClustering(bool debug=false);
  void doSVX4Clustering(bool debug=false);

  std::vector<std::vector<Cluster>> getFEI4Clustering(){return fei4clusters;}
  std::vector<std::vector<Cluster>> getFE65Clustering(){return fe65clusters;}
  std::vector<std::vector<Cluster>> getRD53AClustering(){return rd53aclusters;}
  std::vector<std::vector<Cluster>> getSVX4Clustering(){return svx4clusters;}

  std::vector<Cluster> getFEI4Clustering(int layer);
  std::vector<Cluster> getFE65Clustering(int layer);
  std::vector<Cluster> getRD53AClustering(int layer);
  std::vector<Cluster> getSVX4Clustering(int layer);

  std::vector<std::vector<PixelHit>> getFEI4Hits(){return fei4hits;}
  std::vector<std::vector<PixelHit>> getRD53AHits(){return rd53ahits;}
  std::vector<std::vector<PixelHit>> getFE65Hits(){return fe65hits;}
  std::vector<std::vector<StripHit>> getSVX4Hits(){return svx4hits;}
  std::vector<std::vector<LGADHit>> getLGADHits(){return lgadhits;}
  std::vector<PixelHit> getFEI4Hits(int layer);
  std::vector<PixelHit> getRD53AHits(int layer);
  std::vector<StripHit> getSVX4Hits(int layer);
  std::vector<LGADHit> getLGADHits(int layer);
  void setnSkipbyHSIO2(int _nskip){nSkipbyHSIO2=_nskip;}
  int getnSkipbyHSIO2(){return nSkipbyHSIO2;}
};


#endif
