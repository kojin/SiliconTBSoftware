#include "TrackHitEvent/TrackHitEvent.hh"
TrackHitEvent::TrackHitEvent(){
  fe65hits.clear();
  fei4hits.clear();
  svx4hits.clear();
  nSkipbyHSIO2=0;
}

void TrackHitEvent::addFE65Hits(PixelHit ahit){
  bool isnewlayer=true;
  int ilay=0;
  for(auto lyhit : fe65hits){
    if(lyhit.size()>0){
      if(lyhit[0].GetLayer()==ahit.GetLayer()){
	fe65hits[ilay].push_back(ahit);
	isnewlayer=false;
	return;
      }
    }
    ilay++;
  }
  if(isnewlayer==true){
    std::vector<PixelHit> newhits; newhits.clear();
    newhits.push_back(ahit);
    fe65hits.push_back(newhits);
  }else{
    std::cerr << "strange FE65 hits!!" << std::endl;
  }
}


void TrackHitEvent::addRD53AHits(PixelHit ahit){
  bool isnewlayer=true;
  int ilay=0;
  for(auto lyhit : rd53ahits){
    if(lyhit.size()>0){
      if(lyhit[0].GetLayer()==ahit.GetLayer()){
	rd53ahits[ilay].push_back(ahit);
	isnewlayer=false;
	return;
      }
    }
    ilay++;
  }
  if(isnewlayer==true){
    std::vector<PixelHit> newhits; newhits.clear();
    newhits.push_back(ahit);
    rd53ahits.push_back(newhits);
  }else{
    std::cerr << "strange RD53A hits!!" << std::endl;
  }
}


void TrackHitEvent::addFEI4Hits(PixelHit ahit){
  bool isnewlayer=true;
  int ilay=0;
  for(auto lyhit : fei4hits){
    if(lyhit.size()>0){
      //      if(lyhit[0].DAQLink()==ahit.DAQLink()){
      if(lyhit[0].GetLayer()==ahit.GetLayer()){
	fei4hits[ilay].push_back(ahit);
	isnewlayer=false;
	return;
      }
    }
    ilay++;
  }
  if(isnewlayer==true){
    std::vector<PixelHit> newhits; newhits.clear();
    newhits.push_back(ahit);
    fei4hits.push_back(newhits);
  }else{
    std::cerr << "strange FEI4 hits!!" << std::endl;
  }
}
void TrackHitEvent::addSVX4Hits(StripHit ahit){
  bool isnewlayer=true;
  int ilay=0;
  for(auto lyhit : svx4hits){
    if(lyhit.size()>0){
      if(lyhit[0].GetLayer()==ahit.GetLayer()){
	svx4hits[ilay].push_back(ahit);
	isnewlayer=false;
	return;
      }
    }
    ilay++;
  }
  if(isnewlayer==true){
    std::vector<StripHit> newhits; newhits.clear();
    newhits.push_back(ahit);
    svx4hits.push_back(newhits);
    //    std::cout << "new event (" << ahit.GetLayer() << "): added" << std::endl;
  }else{
    std::cerr << "strange SVX4 hits!!" << std::endl;
  }

}

std::vector<Cluster> TrackHitEvent::getFEI4Clustering(int layer){
  //  std::cout << "nlayer = " << fei4hits.size() << std::endl;
  if(fei4clusters.size()==0){
    std::vector<Cluster> emptycluster;
    return emptycluster;
  }
  for(auto lyclu : fei4clusters){
    if(lyclu[0].GetLayer()==layer)return lyclu;
    //    if(lyhit[0].DAQLink()==layer)return lyhit;
  }
  std::vector<Cluster> emptycluster;
  return emptycluster;
}

std::vector<Cluster> TrackHitEvent::getFE65Clustering(int layer){
  //  std::cout << "nlayer = " << fe65hits.size() << std::endl;
  if(fe65clusters.size()==0){
    std::vector<Cluster> emptycluster;
    return emptycluster;
  }
  for(auto lyclu : fe65clusters){
    if(lyclu[0].GetLayer()==layer)return lyclu;
    //    if(lyhit[0].DAQLink()==layer)return lyhit;
  }
  std::vector<Cluster> emptycluster;
  return emptycluster;
}


std::vector<Cluster> TrackHitEvent::getRD53AClustering(int layer){
  //  std::cout << "nlayer = " << rd53ahits.size() << std::endl;
  if(rd53aclusters.size()==0){
    std::vector<Cluster> emptycluster;
    return emptycluster;
  }
  for(auto lyclu : rd53aclusters){
    if(lyclu[0].GetLayer()==layer)return lyclu;
    //    if(lyhit[0].DAQLink()==layer)return lyhit;
  }
  std::vector<Cluster> emptycluster;
  return emptycluster;
}

std::vector<Cluster> TrackHitEvent::getSVX4Clustering(int layer){
  //  std::cout << "nlayer = " << svx4hits.size() << std::endl;
  if(svx4clusters.size()==0){
    std::vector<Cluster> emptycluster;
    return emptycluster;
  }
  for(auto lyclu : svx4clusters){
    if(lyclu[0].GetLayer()==layer)return lyclu;
    //    if(lyhit[0].DAQLink()==layer)return lyhit;
  }
  std::vector<Cluster> emptycluster;
  return emptycluster;
}

std::vector<PixelHit> TrackHitEvent::getFEI4Hits(int layer){
  //  std::cout << "nlayer = " << fei4hits.size() << std::endl;
  if(fei4hits.size()==0){
    std::vector<PixelHit> emptyhits;
    return emptyhits;
  }
  for(auto lyhit : fei4hits){
    if(lyhit[0].GetLayer()==layer)return lyhit;
    //    if(lyhit[0].DAQLink()==layer)return lyhit;
  }
  std::vector<PixelHit> emptyhits;
  return emptyhits;
}

std::vector<PixelHit> TrackHitEvent::getRD53AHits(int layer){
  //  std::cout << "nlayer = " << rd53ahits.size() << std::endl;
  if(rd53ahits.size()==0){
    std::vector<PixelHit> emptyhits;
    return emptyhits;
  }
  for(auto lyhit : rd53ahits){
    if(lyhit[0].GetLayer()==layer)return lyhit;
    //    if(lyhit[0].DAQLink()==layer)return lyhit;
  }
  std::vector<PixelHit> emptyhits;
  return emptyhits;
}

std::vector<StripHit> TrackHitEvent::getSVX4Hits(int layer){
  //  std::cout << "nlayer = " << fei4hits.size() << std::endl;
  if(svx4hits.size()==0){
    std::vector<StripHit> emptyhits;
    return emptyhits;
  }
  for(auto lyhit : svx4hits){
    if(lyhit[0].GetLayer()==layer)return lyhit;
  }
  std::vector<StripHit> emptyhits;
  return emptyhits;
}

std::vector<LGADHit> TrackHitEvent::getLGADHits(int layer){
  if(lgadhits.size()==0){
    std::vector<LGADHit> emptyhits;
    return emptyhits;
  }
  for(auto lyhit : lgadhits){
    if(lyhit[0].GetLayer()==layer)return lyhit;
  }
  std::vector<LGADHit> emptyhits;
  return emptyhits;
}

void TrackHitEvent::doClustering(int ilayer,std::vector<PixelHit> hits,std::vector<Cluster>&clusters){
  bool debug=false;
  std::vector<unsigned int> used; used.clear();
  for(unsigned int ihit=0;ihit<hits.size();ihit++){
    bool isused=false;
    for(auto xx : used){
      if(ihit==xx){
	isused=true;
	break;
      }
    }
    if(isused)continue;
    Cluster tmpclu;
    tmpclu.AddHitsForCluster(hits[ihit]);
    tmpclu.SetLayer(hits[ihit].GetLayer());
    used.push_back(ihit);
    std::string modname;modname="";
    for(auto h : hli){
      if(h.layer==hits[ihit].GetLayer())modname=h.name;
    }
    if(modname==""){
      std::cout << "unknown layer :" << hits[ihit].GetLayer() << std::endl;
      for(auto h : hli){
	std::cout << h.layer << " : " << h.name << std::endl;
      }
    }
    if(ihit==hits.size()-1) {
      tmpclu.GetClusterPosition(1); // 0 cernter of hits; 1 tot weight
      tmpclu.SetGlobalPosition(ar0,modname);
      clusters.push_back(tmpclu);
      break;    
    }
    for(unsigned int jhit=ihit+1;jhit<hits.size();jhit++){
      for(auto inclu :tmpclu.GetHitsForCluster()){
	if(hits[jhit].isNext(inclu)){
	  tmpclu.AddHitsForCluster(hits[jhit]);
	  used.push_back(jhit);
	  break;
	}
      }
    }
    //    int nhit=0;
//    while(hits[ihit].isNext(hits[ihit+1])){ // To be updated !!
//      tmpclu.AddHitsForCluster(hits[ihit+1]);
//      ihit++; nhit++;
//      if(ihit==hits.size()-1){
//	break;
//      }
//    }
    tmpclu.GetClusterPosition(1); // 0 cernter of hits; 1 tot weight
    tmpclu.SetGlobalPosition(ar0,modname);
    clusters.push_back(tmpclu);
    
  }
  if(debug&&hits.size()>0){
    std::cout << "--" << hits[0].Name()<<std::endl;
    for(auto tmpclu : clusters){
      std::cout << "cluster size  = " << tmpclu.GetHitsForCluster().size() << std::endl;
      if(tmpclu.GetHitsForCluster().size()>0){
	for(unsigned int ihit = 0 ; ihit<tmpclu.GetHitsForCluster().size(); ihit++){
	  std::cout << tmpclu.GetHitsForCluster()[ihit].Col() << " " << tmpclu.GetHitsForCluster()[ihit].Row() << " " << tmpclu.GetHitsForCluster()[ihit].ToT() << std::endl;
	}
      }
    }
  }
}

void TrackHitEvent::doFE65Clustering(bool debug){
  //  std::vector<std::vector<PixelHit>> fe65hits;
  int ilayer=2;
  for(auto ilay : fe65hits){
    std::vector<Cluster> tmpclu;
    doClustering(ilayer++,ilay,tmpclu);
    fe65clusters.push_back(tmpclu);
  }
}

void TrackHitEvent::doRD53AClustering(bool debug){
  //  std::vector<std::vector<PixelHit>> rd53ahits;
  int ilayer=2;
  for(auto ilay : rd53ahits){
    std::vector<Cluster> tmpclu;
    doClustering(ilayer++,ilay,tmpclu);
    rd53aclusters.push_back(tmpclu);
  }
}

void TrackHitEvent::doFEI4Clustering(bool debug){
  //  std::vector<std::vector<PixelHit>> fei4hits;
  int ilayer=0;
  for(auto ilay : fei4hits){
    if(ilayer==2)ilayer++;
    std::vector<Cluster> tmpclu;
    doClustering(ilayer++,ilay,tmpclu);
    fei4clusters.push_back(tmpclu);

    if(debug){
      std::cout << "==== layer Hits ====" << std::endl;
      for(auto ihit : ilay)ihit.PrintPixelHit();
      std::cout << "---------" << std::endl;
      std::cout << "==== layer Cluster ====" << std::endl;
      for(auto iclu : tmpclu){
	for(auto ihit : iclu.GetHitsForCluster()){
	  ihit.PrintPixelHit();
	}
	std::cout << "---------" << std::endl;
      }
      std::cout << "---------" << std::endl;
      
    }

  }
//  int ilay=0;
//  for(auto icluly :fei4clusters ){
//    std::cout << "layer "  << ilay << std::endl;
//    for(auto iclu :icluly){
//      iclu.SetGlobalPosition(ar0,ilay);
//      iclu.PrintPosition();
//    }
//    for(auto iclu :icluly){
//      iclu.PrintPosition();
//    }
//    ilay++;
//  }
//  if(0){
//    ilay=0;
//    for(auto icluly :fei4clusters ){
//      std::cout << "layer "  << ilay << std::endl;
//	for(auto iclu :icluly){
//	  iclu.PrintPosition();
//	}
//      ilay++;
//    }
//    
//  }
}

void TrackHitEvent::doSVX4Clustering(bool debug){
  //  std::vector<std::vector<StripHit>> svx4hits;
}
