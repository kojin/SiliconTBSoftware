#ifndef __EVENTLOOPMANAGER_HH__
#define __EVENTLOOPMANAGER_HH__

#include <iostream>
#include <vector>
#include <thread>
#include "SeabasTLU/TLUconverter.hh"
#include "SPEC/SPECconverter.hh"
#include "XpressK7/XpressK7converter.hh"
#include "HSIO2/HSIO2converter.hh"
#include "SVX4/SVXconverter.hh"
#include "DRS4/DRS4Converter.hh"
#include "TrackHitEvent/TrackHitEvent.hh"
#include "OnlineMonWindow/OnlineMonWindow.hh"
#include "Correlation/CorrelationChecker.hh"
#include "HistogramTools/HistogramVariableStrage.hh"
#include "Configuration/DAQLinkInfo.hh"
#include "Tracking/TrackHitMaker.hh"
#include "Alignment/Align.hh"
#include "EfficiencyCalc/EfficiencyCalc.hh"
#include "TROOT.h"
#include "TNamed.h"
#include "TApplication.h"
#include "TGClient.h"
#include "TGMenu.h"
#include "TGTab.h"
#include "TGButton.h"
#include "TGComboBox.h"
#include "TGLabel.h"
#include "TGTextEntry.h"
#include "TGNumberEntry.h"
#include "TGComboBox.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TRootEmbeddedCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TPaletteAxis.h"
#include "TThread.h"
#include "TFile.h"
#include "TColor.h"
#include "TString.h"
#include "TF1.h"

class EventLoopManager{
private:
  std::vector<std::string> daqsyslist;
  std::vector<std::pair<int,std::string>> daqsystem;
  std::string TLUdatafile;
  std::string SPECdatafile;
  std::vector<std::string> XpressK7datafile;
  std::string HSIO2datafile;
  std::string SVX4datafile;
  std::string DRS4datafile;
  OnlineMonWindow *onlinemon;
  TLUconverter *tlucon;
  SPECconverter *speccon;
  std::vector<XpressK7converter*> xpresscon;
  HSIO2converter *hsio2con;
  SVXconverter *svxcon;
  DRS4Converter *drscon;

  std::vector<DAQLinkInfo> hli;
  std::vector<DAQLinkInfo> laylist;

  //  std::unique_ptr<std::thread> m_thread;
  bool isfirstHSIO2event;
  bool isfirstSVX4event;
  bool isfirstDRS4event;
  bool isfirstAnalysis;
  Align ar0;
  CorrelationChecker * corch;
  TrackHitMaker * trhitmker;
  EfficiencyCalc * efficalc;
  bool isOnlineMonitor;
protected:
  std::vector<HistogramVariableStrage*>hvsv;
  TH1D *numevent;
  TH1D *runnumber;
  //  TH3D *geometory;
public:
  EventLoopManager();
  ~EventLoopManager();
  void OnlineMonThread();
  void Initialize();
  void Execute();
  void AddDAQsystem(std::string daq,std::string filename,int boadnum=0);
  //  void setOnlineMonWindow(OnlineMonWindow *_omw){onlinemon=_omw;}
  void RunOnlineMonitor(){isOnlineMonitor=true;}
  std::vector<HistogramVariableStrage*> getAllHistograms(){return hvsv;}
};

#endif
